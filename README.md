# Expense Tracker API

The backend for a program to help me track my expenses.

## How to Run

Start the docker images using `docker compose up -d` Then use `gradle bootRun` to run the app.

NOTE: It still depends on the Keycloak auth server and the OpenAI API.

## Secrets for Local Running

Create a `secret.yml` with the following content:

```yaml
spring:
  ai:
    openai:
      api-key: ########
```

## Running Prod Tests Locally

End-to-end testing of AI features works best with prod data, which requires great care. The following steps are required to setup the app for prod testing.

### Secrets

Create `application-prod-test-secrets.yml` with the following contents for the prod postgres & weaviate accounts:

```yaml
spring:
  datasource:
    username: #####
    password: ######
  ai:
    weaviate:
      api-key: ######
```

### CA Cert

The self-signed CA cert from the kubernetes cluster needs to be loaded into the local java truststore. This can be done with the `import-prod-test-ca.sh` script.

### Authentication

You must authenticate with the `apps-prod` realm. This means using the `apps-prod` environment in the http client for authentication, and then `apps-dev` for everything else.

### Running

The `run-prod-test.sh` script will run the project in prod testing mode. 

## Swagger

The swagger can be accessed locally via `https://localhost:8080/swagger-ui/index.html`

## Terraform

For the Terraform script to run, the following environment variables must be present on the machine. 

```
# The operator access token for communicating with 1Password
ONEPASSWORD_TOKEN=XXXXXXX
```

To run it locally, do:

```bash
cd ./deploy/terraform
./terraform.sh
```