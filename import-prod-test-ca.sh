#!/usr/bin/env bash

set -euo pipefail

TRUSTSTORE="$JAVA_HOME/lib/security/cacerts"

if [ ! -f "$TRUSTSTORE" ]; then
  echo "Cannot find JDK truststore" >&2
  exit 1
fi

kubectl get secret craigmiller160-self-signed-ca-secret \
  -n cert-manager \
  -o json |
  jq -r '.data."ca.crt"' |
  base64 -D \
  > temp.crt

keytool -import \
  -trustcacerts \
  -file temp.crt \
  -alias craig-self-signed2 \
  -keystore "$TRUSTSTORE"

rm temp.crt