#!/usr/bin/env bash

set -euo pipefail

if [ ! -f application-prod-test-secrets.yml ]; then
  echo "Must have application-prod-test-secrets.yml file" >&2
  exit 1
fi

gradle bootRun \
  -PjvmArgs="-Dspring.profiles.active=prod -Dspring.config.location=classpath:application.yml,classpath:application-prod-test.yml,file:application-prod-test-secrets.yml"