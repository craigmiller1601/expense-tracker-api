package us.craigmiller160.expensetrackerapi.service.ai

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import us.craigmiller160.expensetrackerapi.client.ai.AiClient
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.toEmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.repository.EmbeddedTransactionRepository
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.WeaviateException
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.data.model.projection.toTransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingChangeLogRepository
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class EmbeddingSyncInitServiceTest
@Autowired
constructor(
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository,
    private val embeddingSyncInitService: EmbeddingSyncInitService,
    private val dataHelper: DataHelper,
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val embeddingChangeLogRepository: EmbeddingChangeLogRepository,
    private val defaultUsers: DefaultUsers,
    private val aiClient: AiClient
) {

  companion object {
    private val VECTORS = listOf(1.1f, 2.2f, 3.3f)
  }

  private lateinit var transactions: List<Transaction>

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
    whenever(aiClient.createEmbedding(any())).thenAnswer { invocation ->
      val count = (invocation.arguments[0] as List<String>).size
      val doubleVectors = VECTORS.map { it.toDouble() }
      (0 until count).map { doubleVectors }
    }

    transactions =
        (0 until 100).map { dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) }
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `performs initial sync of data to embedded database`() {
    embeddingSyncInitService.initializeSync()

    val changeLogCount = embeddingChangeLogRepository.getAllChangeLogs().size
    assertThat(changeLogCount).isEqualTo(0)

    val allTransactions =
        embeddedTransactionRepository.getAllTransactions(
            defaultUsers.primaryUser.userTypedId, 0, 1000)
    assertThat(allTransactions)
        .hasSize(transactions.size)
        .containsExactlyInAnyOrder(
            *transactions
                .map { it.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS) }
                .toTypedArray())

    assertThat(embeddingSyncConfigRepository.isInitialSyncComplete()).isTrue()
  }

  @Test
  fun `does not perform initial sync of data when that sync has already been done`() {
    embeddingSyncConfigRepository.setInitialSyncComplete(true)
    embeddingSyncInitService.initializeSync()

    val changeLogCount = embeddingChangeLogRepository.getAllChangeLogs().size
    assertThat(changeLogCount).isEqualTo(100)

    val ex =
        assertThrows<WeaviateException> {
          embeddedTransactionRepository.getTransactionCount(defaultUsers.primaryUser.userTypedId)
        }
    assertThat(ex.message).contains("tenant not found")
  }
}
