package us.craigmiller160.expensetrackerapi.service.ai

import java.time.LocalDate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import us.craigmiller160.expensetrackerapi.client.ai.AiClient
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.toEmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.repository.EmbeddedTransactionRepository
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.model.projection.toTransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class EmbeddingServiceTest
@Autowired
constructor(
    private val embeddingService: EmbeddingService,
    private val transactionRepository: TransactionRepository,
    private val dataHelper: DataHelper,
    private val defaultUsers: DefaultUsers,
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val aiClient: AiClient
) {

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `creates and stores embeddings for transactions`() {
    val vectors = listOf(1.2, 3.4, 5.6)

    val transactions =
        (0 until 5)
            .map { dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) }
            .map { it.toTransactionForEmbedding() }

    whenever(aiClient.createEmbedding(transactions.map { it.description }))
        .thenReturn(transactions.map { vectors })

    embeddingService.embedNewTransactions(transactions)

    val actualTransactions =
        embeddedTransactionRepository.getAllTransactions(
            defaultUsers.primaryUser.userTypedId, 0, 50)

    val expectedTransactions =
        transactions.map { txn -> txn.toEmbeddedTransaction(vectors.map { it.toFloat() }) }

    assertThat(actualTransactions)
        .hasSize(expectedTransactions.size)
        .containsOnly(*expectedTransactions.toTypedArray())
  }

  @Test
  fun `creates and stores embedding for single transaction`() {
    val vectors = listOf(1.2, 3.4, 5.6)

    val transaction =
        dataHelper
            .createTransaction(defaultUsers.primaryUser.userTypedId)
            .toTransactionForEmbedding()
    whenever(aiClient.createEmbedding(listOf(transaction.description))).thenReturn(listOf(vectors))
    embeddingService.embedNewTransaction(transaction)

    val actualTransaction =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, transaction.uid)
    assertThat(actualTransaction)
        .isNotNull
        .isEqualTo(transaction.toEmbeddedTransaction(vectors.map { it.toFloat() }))
  }

  @Test
  fun `updates and stores embedding for single transaction`() {
    val vectors = listOf(1.2, 3.4, 5.6)

    val transaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Hello World",
            expenseDate = LocalDate.of(2024, 1, 1),
            userId = defaultUsers.primaryUser.userTypedId,
            isCategorized = true,
            vectors =
                listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
    embeddedTransactionRepository.insertTransaction(transaction)

    val transactionForEmbedding =
        TransactionForEmbedding(
            userId = transaction.userId,
            uid = transaction.uid,
            description = "Other Thing",
            isCategorized = true,
            expenseDate = LocalDate.of(2024, 2, 2))

    whenever(aiClient.createEmbedding(listOf(transactionForEmbedding.description)))
        .thenReturn(listOf(vectors))

    embeddingService.embedAndUpdateTransaction(transactionForEmbedding)

    val actualTransaction =
        embeddedTransactionRepository.getTransactionById(
            transactionForEmbedding.userId, transactionForEmbedding.uid)
    val expectedTransaction =
        transactionForEmbedding.toEmbeddedTransaction(vectors.map { it.toFloat() })
    assertThat(actualTransaction).isEqualTo(expectedTransaction)
  }
}
