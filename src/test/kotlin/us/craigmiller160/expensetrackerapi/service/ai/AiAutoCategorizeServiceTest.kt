package us.craigmiller160.expensetrackerapi.service.ai

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.tuple
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.oauth2.jwt.Jwt
import us.craigmiller160.expensetrackerapi.client.ai.AiClient
import us.craigmiller160.expensetrackerapi.data.repository.AiAutoCategorizeAuditLogRepository
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class AiAutoCategorizeServiceTest
@Autowired
constructor(
    private val aiClient: AiClient,
    private val aiAutoCategorizeService: AiAutoCategorizeService,
    private val aiAutoCategorizeAuditLogRepo: AiAutoCategorizeAuditLogRepository,
    private val dataHelper: DataHelper,
    private val defaultUsers: DefaultUsers
) {
  companion object {
    private val VECTORS = listOf(1f, 2f, 3f)
  }

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()

    val auth = mock<Authentication>()
    val jwt = mock<Jwt>()
    whenever(auth.principal).thenReturn(jwt)
    whenever(jwt.subject).thenReturn(defaultUsers.primaryUser.userId.toString())
    val securityContext = SecurityContextImpl(auth)
    SecurityContextHolder.setContext(securityContext)
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
    SecurityContextHolder.clearContext()
  }

  @Test
  fun `records audit log for successful operation`() {
    val txn1 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    val txn2 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)

    whenever(aiClient.createEmbedding(any()))
        .thenReturn((0 until 2).map { VECTORS.map { it.toDouble() } })

    val operationId = aiAutoCategorizeService.autoCategorizeUnconfirmedTransactions()
    val auditLogs = aiAutoCategorizeAuditLogRepo.getAllAuditLogs()
    assertThat(auditLogs)
        .hasSize(1)
        .extracting("operationId", "success")
        .isEqualTo(listOf(tuple(operationId, true)))
  }

  @Test
  fun `records audit log for failed operation`() {
    val txn1 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    val txn2 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)

    whenever(aiClient.createEmbedding(any())).thenThrow(RuntimeException("Dying"))

    assertThrows<RuntimeException> {
      aiAutoCategorizeService.autoCategorizeUnconfirmedTransactions()
    }
    val auditLogs = aiAutoCategorizeAuditLogRepo.getAllAuditLogs()
    assertThat(auditLogs)
        .hasSize(1)
        .extracting("success", "error")
        .isEqualTo(
            listOf(
                tuple(
                    false,
                    "java.util.concurrent.CompletionException: java.lang.RuntimeException: Dying\njava.lang.RuntimeException: Dying")))
  }
}
