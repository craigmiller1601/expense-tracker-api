package us.craigmiller160.expensetrackerapi.service.ai

import java.util.UUID
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.tuple
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import us.craigmiller160.expensetrackerapi.client.ai.AiClient
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.toEmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.repository.EmbeddedTransactionRepository
import us.craigmiller160.expensetrackerapi.data.model.ChangeTrackedTable
import us.craigmiller160.expensetrackerapi.data.model.ChangeType
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.data.model.projection.toTransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingChangeLogRepository
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class EmbeddingSyncServiceTest
@Autowired
constructor(
    private val dataHelper: DataHelper,
    private val defaultUsers: DefaultUsers,
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val embeddingChangeLogRepository: EmbeddingChangeLogRepository,
    private val transactionRepository: TransactionRepository,
    private val embeddingSyncService: EmbeddingSyncService,
    private val aiClient: AiClient,
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository
) {
  companion object {
    private val VECTORS = listOf(1.1f, 2.2f, 3.3f)
  }

  private lateinit var transaction1: Transaction

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
    setupData()
  }

  private fun setupData() {
    transaction1 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    transaction1.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS).let {
      embeddedTransactionRepository.insertTransaction(it)
    }

    embeddingChangeLogRepository
        .deleteAllChangeLogs() // TODO this line is not affecting anything... why????

    whenever(aiClient.createEmbedding(any())).thenReturn(listOf(VECTORS.map { it.toDouble() }))

    embeddingSyncConfigRepository.setInitialSyncComplete(true)
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `does nothing if initial sync not performed`() {
    val startingCount =
        embeddedTransactionRepository.getTransactionCount(defaultUsers.primaryUser.userTypedId)
    assertThat(startingCount).isEqualTo(1)

    embeddingSyncConfigRepository.setInitialSyncComplete(false)
    dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    embeddingSyncService.sync()

    val count =
        embeddedTransactionRepository.getTransactionCount(defaultUsers.primaryUser.userTypedId)
    assertThat(count).isEqualTo(1)
  }

  @Test
  fun `handles single insert`() {
    val uid = TypedId<TransactionId>()
    val existingEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(defaultUsers.primaryUser.userTypedId, uid)
    assertThat(existingEmbeddedTxn).isNull()

    val txn =
        dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) { base ->
          base.uid = uid
        }
    embeddingSyncService.sync()

    val createdEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, txn.uid)
    assertThat(createdEmbeddedTxn)
        .isNotNull
        .isEqualTo(txn.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS))

    validateChangeLogs(listOf(uid.uuid to ChangeType.INSERT))
  }

  private fun validateChangeLogs(expected: List<Pair<UUID, ChangeType>>) {
    val expectedAsTuples =
        expected
            .map { (uuid, changeType) ->
              tuple(uuid, changeType, ChangeTrackedTable.TRANSACTIONS, true)
            }
            .toTypedArray()

    val allChangeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(allChangeLogs)
        .hasSize(expectedAsTuples.size)
        .extracting({ it.recordId }, { it.type }, { it.tableName }, { it.isSynced })
        .containsExactlyInAnyOrder(*expectedAsTuples)
  }

  @Test
  fun `handles single update`() {
    val existingEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, transaction1.uid)
    assertThat(existingEmbeddedTxn)
        .isNotNull
        .isEqualTo(transaction1.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS))

    transaction1.description = "Updated"
    transactionRepository.save(transaction1)

    embeddingSyncService.sync()

    val updatedEmbeddedTransaction =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, transaction1.uid)
    assertThat(updatedEmbeddedTransaction)
        .isNotNull
        .isEqualTo(transaction1.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS))

    validateChangeLogs(listOf(transaction1.uid.uuid to ChangeType.UPDATE))
  }

  @Test
  fun `handles single delete`() {
    val existingEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, transaction1.uid)
    assertThat(existingEmbeddedTxn).isNotNull

    transactionRepository.deleteById(transaction1.uid)
    embeddingSyncService.sync()

    val deletedEmbeddedTransaction =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, transaction1.uid)
    assertThat(deletedEmbeddedTransaction).isNull()

    validateChangeLogs(listOf(transaction1.uid.uuid to ChangeType.DELETE))
  }

  @Test
  fun `handles insert and update in single operation`() {
    val uid = TypedId<TransactionId>()
    val existingEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(defaultUsers.primaryUser.userTypedId, uid)
    assertThat(existingEmbeddedTxn).isNull()

    val txn =
        dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) { base ->
          base.uid = uid
        }
    txn.description = "updated"
    transactionRepository.saveAndFlush(txn)
    embeddingSyncService.sync()

    val embeddedTransaction =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, txn.uid)
    assertThat(embeddedTransaction)
        .isNotNull
        .isEqualTo(txn.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS))

    validateChangeLogs(listOf(uid.uuid to ChangeType.INSERT, uid.uuid to ChangeType.UPDATE))
  }

  @Test
  fun `handles update and delete in single operation`() {
    val existingEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, transaction1.uid)
    assertThat(existingEmbeddedTxn)
        .isNotNull
        .isEqualTo(transaction1.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS))

    transaction1.description = "Updated"
    transactionRepository.save(transaction1)
    transactionRepository.deleteById(transaction1.uid)

    embeddingSyncService.sync()

    val deletedEmbeddedTransaction =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, transaction1.uid)
    assertThat(deletedEmbeddedTransaction).isNull()

    validateChangeLogs(
        listOf(
            transaction1.uid.uuid to ChangeType.UPDATE, transaction1.uid.uuid to ChangeType.DELETE))
  }

  @Test
  fun `handles insert update and delete in single operation`() {
    val uid = TypedId<TransactionId>()
    val existingEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(defaultUsers.primaryUser.userTypedId, uid)
    assertThat(existingEmbeddedTxn).isNull()

    val txn =
        dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) { base ->
          base.uid = uid
        }
    txn.description = "updated"
    val updatedTransaction = transactionRepository.saveAndFlush(txn)
    transactionRepository.deleteById(updatedTransaction.id)

    embeddingSyncService.sync()

    val missingTransaction =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, txn.uid)
    assertThat(missingTransaction).isNull()

    validateChangeLogs(
        listOf(
            txn.uid.uuid to ChangeType.INSERT,
            txn.uid.uuid to ChangeType.UPDATE,
            txn.uid.uuid to ChangeType.DELETE))
  }

  @Test
  fun `handles insert and delete in single operation`() {
    val uid = TypedId<TransactionId>()
    val existingEmbeddedTxn =
        embeddedTransactionRepository.getTransactionById(defaultUsers.primaryUser.userTypedId, uid)
    assertThat(existingEmbeddedTxn).isNull()

    val txn =
        dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) { base ->
          base.uid = uid
        }
    transactionRepository.deleteById(txn.id)

    // Will throw exception if this is not handled gracefully
    embeddingSyncService.sync()

    val missingTransaction =
        embeddedTransactionRepository.getTransactionById(
            defaultUsers.primaryUser.userTypedId, txn.uid)
    assertThat(missingTransaction).isNull()

    validateChangeLogs(listOf(txn.uid.uuid to ChangeType.INSERT, txn.uid.uuid to ChangeType.DELETE))
  }
}
