package us.craigmiller160.expensetrackerapi.client.ai

import com.aallam.openai.api.core.Usage
import com.aallam.openai.api.embedding.Embedding
import com.aallam.openai.api.embedding.EmbeddingRequest
import com.aallam.openai.api.embedding.EmbeddingResponse
import com.aallam.openai.api.model.ModelId
import com.aallam.openai.client.OpenAI
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.isNull
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import us.craigmiller160.expensetrackerapi.config.OpenaiChatModel
import us.craigmiller160.expensetrackerapi.config.OpenaiEmbeddingModel
import us.craigmiller160.expensetrackerapi.config.OpenaiModels
import us.craigmiller160.expensetrackerapi.config.OpenaiProperties
import us.craigmiller160.expensetrackerapi.config.OpenaiTimeouts

@ExtendWith(MockitoExtension::class)
class OpenaiAiClientTest {
  private val openai = mock<OpenAI>()
  private val objectMapper = jacksonObjectMapper()
  private val openaiProperties =
      OpenaiProperties(
          apiKey = "",
          timeouts = OpenaiTimeouts(socketSeconds = 0, connectSeconds = 0, requestSeconds = 0),
          models =
              OpenaiModels(
                  chat = OpenaiChatModel(name = "chat-model"),
                  embedding = OpenaiEmbeddingModel(name = "embedding-model", dimensions = 300)))
  private val openaiClient = OpenaiAiClient(openai, openaiProperties, objectMapper)

  @Test
  fun `creates embedding for text`() {
    val text = listOf("Hello", "World")
    val embeddings = listOf(listOf(1.1, 2.2), listOf(3.3, 4.4))

    val request =
        EmbeddingRequest(
            model = ModelId(openaiProperties.models.embedding.name),
            dimensions = openaiProperties.models.embedding.dimensions,
            input = text)

    val response =
        EmbeddingResponse(
            usage = Usage(promptTokens = 0, completionTokens = 0, totalTokens = 0),
            embeddings =
                embeddings.mapIndexed { index, vectors ->
                  Embedding(index = index, embedding = vectors)
                })

    val requestCaptor = argumentCaptor<EmbeddingRequest>()

    runBlocking {
      whenever(openai.embeddings(requestCaptor.capture(), isNull())).thenReturn(response)
    }

    val actualEmbeddings = openaiClient.createEmbedding(text)
    assertThat(actualEmbeddings).isEqualTo(embeddings)

    assertThat(requestCaptor.allValues)
        .hasSize(1)
        .first()
        .usingRecursiveComparison()
        .isEqualTo(request)
  }

  @Test
  @Disabled
  fun `auto-categorizes transactions`() {
    TODO()
  }
}
