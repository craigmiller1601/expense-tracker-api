package us.craigmiller160.expensetrackerapi.testutils

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.testcontainers.common.core.AuthenticationHelper

val AuthenticationHelper.TestUserWithToken.userTypedId: TypedId<UserId>
  get() = TypedId(userId)
