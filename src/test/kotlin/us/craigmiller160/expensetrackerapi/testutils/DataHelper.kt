package us.craigmiller160.expensetrackerapi.testutils

import com.github.javafaker.Faker
import java.math.BigDecimal
import java.time.LocalDate
import org.springframework.stereotype.Component
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.ai.repository.EmbeddedTransactionRepository
import us.craigmiller160.expensetrackerapi.data.model.AutoCategorizeRule
import us.craigmiller160.expensetrackerapi.data.model.Category
import us.craigmiller160.expensetrackerapi.data.model.LastRuleApplied
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.data.repository.AiAutoCategorizeAuditLogRepository
import us.craigmiller160.expensetrackerapi.data.repository.AutoCategorizeRuleRepository
import us.craigmiller160.expensetrackerapi.data.repository.CategoryRepository
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingChangeLogRepository
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository
import us.craigmiller160.expensetrackerapi.data.repository.LastRuleAppliedRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.utils.StringToColor

@Component
class DataHelper(
    private val transactionRepository: TransactionRepository,
    private val categoryRepository: CategoryRepository,
    private val autoCategorizeRuleRepository: AutoCategorizeRuleRepository,
    private val lastRuleAppliedRepository: LastRuleAppliedRepository,
    private val embeddingChangeLogRepository: EmbeddingChangeLogRepository,
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository,
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val aiAutoCategorizeAuditLogRepository: AiAutoCategorizeAuditLogRepository
) {
  private val faker = Faker()
  private var internalDate = LocalDate.now().minusDays(100)

  fun createLastRuleApplied(
      userId: TypedId<UserId>,
      transactionId: TypedId<TransactionId>,
      ruleId: TypedId<AutoCategorizeRuleId>
  ): LastRuleApplied =
      lastRuleAppliedRepository.save(
          LastRuleApplied(userId = userId, transactionId = transactionId, ruleId = ruleId))

  fun createTransaction(
      userId: TypedId<UserId>,
      modifier: (Transaction) -> Unit = {}
  ): Transaction {
    internalDate = internalDate.plusDays(1)
    val description = faker.company().name()
    val amount = BigDecimal(faker.commerce().price()) * BigDecimal("-1")
    return Transaction(
            userId = userId, expenseDate = internalDate, description = description, amount = amount)
        .also(modifier)
        .let { transactionRepository.saveAndFlush(it) }
  }

  fun createRule(userId: TypedId<UserId>, categoryId: TypedId<CategoryId>): AutoCategorizeRule {
    val count = autoCategorizeRuleRepository.countAllByUserId(userId)
    val rule =
        AutoCategorizeRule(
            categoryId = categoryId, regex = ".*", ordinal = (count + 1).toInt(), userId = userId)
    return autoCategorizeRuleRepository.saveAndFlush(rule)
  }

  fun createCategory(userId: TypedId<UserId>, name: String): Category =
      categoryRepository.saveAndFlush(
          Category(userId = userId, name = name, color = StringToColor.get(name)))

  fun createDefaultCategories(userId: TypedId<UserId>): List<Category> =
      listOf(
          createCategory(userId, "Shopping"),
          createCategory(userId, "Restaurant"),
          createCategory(userId, "Entertainment"))

  fun cleanData() {
    lastRuleAppliedRepository.deleteAll()
    autoCategorizeRuleRepository.deleteAll()
    transactionRepository.deleteAll()
    categoryRepository.deleteAll()
    embeddingChangeLogRepository.deleteAllChangeLogs()
    embeddingSyncConfigRepository.resetEmbeddingSyncConfig()
    embeddedTransactionRepository.deleteAllTransactionsAllTenants()
    aiAutoCategorizeAuditLogRepository.deleteAllAuditLogs()
  }
}
