package us.craigmiller160.expensetrackerapi.web.controller

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import us.craigmiller160.expensetrackerapi.client.ai.AiClient
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.CategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.toPreviousTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.toUncategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.toEmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.repository.EmbeddedTransactionRepository
import us.craigmiller160.expensetrackerapi.data.model.TransactionView
import us.craigmiller160.expensetrackerapi.data.model.YesNoFilter
import us.craigmiller160.expensetrackerapi.data.model.projection.toTransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId
import us.craigmiller160.expensetrackerapi.web.types.SortDirection
import us.craigmiller160.expensetrackerapi.web.types.transaction.SearchTransactionsRequest
import us.craigmiller160.expensetrackerapi.web.types.transaction.TransactionSortKey
import us.craigmiller160.expensetrackerapi.web.types.transaction.toPageable

@ExpenseTrackerIntegrationTest
class AiControllerTest
@Autowired
constructor(
    private val mockMvc: MockMvc,
    private val defaultUsers: DefaultUsers,
    private val dataHelper: DataHelper,
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val aiClient: AiClient,
    private val transactionRepository: TransactionRepository
) {
  companion object {
    private val VECTORS = listOf(1f, 2f, 3f)
  }

  @BeforeEach
  fun setup() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun cleanup() {
    dataHelper.cleanData()
  }

  private fun getUnconfirmedTransactions(batchNumber: Int): List<TransactionView> {
    val req =
        SearchTransactionsRequest(
            pageNumber = batchNumber,
            pageSize = 50,
            sortKey = TransactionSortKey.EXPENSE_DATE,
            sortDirection = SortDirection.DESC,
            startDate = null,
            endDate = null,
            confirmed = YesNoFilter.NO,
            categorized = YesNoFilter.ALL)
    return doSearch(req)
  }

  private fun getConfirmedTransactions(): List<TransactionView> {
    val req =
        SearchTransactionsRequest(
            pageNumber = 0,
            pageSize = 1000,
            sortKey = TransactionSortKey.EXPENSE_DATE,
            sortDirection = SortDirection.DESC,
            startDate = null,
            endDate = null,
            confirmed = YesNoFilter.YES,
            categorized = YesNoFilter.ALL)
    return doSearch(req)
  }

  private fun doSearch(req: SearchTransactionsRequest): List<TransactionView> =
      transactionRepository
          .searchForTransactions(req, defaultUsers.primaryUser.userTypedId, req.toPageable())
          .content

  @Test
  fun `auto-categorizes unconfirmed transactions with ai`() {
    val categories =
        (0 until 10).map { index ->
          dataHelper.createCategory(defaultUsers.primaryUser.userTypedId, "Category-$index")
        }
    (0 until 60).map {
      dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) { txn ->
        txn.confirmed = false
      }
    }
    (0 until 3).map {
      dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId) { txn ->
        txn.confirmed = true
        txn.categoryId = categories[0].uid
      }
    }
    val confirmedTransactions = getConfirmedTransactions()

    val transactionsBatch1 = getUnconfirmedTransactions(0)
    val transactionsBatch2 = getUnconfirmedTransactions(1)

    confirmedTransactions
        .map { it.toTransactionForEmbedding().toEmbeddedTransaction(VECTORS) }
        .let { embeddedTransactionRepository.insertAllTransactions(it) }

    whenever(aiClient.createEmbedding(any())).thenReturn(listOf(VECTORS.map { it.toDouble() }))

    whenever(
            aiClient.autoCategorizeTransactions(
                categories.map { it.name },
                confirmedTransactions.map { it.toPreviousTransaction() }.sorted(),
                transactionsBatch1.map { it.toUncategorizedTransaction() }))
        .thenReturn(
            transactionsBatch1.map { txn ->
              CategorizedTransaction(uid = txn.uid, category = categories[0].name)
            })
    whenever(
            aiClient.autoCategorizeTransactions(
                categories.map { it.name },
                confirmedTransactions.map { it.toPreviousTransaction() }.sorted(),
                transactionsBatch2.map { it.toUncategorizedTransaction() }))
        .thenReturn(
            transactionsBatch2.map { txn ->
              CategorizedTransaction(uid = txn.uid, category = categories[0].name)
            })

    mockMvc
        .post("/ai/auto-categorize-unconfirmed") {
          header("Authorization", "Bearer ${defaultUsers.primaryUser.token}")
        }
        .andExpect { status { isNoContent() } }

    val allTransactions = transactionRepository.findAll()
    val allCategorizedTransactions = allTransactions.filter { it.categoryId != null }
    assertThat(allCategorizedTransactions).hasSize(allTransactions.size)
    val categoryIds = allTransactions.map { it.categoryId }.toSet()
    assertThat(categoryIds).isEqualTo(setOf(categories[0].uid))
  }
}
