package us.craigmiller160.expensetrackerapi.web.controller

import java.time.LocalDate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.repository.EmbeddedTransactionRepository
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class AiAdminControllerTest
@Autowired
constructor(
    private val transactionRepository: TransactionRepository,
    private val mockMvc: MockMvc,
    private val defaultUsers: DefaultUsers,
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository,
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val dataHelper: DataHelper
) {

  @BeforeEach
  fun setup() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `it cannot delete all embedded transactions across all tenants without admin role`() {
    mockMvc
        .delete("/ai/admin/all-tenants/embedding/all-transactions") {
          header("Authorization", "Bearer ${defaultUsers.secondaryUser.token}")
        }
        .andExpect { status { isForbidden() } }
  }

  @Test
  fun `it deletes all embedded transactions across all tenants`() {
    embeddingSyncConfigRepository.setInitialSyncComplete(true)
    val initCompleteBefore = embeddingSyncConfigRepository.isInitialSyncComplete()
    assertThat(initCompleteBefore).isTrue()

    val primaryTransaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Primary Tenant Transaction",
            expenseDate = LocalDate.of(2024, 1, 1),
            userId = defaultUsers.primaryUser.userTypedId,
            isCategorized = true,
            vectors =
                listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
    val secondaryTransaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Other Tenant Transaction",
            expenseDate = LocalDate.of(2024, 1, 2),
            userId = defaultUsers.secondaryUser.userTypedId,
            isCategorized = true,
            vectors =
                listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
    embeddedTransactionRepository.insertAllTransactions(
        listOf(primaryTransaction, secondaryTransaction))
    val user1BeforeCount =
        embeddedTransactionRepository.getTransactionCount(defaultUsers.primaryUser.userTypedId)
    val user2BeforeCount =
        embeddedTransactionRepository.getTransactionCount(defaultUsers.secondaryUser.userTypedId)
    assertThat(user1BeforeCount).isEqualTo(1L)
    assertThat(user2BeforeCount).isEqualTo(1L)

    val allTenantsBefore = embeddedTransactionRepository.getAllTenants()
    assertThat(allTenantsBefore).hasSize(2)

    mockMvc
        .delete("/ai/admin/all-tenants/embedding/all-transactions") {
          header("Authorization", "Bearer ${defaultUsers.primaryUser.token}")
        }
        .andExpect { status { isNoContent() } }

    val allTenantsAfter = embeddedTransactionRepository.getAllTenants()
    assertThat(allTenantsAfter).hasSize(0)

    val initCompleteAfter = embeddingSyncConfigRepository.isInitialSyncComplete()
    assertThat(initCompleteAfter).isFalse()
  }
}
