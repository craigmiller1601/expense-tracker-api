package us.craigmiller160.expensetrackerapi.data.model

import org.springframework.data.jpa.repository.JpaRepository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId

interface ResidentRepository : JpaRepository<Resident, TypedId<ResidentId>>
