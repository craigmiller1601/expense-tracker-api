package us.craigmiller160.expensetrackerapi.data.model

import jakarta.persistence.Entity
import jakarta.persistence.Table
import us.craigmiller160.expensetrackerapi.data.model.core.ImmutableTableEntity

@Entity
@Table(name = "countries")
class Country(var name: String = "") : ImmutableTableEntity<CountryId>()
