package us.craigmiller160.expensetrackerapi.data.model

import jakarta.persistence.Entity
import jakarta.persistence.Table
import us.craigmiller160.expensetrackerapi.data.model.core.MutableTableEntity

@Entity
@Table(name = "residents")
class Resident(var name: String = "") : MutableTableEntity<ResidentId>()
