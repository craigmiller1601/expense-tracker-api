package us.craigmiller160.expensetrackerapi.data.model

import java.time.ZonedDateTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.InvalidDataAccessApiUsageException
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest

/** Special test class for validating that the TypedId & parent model types work. */
@ExpenseTrackerIntegrationTest
class ModelValidationTest
@Autowired
constructor(
    private val countryRepository: CountryRepository,
    private val residentRepository: ResidentRepository
) {
  private val now = ZonedDateTime.now()

  @BeforeEach
  fun beforeEach() {
    clean()
  }

  @AfterEach
  fun afterEach() {
    clean()
  }

  private fun clean() {
    countryRepository.deleteAll()
    residentRepository.deleteAll()
  }

  @Test
  fun `immutable entity inserts but cannot be updated`() {
    val country = Country("USA")
    countryRepository.save(country)

    assertThat(country.created).isAfterOrEqualTo(now)

    val dbCountry = countryRepository.findById(country.uid).orElseThrow()
    assertThat(dbCountry).isEqualTo(country)

    val newCountry = dbCountry.apply { name = "CAN" }
    assertThat(newCountry.uid).isEqualTo(country.uid)
    // Just confirming the apply does a mutation
    assertThat(newCountry.name).isEqualTo(dbCountry.name)

    val ex =
        assertThrows<InvalidDataAccessApiUsageException> {
          countryRepository.saveAndFlush(newCountry)
        }
    assertThat(ex.cause).isNotNull.isInstanceOf(IllegalStateException::class.java)
  }

  @Test
  fun `mutable entity inserts & can be updated`() {
    val resident = Resident("Bob")
    residentRepository.save(resident)

    assertThat(resident.created).isAfterOrEqualTo(now)
    assertThat(resident.updated).isAfterOrEqualTo(resident.created)
    val originalUpdated = resident.updated

    val dbResident = residentRepository.findById(resident.uid).orElseThrow()
    assertThat(dbResident).isEqualTo(resident)
    assertThat(dbResident.version).isEqualTo(1)

    Thread.sleep(100)

    val newResident = dbResident.apply { name = "Sally" }
    residentRepository.save(newResident)

    val dbResident2 = residentRepository.findById(resident.uid).orElseThrow()
    assertThat(dbResident2.name).isEqualTo(newResident.name)
    assertThat(dbResident2.updated).isAfter(originalUpdated)
    assertThat(dbResident2.version).isEqualTo(2)
  }
}
