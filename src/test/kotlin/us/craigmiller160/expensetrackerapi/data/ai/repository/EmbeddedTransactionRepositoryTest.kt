package us.craigmiller160.expensetrackerapi.data.ai.repository

import java.time.LocalDate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class EmbeddedTransactionRepositoryTest
@Autowired
constructor(
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val defaultUsers: DefaultUsers,
    private val dataHelper: DataHelper,
) {
  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `inserts and retrieves a single transaction`() {
    val transaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Hello World",
            expenseDate = LocalDate.of(2024, 1, 1),
            vectors = listOf(22.4f, 53.6f, 82.2f),
            userId = defaultUsers.primaryUser.userTypedId,
            isCategorized = true)

    embeddedTransactionRepository.insertTransaction(transaction)
    val foundTransaction =
        embeddedTransactionRepository.getTransactionById(transaction.userId, transaction.uid)
    assertThat(foundTransaction).isNotNull.isEqualTo(transaction)
  }

  @Test
  fun `can update isCategorized for record`() {
    val transaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Hello World",
            expenseDate = LocalDate.of(2024, 1, 1),
            vectors = listOf(22.4f, 53.6f, 82.2f),
            userId = defaultUsers.primaryUser.userTypedId,
            isCategorized = false)
    embeddedTransactionRepository.insertTransaction(transaction)

    embeddedTransactionRepository.updateTransactionIsCategorized(
        defaultUsers.primaryUser.userTypedId, transaction.uid, true)

    val actualTransaction =
        embeddedTransactionRepository.getTransactionById(transaction.userId, transaction.uid)
    assertThat(actualTransaction).isNotNull.hasFieldOrPropertyWithValue("isCategorized", true)
  }

  @Test
  fun `inserts, counts, and retrieves a batch of transactions`() {
    val transactions =
        (0 until 50).map { index ->
          EmbeddedTransaction(
              uid = TypedId(),
              description = "Hello $index",
              expenseDate = LocalDate.of(2024, 1, 1).plusDays(index.toLong()),
              userId = defaultUsers.primaryUser.userTypedId,
              isCategorized = index % 2 == 0,
              vectors =
                  listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
        }

    val otherTenantTransaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Other Tenant Transaction",
            expenseDate = LocalDate.of(2024, 1, 1),
            userId = defaultUsers.secondaryUser.userTypedId,
            isCategorized = false,
            vectors =
                listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))

    embeddedTransactionRepository.insertAllTransactions(
        transactions + listOf(otherTenantTransaction))

    val count =
        embeddedTransactionRepository.getTransactionCount(defaultUsers.primaryUser.userTypedId)
    assertThat(count).isEqualTo(transactions.size.toLong())

    val actualTransactionsPage1 =
        embeddedTransactionRepository.getAllTransactions(
            defaultUsers.primaryUser.userTypedId, 0, 25)

    val expectedTransactionsPage1 = transactions.slice(0 until 25).sortedBy { it.expenseDate }

    assertThat(actualTransactionsPage1)
        .hasSize(expectedTransactionsPage1.size)
        .containsExactlyElementsOf(expectedTransactionsPage1)

    val actualTransactionsPage2 =
        embeddedTransactionRepository.getAllTransactions(
            defaultUsers.primaryUser.userTypedId, 1, 25)

    val expectedTransactionsPage2 = transactions.slice(25 until 50).sortedBy { it.expenseDate }

    assertThat(actualTransactionsPage2)
        .hasSize(expectedTransactionsPage2.size)
        .containsExactlyElementsOf(expectedTransactionsPage2)
  }

  @Test
  fun `it gets all unique tenant IDs`() {
    val primaryTransaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Primary Tenant Transaction",
            expenseDate = LocalDate.of(2024, 1, 1),
            userId = defaultUsers.primaryUser.userTypedId,
            isCategorized = true,
            vectors =
                listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
    val secondaryTransaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Other Tenant Transaction",
            expenseDate = LocalDate.of(2024, 1, 2),
            userId = defaultUsers.secondaryUser.userTypedId,
            isCategorized = false,
            vectors =
                listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
    embeddedTransactionRepository.insertTransaction(primaryTransaction)
    embeddedTransactionRepository.insertTransaction(secondaryTransaction)

    val tenants = embeddedTransactionRepository.getAllTenants()
    assertThat(tenants)
        .hasSize(2)
        .containsOnly(defaultUsers.primaryUser.userTypedId, defaultUsers.secondaryUser.userTypedId)
  }

  @Test
  fun `gets all transactions for the provided list of IDs`() {
    val transactions =
        (0 until 5).map { index ->
          EmbeddedTransaction(
              uid = TypedId(),
              description = "Hello $index",
              expenseDate = LocalDate.of(2024, 1, 1).plusDays(index.toLong()),
              userId = defaultUsers.primaryUser.userTypedId,
              isCategorized = index % 2 == 0,
              vectors =
                  listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
        }
    embeddedTransactionRepository.insertAllTransactions(transactions)
    val ids = transactions.slice(0 until 2).map { it.uid }
    val results =
        embeddedTransactionRepository.getAllTransactionsById(
            defaultUsers.primaryUser.userTypedId, ids)
    assertThat(results).hasSize(2).containsExactlyInAnyOrder(transactions[0], transactions[1])
  }

  @Test
  fun `updates transaction`() {
    val makeVectors: () -> List<Float> = {
      listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat())
    }

    val transaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Hello World",
            expenseDate = LocalDate.of(2024, 1, 1),
            userId = defaultUsers.primaryUser.userTypedId,
            isCategorized = false,
            vectors = makeVectors())
    embeddedTransactionRepository.insertTransaction(transaction)

    val newTransaction =
        EmbeddedTransaction(
            uid = transaction.uid,
            userId = transaction.userId,
            description = "Another Thing",
            expenseDate = LocalDate.of(2024, 2, 2),
            isCategorized = true,
            vectors = makeVectors())
    embeddedTransactionRepository.updateTransaction(newTransaction)

    val actualTransaction =
        embeddedTransactionRepository.getTransactionById(newTransaction.userId, newTransaction.uid)
    assertThat(actualTransaction).isEqualTo(newTransaction)
  }

  @Test
  fun `deletes transaction`() {
    val transaction =
        EmbeddedTransaction(
            uid = TypedId(),
            description = "Hello World",
            expenseDate = LocalDate.of(2024, 1, 1),
            userId = defaultUsers.primaryUser.userTypedId,
            isCategorized = true,
            vectors =
                listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
    embeddedTransactionRepository.insertTransaction(transaction)

    embeddedTransactionRepository.deleteTransactionById(transaction.userId, transaction.uid)

    val result =
        embeddedTransactionRepository.getTransactionById(transaction.userId, transaction.uid)
    assertNull(result)
  }

  @Test
  fun `deletes all transactions by id`() {
    val transactions =
        (0 until 5).map { index ->
          EmbeddedTransaction(
              uid = TypedId(),
              description = "Hello $index",
              expenseDate = LocalDate.of(2024, 1, 1).plusDays(index.toLong()),
              userId = defaultUsers.primaryUser.userTypedId,
              isCategorized = index % 2 == 0,
              vectors =
                  listOf(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat()))
        }
    embeddedTransactionRepository.insertAllTransactions(transactions)

    val countBefore =
        embeddedTransactionRepository.getTransactionCount(defaultUsers.primaryUser.userTypedId)
    assertThat(countBefore).isEqualTo(5)

    embeddedTransactionRepository.deleteAllTransactionsById(
        defaultUsers.primaryUser.userTypedId, transactions.map { it.uid })

    val countAfter =
        embeddedTransactionRepository.getTransactionCount(defaultUsers.primaryUser.userTypedId)
    assertThat(countAfter).isEqualTo(0)
  }

  @Test
  fun `returns no results when searching for non-existent tenant`() {
    val list =
        embeddedTransactionRepository.searchForTransactions(
            defaultUsers.tertiaryUser.userTypedId, listOf(), LocalDate.now(), LocalDate.now(), 10)
    assertThat(list).isEmpty()
  }
}
