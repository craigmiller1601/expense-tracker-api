package us.craigmiller160.expensetrackerapi.data.ai.weaviate

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.weaviate.client.base.Result
import io.weaviate.client.base.WeaviateErrorResponse
import io.weaviate.client.v1.batch.model.ObjectGetResponse
import io.weaviate.client.v1.batch.model.ObjectsGetResponseAO2Result
import io.weaviate.client.v1.data.model.WeaviateObject
import java.util.UUID
import java.util.stream.Stream
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class WeaviateResultExtTest {

  companion object {
    private val objectMapper = jacksonObjectMapper()

    @JvmStatic
    fun getOrThrowArgs(): Stream<GetOrThrowArgs> {
      val singleObject =
          WeaviateObject.builder()
              .id(UUID.randomUUID().toString())
              .properties(mapOf("hello" to "world"))
              .build()
      val singleSuccessResult = Result(200, singleObject, null)

      val singleErrorMessage = "Single error"
      val multiErrorMessage = "Multi error"

      val singleError =
          WeaviateErrorResponse.builder().code(400).message(singleErrorMessage).build()
      val singleErrorResult = Result(400, null, singleError)

      val batchResponseSuccess =
          (0 until 3)
              .map {
                ObjectGetResponse().apply {
                  className = "Class ${UUID.randomUUID()}"
                  result = ObjectsGetResponseAO2Result().apply { status = "success" }
                }
              }
              .toTypedArray()

      val multiErrorResponse =
          mapOf("error" to listOf(mapOf("message" to multiErrorMessage))).let {
            objectMapper.convertValue(it, ObjectsGetResponseAO2Result.ErrorResponse::class.java)
          }

      val batchResponseFailure =
          (0 until 3)
              .map {
                ObjectGetResponse().apply {
                  className = "Class ${UUID.randomUUID()}"
                  result =
                      ObjectsGetResponseAO2Result().apply {
                        status = "failed"
                        errors = multiErrorResponse
                      }
                }
              }
              .toTypedArray()

      val batchSuccessResult = Result(200, batchResponseSuccess, null)
      val batchErrorResult = Result(400, batchResponseFailure, null)

      val exceptionBatchMessage =
          (0 until 3).joinToString("\n") { index -> "Exception $index = $multiErrorMessage" }

      return Stream.of(
          GetOrThrowArgs(singleSuccessResult, kotlin.Result.success(singleObject)),
          GetOrThrowArgs(
              singleErrorResult, kotlin.Result.failure<Any>(WeaviateException(singleErrorMessage))),
          GetOrThrowArgs(batchSuccessResult, kotlin.Result.success(batchResponseSuccess)),
          GetOrThrowArgs(
              batchErrorResult,
              kotlin.Result.failure<Any>(WeaviateException(exceptionBatchMessage))))
    }
  }

  @ParameterizedTest
  @MethodSource("getOrThrowArgs")
  fun `handle rest api getOrThrow`(args: GetOrThrowArgs) {
    val actualKotlinResult = kotlin.runCatching { args.weaviateResult.getOrThrow() }

    if (args.kotlinResult.isSuccess) {
      assertTrue(actualKotlinResult.isSuccess)
      assertThat(actualKotlinResult.getOrNull()).isNotNull.isEqualTo(args.kotlinResult.getOrNull())
    } else {
      assertTrue(actualKotlinResult.isFailure)
      assertThat(actualKotlinResult.exceptionOrNull())
          .isNotNull()
          .isInstanceOf(args.kotlinResult.exceptionOrNull()!!::class.java)
          .hasFieldOrPropertyWithValue("message", args.kotlinResult.exceptionOrNull()!!.message)
    }
  }
}

data class GetOrThrowArgs(val weaviateResult: Result<*>, val kotlinResult: kotlin.Result<*>) {
  override fun toString(): String {
    val weaviateResultType = if (weaviateResult.result is Array<*>) "single" else "batch"
    val errorType = if (kotlinResult.isSuccess) "no errors" else "errors"
    return "Weaviate $weaviateResultType result with $errorType"
  }
}
