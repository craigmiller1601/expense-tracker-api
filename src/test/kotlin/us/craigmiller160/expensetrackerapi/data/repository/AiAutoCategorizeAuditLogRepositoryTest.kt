package us.craigmiller160.expensetrackerapi.data.repository

import java.time.ZoneId
import java.time.ZonedDateTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AiAutoCategorizeAuditLogOperationId
import us.craigmiller160.expensetrackerapi.data.model.AiAutoCategorizeAuditLog
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper

@ExpenseTrackerIntegrationTest
class AiAutoCategorizeAuditLogRepositoryTest
@Autowired
constructor(
    private val repo: AiAutoCategorizeAuditLogRepository,
    private val dataHelper: DataHelper
) {
  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `can create audit logs`() {
    val operationId = TypedId<AiAutoCategorizeAuditLogOperationId>()
    val auditLog1 =
        AiAutoCategorizeAuditLog(
            id = TypedId(),
            operationId = operationId,
            batchIndex = 0,
            timestamp = ZonedDateTime.now(ZoneId.of("UTC")),
            success = true,
            error = null,
            aiResponse = """{"hello": "world"}""")
    val auditLog2 =
        AiAutoCategorizeAuditLog(
            id = TypedId(),
            operationId = operationId,
            batchIndex = 0,
            timestamp = ZonedDateTime.now(ZoneId.of("UTC")),
            success = false,
            aiResponse = null,
            error = "The error happened")

    repo.createAuditLog(auditLog1)
    repo.createAuditLog(auditLog2)

    val allAuditLogs = repo.getAllAuditLogs()
    assertThat(allAuditLogs)
        .hasSize(2)
        .usingRecursiveFieldByFieldElementComparatorIgnoringFields("timestamp")
        .containsExactly(auditLog2, auditLog1)
  }
}
