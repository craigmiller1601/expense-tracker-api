package us.craigmiller160.expensetrackerapi.data.repository

import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.UUID
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.tuple
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.data.model.ChangeTrackedTable
import us.craigmiller160.expensetrackerapi.data.model.ChangeType
import us.craigmiller160.expensetrackerapi.data.model.EmbeddingChangeLog
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class EmbeddingChangeLogRepositoryTest
@Autowired
constructor(
    private val embeddingChangeLogRepository: EmbeddingChangeLogRepository,
    private val defaultUsers: DefaultUsers,
    private val dataHelper: DataHelper
) {

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `can insert and retrieve change logs`() {
    val now = ZonedDateTime.now(ZoneId.of("UTC"))
    val changeLog1 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.INSERT,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = false,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(1),
        )
    val changeLog2 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.UPDATE,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = true,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(2),
        )
    embeddingChangeLogRepository.createChangeLog(changeLog1)
    embeddingChangeLogRepository.createChangeLog(changeLog2)

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs)
        .hasSize(2)
        .usingRecursiveFieldByFieldElementComparatorIgnoringFields(
            "changeTimestamp", "syncTimestamp")
        .containsExactly(changeLog2, changeLog1)
  }

  @Test
  fun `can get all not-synced change logs`() {
    val now = ZonedDateTime.now(ZoneId.of("UTC"))
    val changeLog1 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.INSERT,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = false,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(1),
        )
    val changeLog2 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.UPDATE,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = true,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(2),
        )

    embeddingChangeLogRepository.createChangeLog(changeLog1)
    embeddingChangeLogRepository.createChangeLog(changeLog2)

    val allNotSyncedChangeLogs = embeddingChangeLogRepository.getAllNotSyncedChangeLogs()
    assertThat(allNotSyncedChangeLogs).hasSize(1)
    assertThat(allNotSyncedChangeLogs.first().uid).isEqualTo(changeLog1.uid)
  }

  @Test
  fun `can mark change logs as synced`() {
    val now = ZonedDateTime.now(ZoneId.of("UTC"))
    val changeLog1 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.INSERT,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = false,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(1),
        )
    val changeLog2 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.UPDATE,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = false,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(2),
        )

    embeddingChangeLogRepository.createChangeLog(changeLog1)
    embeddingChangeLogRepository.createChangeLog(changeLog2)

    embeddingChangeLogRepository.markChangeLogsAsSynced(listOf(changeLog1.uid, changeLog2.uid))

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs)
        .hasSize(2)
        .extracting({ it.uid.uuid }, { it.isSynced })
        .containsExactly(
            *arrayOf(tuple(changeLog2.uid.uuid, true), tuple(changeLog1.uid.uuid, true)))

    assertThat(changeLogs.first().syncTimestamp).isNotNull()
    assertThat(changeLogs[1].syncTimestamp).isNotNull()
  }

  @Test
  fun `can delete all synced change logs older than a timestamp`() {
    val now = ZonedDateTime.now(ZoneId.of("UTC"))
    val changeLog1 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.INSERT,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = true,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(1),
        )
    val changeLog2 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.UPDATE,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = false,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(2),
        )
    val changeLog3 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.INSERT,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = false,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(3),
        )
    val changeLog4 =
        EmbeddingChangeLog(
            uid = TypedId(),
            type = ChangeType.UPDATE,
            tableName = ChangeTrackedTable.TRANSACTIONS,
            isSynced = false,
            recordId = UUID.randomUUID(),
            userId = defaultUsers.primaryUser.userTypedId,
            changeTimestamp = now.plusHours(4),
        )

    embeddingChangeLogRepository.createChangeLog(changeLog1)
    embeddingChangeLogRepository.createChangeLog(changeLog2)
    embeddingChangeLogRepository.createChangeLog(changeLog3)
    embeddingChangeLogRepository.createChangeLog(changeLog4)

    embeddingChangeLogRepository.deleteSyncedChangeLogsOlderThanTimestamp(
        changeLog3.changeTimestamp)

    val allChangeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(allChangeLogs)
        .hasSize(3)
        .usingRecursiveFieldByFieldElementComparatorIgnoringFields(
            "changeTimestamp", "syncTimestamp")
        .containsExactly(changeLog4, changeLog3, changeLog2)
  }
}
