package us.craigmiller160.expensetrackerapi.data.repository

import jakarta.transaction.Transactional
import java.time.ZoneId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.ComponentScan
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionAndCategory
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
@ComponentScan("us.craigmiller160.expensetrackerapi.data.repository")
class TransactionRepositoryCustomJdbcImplTest
@Autowired
constructor(
    private val transactionRepository: TransactionRepository,
    private val dataHelper: DataHelper,
    private val defaultUsers: DefaultUsers,
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val testTransactionalService: TestTransactionalService,
) {

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `rolls back transaction for jdbc template`() {
    assertDoesNotThrow { testTransactionalService.operation(false) }

    val count =
        jdbcTemplate.queryForObject(
            "select count(*) from transactions", MapSqlParameterSource(), Long::class.java)
    assertThat(count).isEqualTo(1L)

    assertThrows<RuntimeException> { testTransactionalService.operation(true) }

    val count2 =
        jdbcTemplate.queryForObject(
            "select count(*) from transactions", MapSqlParameterSource(), Long::class.java)
    assertThat(count2).isEqualTo(1L)
  }

  @Test
  fun `gets all transactions for all users with pagination`() {
    (0 until 20).map { index ->
      val userId =
          if (index % 2 == 0) {
            defaultUsers.primaryUser.userTypedId
          } else {
            defaultUsers.secondaryUser.userTypedId
          }
      dataHelper.createTransaction(userId)
    }
    val transactions =
        transactionRepository
            .findAll()
            .map { txn ->
              txn.apply {
                created = this.created.withZoneSameInstant(ZoneId.of("UTC"))
                updated = this.updated.withZoneSameInstant(ZoneId.of("UTC"))
              }
            }
            .sortedBy { it.created }

    val page1 = transactionRepository.getAllTransactionsAllUsers(0, 10)
    assertThat(page1)
        .hasSize(10)
        .usingRecursiveComparison()
        .ignoringFields("innerIsNew")
        .isEqualTo(transactions.slice(0 until 10))

    val page2 = transactionRepository.getAllTransactionsAllUsers(1, 10)
    assertThat(page2)
        .hasSize(10)
        .usingRecursiveComparison()
        .ignoringFields("innerIsNew")
        .isEqualTo(transactions.slice(10 until 20))
  }

  @Test
  fun `categorizes all transactions`() {
    val cat1 = dataHelper.createCategory(defaultUsers.primaryUser.userTypedId, "Entertainment")
    val cat2 = dataHelper.createCategory(defaultUsers.primaryUser.userTypedId, "Restaurants")
    val cat3 = dataHelper.createCategory(defaultUsers.secondaryUser.userTypedId, "Miscellaneous")

    val txn1 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    val txn2 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    val txn3 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    val txn4 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    val txn5 = dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    val txn6 = dataHelper.createTransaction(defaultUsers.secondaryUser.userTypedId)

    val txnsToCategorize =
        listOf(
            TransactionAndCategory(txn1.uid, cat1.uid),
            TransactionAndCategory(txn2.uid, cat2.uid),
            TransactionAndCategory(txn3.uid, cat1.uid),
            TransactionAndCategory(txn4.uid, cat2.uid),
            TransactionAndCategory(txn5.uid, cat3.uid),
            TransactionAndCategory(txn6.uid, cat1.uid))

    val count =
        transactionRepository.categorizeAllTransactions(
            defaultUsers.primaryUser.userTypedId, txnsToCategorize)
    assertThat(count).isEqualTo(4)

    val resultTxn1 = transactionRepository.findById(txn1.uid).orElseThrow()
    assertThat(resultTxn1).hasFieldOrPropertyWithValue("categoryId", cat1.uid)

    val resultTxn2 = transactionRepository.findById(txn2.uid).orElseThrow()
    assertThat(resultTxn2).hasFieldOrPropertyWithValue("categoryId", cat2.uid)

    val resultTxn3 = transactionRepository.findById(txn3.uid).orElseThrow()
    assertThat(resultTxn3).hasFieldOrPropertyWithValue("categoryId", cat1.uid)

    val resultTxn4 = transactionRepository.findById(txn4.id).orElseThrow()
    assertThat(resultTxn4).hasFieldOrPropertyWithValue("categoryId", cat2.uid)

    val resultTxn5 = transactionRepository.findById(txn5.id).orElseThrow()
    assertThat(resultTxn5).hasFieldOrPropertyWithValue("categoryId", null)

    val resultTxn6 = transactionRepository.findById(txn6.id).orElseThrow()
    assertThat(resultTxn6).hasFieldOrPropertyWithValue("categoryId", null)
  }
}

@Service
class TestTransactionalService(
    private val dataHelper: DataHelper,
    private val defaultUsers: DefaultUsers
) {
  @Transactional
  fun operation(throwException: Boolean) {
    dataHelper.createTransaction(defaultUsers.primaryUser.userTypedId)
    if (throwException) {
      throw FailTestException()
    }
  }
}

class FailTestException : RuntimeException()
