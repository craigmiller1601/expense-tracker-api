package us.craigmiller160.expensetrackerapi.data.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper

@ExpenseTrackerIntegrationTest
class EmbeddingSyncConfigRepositoryTest
@Autowired
constructor(
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository,
    private val dataHelper: DataHelper
) {

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `correctly handles embedding sync config`() {
    val isInit = embeddingSyncConfigRepository.isInitialSyncComplete()
    assertThat(isInit).isFalse()

    embeddingSyncConfigRepository.setInitialSyncComplete(true)
    val isInit2 = embeddingSyncConfigRepository.isInitialSyncComplete()
    assertThat(isInit2).isTrue()

    embeddingSyncConfigRepository.resetEmbeddingSyncConfig()
    val isInit3 = embeddingSyncConfigRepository.isInitialSyncComplete()
    assertThat(isInit3).isFalse()
  }
}
