package us.craigmiller160.expensetrackerapi.data

import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.tuple
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import us.craigmiller160.expensetrackerapi.data.model.ChangeTrackedTable
import us.craigmiller160.expensetrackerapi.data.model.ChangeType
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingChangeLogRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.testcore.ExpenseTrackerIntegrationTest
import us.craigmiller160.expensetrackerapi.testutils.DataHelper
import us.craigmiller160.expensetrackerapi.testutils.DefaultUsers
import us.craigmiller160.expensetrackerapi.testutils.userTypedId

@ExpenseTrackerIntegrationTest
class EmbeddingChangeLogTriggerTest
@Autowired
constructor(
    private val transactionRepository: TransactionRepository,
    private val embeddingChangeLogRepository: EmbeddingChangeLogRepository,
    private val defaultUsers: DefaultUsers,
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val dataHelper: DataHelper,
) {

  private lateinit var transaction1: Transaction
  private lateinit var transaction2: Transaction

  @BeforeEach
  fun beforeEach() {
    dataHelper.cleanData()
    transaction1 =
        Transaction(
            userId = defaultUsers.primaryUser.userTypedId,
            expenseDate = LocalDate.now(),
            description = "Hello",
            amount = BigDecimal("100"))
    transaction2 =
        Transaction(
            userId = defaultUsers.primaryUser.userTypedId,
            expenseDate = LocalDate.now(),
            description = "World",
            amount = BigDecimal("20"))
    transactionRepository.saveAllAndFlush(listOf(transaction1, transaction2))
    embeddingChangeLogRepository.deleteAllChangeLogs()
  }

  @AfterEach
  fun afterEach() {
    dataHelper.cleanData()
  }

  @Test
  fun `adds entry to change log on insert`() {
    val changeLogsBefore = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogsBefore).hasSize(0)

    val transaction =
        Transaction(
            userId = defaultUsers.primaryUser.userTypedId,
            expenseDate = LocalDate.now(),
            description = "Hello2",
            amount = BigDecimal("1002"))
    transactionRepository.saveAndFlush(transaction)

    val changeLogTuple =
        tuple(transaction.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.INSERT, false)

    val changeLogsAfter = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogsAfter)
        .hasSize(1)
        .extracting(
            { it.recordId }, { ChangeTrackedTable.TRANSACTIONS }, { ChangeType.INSERT }, { false })
        .containsExactlyInAnyOrder(changeLogTuple)
  }

  @Test
  fun `adds multiple entries to change log on insert that affects multiple records`() {
    val changeLogsBefore = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogsBefore).hasSize(0)

    val transaction1 =
        Transaction(
            userId = defaultUsers.primaryUser.userTypedId,
            expenseDate = LocalDate.now(),
            description = "Hello2",
            amount = BigDecimal("1002"))
    val transaction2 =
        Transaction(
            userId = defaultUsers.primaryUser.userTypedId,
            expenseDate = LocalDate.now(),
            description = "World2",
            amount = BigDecimal("202"))
    transactionRepository.saveAllAndFlush(listOf(transaction1, transaction2))

    val changeLogTuples =
        sequenceOf(transaction1, transaction2)
            .map { txn ->
              tuple(txn.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.INSERT, false)
            }
            .toList()

    val changeLogsAfter = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogsAfter)
        .hasSize(2)
        .extracting(
            { it.recordId }, { ChangeTrackedTable.TRANSACTIONS }, { ChangeType.INSERT }, { false })
        .containsExactlyInAnyOrder(*changeLogTuples.toTypedArray())
  }

  @Test
  fun `adds entry to change log on update`() {
    val params = MapSqlParameterSource().addValue("uid", transaction1.uid.uuid)
    val rowsUpdated =
        jdbcTemplate.update(
            "UPDATE transactions SET description = 'updated' WHERE uid = :uid", params)
    assertThat(rowsUpdated).isEqualTo(1)

    val dbTxn = transactionRepository.findById(transaction1.uid).orElseThrow()
    assertThat(dbTxn.description).isEqualTo("updated")

    val expectedChangeLogTuple =
        tuple(transaction1.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.UPDATE, false)

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs)
        .hasSize(1)
        .extracting(
            { it.recordId }, { ChangeTrackedTable.TRANSACTIONS }, { ChangeType.UPDATE }, { false })
        .containsExactly(expectedChangeLogTuple)
  }

  @Test
  fun `adds multiple entries to change log on update that affects multiple records`() {
    val rowsUpdated =
        jdbcTemplate.update(
            "UPDATE transactions SET description = 'updated'", MapSqlParameterSource())
    assertThat(rowsUpdated).isEqualTo(2)

    val dbTxn1 = transactionRepository.findById(transaction1.uid).orElseThrow()
    assertThat(dbTxn1.description).isEqualTo("updated")
    val dbTxn2 = transactionRepository.findById(transaction2.uid).orElseThrow()
    assertThat(dbTxn2.description).isEqualTo("updated")

    val expectedChangeLogTuples =
        arrayOf(
            tuple(transaction1.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.UPDATE, false),
            tuple(transaction2.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.UPDATE, false))

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs)
        .hasSize(2)
        .extracting(
            { it.recordId }, { ChangeTrackedTable.TRANSACTIONS }, { ChangeType.UPDATE }, { false })
        .containsExactlyInAnyOrder(*expectedChangeLogTuples)
  }

  @Test
  fun `does nothing with update that changes nothing`() {
    val params = MapSqlParameterSource().addValue("uid", UUID.randomUUID())
    val rowsUpdated =
        jdbcTemplate.update(
            "UPDATE transactions SET description = 'updated' WHERE uid = :uid", params)
    assertThat(rowsUpdated).isEqualTo(0)

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs).hasSize(0)
  }

  @Test
  fun `adds entry to change log on delete`() {
    val params = MapSqlParameterSource().addValue("uid", transaction1.uid.uuid)
    val rowsUpdated = jdbcTemplate.update("DELETE FROM transactions WHERE uid = :uid", params)
    assertThat(rowsUpdated).isEqualTo(1)

    val dbTxn = transactionRepository.findById(transaction1.uid)
    assertThat(dbTxn).isEmpty

    val expectedChangeLogTuple =
        tuple(transaction1.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.DELETE, false)

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs)
        .hasSize(1)
        .extracting(
            { it.recordId }, { ChangeTrackedTable.TRANSACTIONS }, { ChangeType.DELETE }, { false })
        .containsExactly(expectedChangeLogTuple)
  }

  @Test
  fun `adds multiple entries to change long on delete that affects multiple records`() {
    val rowsUpdated = jdbcTemplate.update("DELETE FROM transactions", MapSqlParameterSource())
    assertThat(rowsUpdated).isEqualTo(2)

    assertThat(transactionRepository.count()).isEqualTo(0)

    val expectedChangeLogTuples =
        arrayOf(
            tuple(transaction1.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.DELETE, false),
            tuple(transaction2.uid.uuid, ChangeTrackedTable.TRANSACTIONS, ChangeType.DELETE, false))

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs)
        .hasSize(2)
        .extracting(
            { it.recordId }, { ChangeTrackedTable.TRANSACTIONS }, { ChangeType.DELETE }, { false })
        .containsExactlyInAnyOrder(*expectedChangeLogTuples)
  }

  @Test
  fun `does nothing with delete that changes nothing`() {
    val params = MapSqlParameterSource().addValue("uid", UUID.randomUUID())
    val rowsUpdated = jdbcTemplate.update("DELETE FROM transactions WHERE uid = :uid", params)
    assertThat(rowsUpdated).isEqualTo(0)

    val changeLogs = embeddingChangeLogRepository.getAllChangeLogs()
    assertThat(changeLogs).hasSize(0)
  }
}
