package us.craigmiller160.expensetrackerapi.testcore

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import us.craigmiller160.testcontainers.common.TestcontainersExtension

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@SpringBootTest
@ExtendWith(value = [TestcontainersExtension::class, SpringExtension::class, MockExtension::class])
@AutoConfigureMockMvc
@ActiveProfiles("test")
annotation class ExpenseTrackerIntegrationTest
