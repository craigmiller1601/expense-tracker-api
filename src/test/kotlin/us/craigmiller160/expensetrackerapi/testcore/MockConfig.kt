package us.craigmiller160.expensetrackerapi.testcore

import com.aallam.openai.client.OpenAI
import org.mockito.kotlin.mock
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import us.craigmiller160.expensetrackerapi.client.ai.AiClient

@Configuration
class MockConfig {
  @Bean fun openaiClient(): OpenAI = mock<OpenAI>()
  @Bean fun aiClient(): AiClient = mock<AiClient>()
}
