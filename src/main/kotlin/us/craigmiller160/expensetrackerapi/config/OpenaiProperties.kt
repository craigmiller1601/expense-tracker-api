package us.craigmiller160.expensetrackerapi.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties

@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
@ConfigurationProperties("spring.ai.openai")
data class OpenaiProperties(
    val apiKey: String,
    val timeouts: OpenaiTimeouts,
    val models: OpenaiModels
)

data class OpenaiTimeouts(val connectSeconds: Int, val socketSeconds: Int, val requestSeconds: Int)

data class OpenaiModels(val chat: OpenaiChatModel, val embedding: OpenaiEmbeddingModel)

data class OpenaiChatModel(val name: String)

data class OpenaiEmbeddingModel(val name: String, val dimensions: Int)
