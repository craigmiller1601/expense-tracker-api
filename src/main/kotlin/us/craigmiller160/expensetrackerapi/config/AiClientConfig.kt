package us.craigmiller160.expensetrackerapi.config

import com.aallam.openai.api.http.Timeout
import com.aallam.openai.api.logging.LogLevel
import com.aallam.openai.client.LoggingConfig
import com.aallam.openai.client.OpenAI
import io.weaviate.client.Config
import io.weaviate.client.WeaviateAuthClient
import io.weaviate.client.WeaviateClient
import kotlin.time.Duration.Companion.seconds
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class AiClientConfig {
  @Bean
  fun weaviateClient(props: WeaviateProperties): WeaviateClient =
      Config(props.scheme, props.host).let { WeaviateAuthClient.apiKey(it, props.apiKey) }

  @Bean
  @Profile("!test")
  fun openaiClient(props: OpenaiProperties): OpenAI =
      OpenAI(
          token = props.apiKey,
          timeout = props.timeouts.toTimeout(),
          logging = LoggingConfig(logLevel = LogLevel.None),
      )
}

private fun OpenaiTimeouts.toTimeout(): Timeout =
    Timeout(
        socket = this.socketSeconds.seconds,
        connect = this.connectSeconds.seconds,
        request = this.requestSeconds.seconds)
