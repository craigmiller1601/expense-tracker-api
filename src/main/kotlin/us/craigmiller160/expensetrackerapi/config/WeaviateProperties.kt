package us.craigmiller160.expensetrackerapi.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties

@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
@ConfigurationProperties(prefix = "spring.ai.weaviate")
data class WeaviateProperties(
    val scheme: String,
    val host: String,
    val apiKey: String,
    val classes: WeaviateClasses
)

data class WeaviateClasses(val transactions: String)
