package us.craigmiller160.expensetrackerapi.config

import java.util.Properties
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean
import org.springframework.core.env.PropertiesPropertySource
import org.springframework.core.env.PropertySource
import org.springframework.core.io.support.EncodedResource
import org.springframework.core.io.support.PropertySourceFactory

class YamlPropertySourceFactory : PropertySourceFactory {
  override fun createPropertySource(name: String?, resource: EncodedResource): PropertySource<*> {
    if (!resource.resource.exists()) {
      return PropertiesPropertySource(resource.resource.filename ?: "Unknown", Properties())
    }

    val factory = YamlPropertiesFactoryBean()
    factory.setResources(resource.resource)

    val properties = factory.getObject()

    return PropertiesPropertySource(resource.resource.filename ?: "Unknown", properties!!)
  }
}
