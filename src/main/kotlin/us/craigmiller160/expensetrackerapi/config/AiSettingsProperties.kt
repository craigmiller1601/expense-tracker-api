package us.craigmiller160.expensetrackerapi.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "spring.ai.settings")
data class AiSettingsProperties(
    val transactionSearch: TransactionSearchProperties,
    val autoCategorize: AutoCategorizeProperties
)

data class TransactionSearchProperties(val oldestRecordMonths: Int)

data class AutoCategorizeProperties(val maxTransactionsPerRequest: Int)
