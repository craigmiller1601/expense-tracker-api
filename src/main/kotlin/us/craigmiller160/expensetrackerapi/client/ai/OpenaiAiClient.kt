package us.craigmiller160.expensetrackerapi.client.ai

import arrow.core.Either
import arrow.core.getOrHandle
import com.aallam.openai.api.chat.ChatCompletionRequest
import com.aallam.openai.api.chat.ChatMessage
import com.aallam.openai.api.chat.ChatRole
import com.aallam.openai.api.embedding.EmbeddingRequest
import com.aallam.openai.api.model.ModelId
import com.aallam.openai.client.OpenAI
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import kotlin.time.Duration.Companion.nanoseconds
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import us.craigmiller160.expensetrackerapi.common.error.InvalidAiResponseJsonException
import us.craigmiller160.expensetrackerapi.config.OpenaiProperties
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.CategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.PreviousTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.UncategorizedTransaction

@Component
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class OpenaiAiClient(
    private val openai: OpenAI,
    private val openaiProperties: OpenaiProperties,
    private val objectMapper: ObjectMapper
) : AiClient {
  companion object {
    private val aiResponseTypeRef: TypeReference<List<CategorizedTransaction>> = jacksonTypeRef()
  }

  private val log = LoggerFactory.getLogger(javaClass)

  override fun createEmbedding(text: List<String>): List<List<Double>> {
    log.debug("About to create embedding with OpenAI")
    val openaiStart = System.nanoTime()
    val response =
        EmbeddingRequest(
                model = ModelId(openaiProperties.models.embedding.name),
                input = text,
                dimensions = openaiProperties.models.embedding.dimensions)
            .let { runBlocking { openai.embeddings(it) } }

    val openaiEnd = System.nanoTime()
    val openaiTimeMillis = (openaiEnd - openaiStart).nanoseconds.inWholeMilliseconds

    log.debug(
        "OpenAI embedding generation complete. Time: {},s Prompt Tokens: {} Completion Tokens: {} Total Tokens: {}",
        openaiTimeMillis,
        response.usage.promptTokens,
        response.usage.completionTokens,
        response.usage.totalTokens)

    return response.embeddings.map { it.embedding }
  }

  override fun autoCategorizeTransactions(
      categoryNames: List<String>,
      previousTransactions: List<PreviousTransaction>,
      uncategorizedTransactions: List<UncategorizedTransaction>
  ): List<CategorizedTransaction> {
    log.debug("About to auto-categorize transactions with OpenAI")
    val prompt =
        buildAutoCategorizeTransactionsPrompt(
            categoryNames, previousTransactions, uncategorizedTransactions)

    val start = System.nanoTime()
    val aiResponse = runBlocking {
      ChatCompletionRequest(model = ModelId(openaiProperties.models.chat.name), messages = prompt)
          .let { openai.chatCompletion(it) }
    }
    val end = System.nanoTime()
    val openaiTimeMillis = (end - start).nanoseconds.inWholeMilliseconds

    log.debug(
        "OpenAI auto-categorize transactions operation complete. Time: {},s Prompt Tokens: {} Completion Tokens: {} Total Tokens: {}",
        openaiTimeMillis,
        aiResponse.usage?.promptTokens,
        aiResponse.usage?.completionTokens,
        aiResponse.usage?.totalTokens)

    val responseJson =
        aiResponse.choices.mapNotNull { choice -> choice.message.content }.joinToString("\n")

    return Either.catch {
          cleanResponseJson(responseJson).let { objectMapper.readValue(it, aiResponseTypeRef) }
        }
        .mapLeft { ex -> InvalidAiResponseJsonException(responseJson, ex) }
        .getOrHandle { throw it }
  }

  private fun cleanResponseJson(responseJson: String): String =
      responseJson.replace(Regex("^```json"), "").replace(Regex("```$"), "")

  private fun buildAutoCategorizeTransactionsPrompt(
      categoryNames: List<String>,
      previousTransactions: List<PreviousTransaction>,
      uncategorizedTransactions: List<UncategorizedTransaction>
  ): List<ChatMessage> {
    val categoryNamesJson = objectMapper.writeValueAsString(categoryNames)
    val uncategorizedTransactionsJson = objectMapper.writeValueAsString(uncategorizedTransactions)
    val previousTransactionsJson = objectMapper.writeValueAsString(previousTransactions)

    val systemMessage =
        SimpleChatMessage(
            role = ChatRole.System,
            content =
                "You are an accountant reviewing and categorizing transactions from an expense tracking application.")

    val categoryMessage =
        SimpleChatMessage(
            role = ChatRole.User,
            content =
                """
                The following is a JSON array with the names of all of the categories available to be used.
                
                Here is the JSON:
                
                $categoryNamesJson
            """
                    .trimIndent())

    val previousTransactionsMessage =
        SimpleChatMessage(
            role = ChatRole.User,
            content =
                """
              The following transactions were categorized by your client previously. They demonstrate the
              preference for how things should be categorized by your client. They are provided as a JSON array
              with JSON objects. Each object should be interpreted using these JSON Path expressions:
              
              $.expenseDate = the date of the past transaction.
              $.description = the description of the past transaction.
              $.category = the name of the category that was selected for the past transaction.
              
              $previousTransactionsJson
          """
                    .trimIndent())

    val categorizationGuidelinesMessage =
        SimpleChatMessage(
            role = ChatRole.User,
            content =
                """
              You will follow these guidelines when categorizing the transactions.
              
              1. Use the previously categorized transactions provided. Compare their descriptions to existing transactions. If the transactions seem to be similar, re-use the previously selected category for the new one.
              2. If previously categorized transactions with similar descriptions to a new one have different categories assigned to them, prioritize the newest one.
              3. If there are no matches from previously categorized transactions, do your best to categorize them yourself. If you really do not know what to assign the category as, leave it blank.
              4. Any transactions from Amazon.com or any Checks should not be categorized.
          """
                    .trimIndent())

    val transactionsToCategorize =
        SimpleChatMessage(
            role = ChatRole.User,
            content =
                """
              The following JSON array contains the transactions to categorize. The JSON objects
              in the array should be interpreted using these JSON Path expressions:
              
              $.uid = This is the unique UUID of the transaction.
              $.description = This is the description of the transaction.
              
              The transactions are here:
              
              $uncategorizedTransactionsJson
          """
                    .trimIndent())

    val responseInstructions =
        SimpleChatMessage(
            role = ChatRole.User,
            content =
                """
              When you categorize the transactions, return the response as a JSON array. The array should contain objects
              with the following shape:
              
              { "uid": "", "category": "" }
              
              The above example can be interpreted using these JSON Path expressions:
              
              $.uid = This is the unique UUID of the transaction.
              $.category = This is the name of the category. If no category is assigned, it should be null.
              
              Return only the JSON in that format, and nothing else.
          """
                    .trimIndent())

    val prompt =
        listOf(
            systemMessage,
            categoryMessage,
            previousTransactionsMessage,
            categorizationGuidelinesMessage,
            transactionsToCategorize,
            responseInstructions)
    val promptJson = objectMapper.writeValueAsString(prompt)
    log.debug("Prompting OpenAI to auto-categorize transactions. Prompt: {}", promptJson)
    return prompt.map { it.toChatMessage() }
  }

  private data class SimpleChatMessage(val role: ChatRole, val content: String)

  private fun SimpleChatMessage.toChatMessage(): ChatMessage =
      ChatMessage(role = role, content = content)
}
