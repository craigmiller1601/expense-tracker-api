package us.craigmiller160.expensetrackerapi.client.ai

import us.craigmiller160.expensetrackerapi.data.ai.model.chat.CategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.PreviousTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.UncategorizedTransaction

interface AiClient {
  fun createEmbedding(text: List<String>): List<List<Double>>
  fun autoCategorizeTransactions(
      categoryNames: List<String>,
      previousTransactions: List<PreviousTransaction>,
      uncategorizedTransactions: List<UncategorizedTransaction>
  ): List<CategorizedTransaction>
}
