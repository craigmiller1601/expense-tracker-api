package us.craigmiller160.expensetrackerapi.common.data.typedid.jackson

import com.fasterxml.jackson.databind.module.SimpleModule
import org.springframework.stereotype.Component
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId

@Suppress("LeakingThis")
@Component
class TypedIdModule : SimpleModule("TypedIdModule") {
  init {
    addSerializer(TypedIdSerializer())
    addDeserializer(TypedId::class.java, TypedIdDeserializer())
  }
}
