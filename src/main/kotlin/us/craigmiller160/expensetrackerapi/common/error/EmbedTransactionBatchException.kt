package us.craigmiller160.expensetrackerapi.common.error

class EmbedTransactionBatchException(msg: String, cause: Throwable? = null) :
    RuntimeException(msg, cause)
