package us.craigmiller160.expensetrackerapi.common.error

sealed class AiException(msg: String, cause: Throwable? = null) : RuntimeException(msg, cause)
