package us.craigmiller160.expensetrackerapi.common.error

class EmbeddingSyncException(msg: String, cause: Throwable? = null) : RuntimeException(msg, cause)
