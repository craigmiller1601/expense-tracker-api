package us.craigmiller160.expensetrackerapi.common.error

class EndpointDisabledException : RuntimeException("Endpoint is disabled")
