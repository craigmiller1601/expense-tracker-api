package us.craigmiller160.expensetrackerapi.common.error

class InvalidAiResponseJsonException(rawBody: String, ex: Throwable? = null) :
    AiException("Invalid AI Response JSON. Body: $rawBody", ex)
