package us.craigmiller160.expensetrackerapi.common.error

class InvalidAiCategoryException(msg: String, cause: Throwable? = null) : AiException(msg, cause)
