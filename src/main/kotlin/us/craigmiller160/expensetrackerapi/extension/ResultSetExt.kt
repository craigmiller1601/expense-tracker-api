package us.craigmiller160.expensetrackerapi.extension

import java.sql.ResultSet
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.UUID
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId

fun ResultSet.getUUID(columnName: String): UUID = UUID.fromString(getString(columnName))

fun ResultSet.getNullableUUID(columnName: String): UUID? =
    getString(columnName)?.let { UUID.fromString(it) }

fun <T> ResultSet.getTypedId(columnName: String): TypedId<T> = TypedId(getUUID(columnName))

fun <T> ResultSet.getNullableTypedId(columnName: String): TypedId<T>? =
    getNullableUUID(columnName)?.let { TypedId(it) }

fun ResultSet.getNullableLocalDate(columnName: String): LocalDate? =
    getDate(columnName)?.toLocalDate()

fun ResultSet.getLocalDate(columnName: String): LocalDate = getDate(columnName).toLocalDate()

fun ResultSet.getNullableZonedDateTime(columnName: String): ZonedDateTime? =
    getTimestamp(columnName)?.toInstant()?.atZone(ZoneId.of("UTC"))

fun ResultSet.getZonedDateTime(columnName: String): ZonedDateTime =
    getNullableZonedDateTime(columnName)
        ?: throw IllegalStateException("Required column $columnName is null")

fun ResultSet.getNullableLong(columnName: String): Long? {
  val value = getLong(columnName)
  return if (wasNull()) null else value
}
