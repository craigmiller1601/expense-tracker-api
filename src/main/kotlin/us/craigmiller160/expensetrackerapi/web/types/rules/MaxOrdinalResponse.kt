package us.craigmiller160.expensetrackerapi.web.types.rules

data class MaxOrdinalResponse(val maxOrdinal: Int)
