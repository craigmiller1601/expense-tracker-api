package us.craigmiller160.expensetrackerapi.web.types.transaction

import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import us.craigmiller160.expensetrackerapi.web.types.MAX_PAGE_SIZE
import us.craigmiller160.expensetrackerapi.web.types.PageableRequest
import us.craigmiller160.expensetrackerapi.web.types.QueryObject

data class GetPossibleDuplicatesRequest(
    @field:Min(0) override val pageNumber: Int,
    @field:Min(0) @field:Max(MAX_PAGE_SIZE) override val pageSize: Int
) : PageableRequest, QueryObject
