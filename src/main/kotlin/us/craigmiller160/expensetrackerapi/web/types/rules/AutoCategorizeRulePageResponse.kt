package us.craigmiller160.expensetrackerapi.web.types.rules

import org.springframework.data.domain.Page
import us.craigmiller160.expensetrackerapi.data.model.AutoCategorizeRuleView
import us.craigmiller160.expensetrackerapi.web.types.PageableResponse

data class AutoCategorizeRulePageResponse(
    val rules: List<AutoCategorizeRuleResponse>,
    override val pageNumber: Int,
    override val totalItems: Long
) : PageableResponse {
  companion object {
    fun from(page: Page<AutoCategorizeRuleView>): AutoCategorizeRulePageResponse =
        AutoCategorizeRulePageResponse(
            rules = page.content.map { AutoCategorizeRuleResponse.from(it) },
            pageNumber = page.number,
            totalItems = page.totalElements)
  }
}
