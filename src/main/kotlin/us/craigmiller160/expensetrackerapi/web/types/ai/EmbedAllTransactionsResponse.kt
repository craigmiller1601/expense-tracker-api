package us.craigmiller160.expensetrackerapi.web.types.ai

data class EmbedAllTransactionsResponse(val count: Long)
