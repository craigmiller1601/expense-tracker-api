package us.craigmiller160.expensetrackerapi.web.types.ai

data class QueryEmbeddingsRequest(val query: String, val limit: Int)
