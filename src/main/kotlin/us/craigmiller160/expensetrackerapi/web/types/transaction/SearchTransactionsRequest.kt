package us.craigmiller160.expensetrackerapi.web.types.transaction

import io.swagger.v3.oas.annotations.Hidden
import jakarta.validation.constraints.AssertTrue
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import java.time.LocalDate
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.format.annotation.DateTimeFormat
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.utils.DateUtils
import us.craigmiller160.expensetrackerapi.data.model.YesNoFilter
import us.craigmiller160.expensetrackerapi.data.model.toColumnName
import us.craigmiller160.expensetrackerapi.data.model.toSpringSortDirection
import us.craigmiller160.expensetrackerapi.web.types.MAX_PAGE_SIZE
import us.craigmiller160.expensetrackerapi.web.types.PageableRequest
import us.craigmiller160.expensetrackerapi.web.types.QueryObject
import us.craigmiller160.expensetrackerapi.web.types.SortDirection
import us.craigmiller160.expensetrackerapi.web.types.SortableRequest

data class SearchTransactionsRequest(
    @field:Min(0) override val pageNumber: Int,
    @field:Min(0) @field:Max(MAX_PAGE_SIZE) override val pageSize: Int,
    override val sortKey: TransactionSortKey,
    override val sortDirection: SortDirection,
    @field:DateTimeFormat(pattern = DateUtils.DATE_PATTERN) val startDate: LocalDate? = null,
    @field:DateTimeFormat(pattern = DateUtils.DATE_PATTERN) val endDate: LocalDate? = null,
    val confirmed: YesNoFilter = YesNoFilter.ALL,
    val categorized: YesNoFilter = YesNoFilter.ALL,
    val duplicate: YesNoFilter = YesNoFilter.ALL,
    val possibleRefund: YesNoFilter = YesNoFilter.ALL,
    val categoryIds: Set<TypedId<CategoryId>>? = null,
    val description: String? = null
) : PageableRequest, SortableRequest<TransactionSortKey>, QueryObject {

  @Hidden
  @AssertTrue(message = "Cannot set categorized to NO and specify categoryIds")
  fun isCategorizedValidation(): Boolean {
    if (YesNoFilter.NO == categorized) {
      return categoryIds.isNullOrEmpty()
    }
    return true
  }
}

fun SearchTransactionsRequest.toPageable(): Pageable {
  val direction = sortDirection.toSpringSortDirection()
  val col = sortKey.toColumnName()
  val sort =
      Sort.by(
          Sort.Order(direction, col),
          Sort.Order(Sort.Direction.ASC, "description"),
          Sort.Order(Sort.Direction.ASC, "created"))
  return PageRequest.of(pageNumber, pageSize, sort)
}
