package us.craigmiller160.expensetrackerapi.web.types.report

import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.data.projection.SpendingByMonth

data class ReportMonthResponse(
    val date: LocalDate,
    val total: BigDecimal,
    val categories: List<ReportCategoryResponse>
) {
  companion object {
    fun from(month: SpendingByMonth): ReportMonthResponse =
        ReportMonthResponse(
            date = month.month,
            total = month.total,
            categories = month.categories.map { ReportCategoryResponse.from(it, month.total) })
  }
}
