package us.craigmiller160.expensetrackerapi.web.types.report

import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.web.types.MAX_PAGE_SIZE
import us.craigmiller160.expensetrackerapi.web.types.PageableRequest
import us.craigmiller160.expensetrackerapi.web.types.QueryObject

data class ReportRequest(
    @field:Min(0) override val pageNumber: Int,
    @field:Min(0) @field:Max(MAX_PAGE_SIZE) override val pageSize: Int,
    val categoryIdType: ReportCategoryIdFilterType = ReportCategoryIdFilterType.EXCLUDE,
    val categoryIds: List<TypedId<CategoryId>> = listOf(),
    val includeNextRecord: Boolean = false
) : PageableRequest, QueryObject
