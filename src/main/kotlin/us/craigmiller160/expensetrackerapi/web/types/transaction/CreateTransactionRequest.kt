package us.craigmiller160.expensetrackerapi.web.types.transaction

import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId

data class CreateTransactionRequest(
    val expenseDate: LocalDate,
    val description: String,
    val amount: BigDecimal,
    val categoryId: TypedId<CategoryId>?
)
