package us.craigmiller160.expensetrackerapi.web.types.transaction

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

interface TransactionUpdateItem {
  val transactionId: TypedId<TransactionId>
}

interface TransactionAndCategoryUpdateItem : TransactionUpdateItem {
  val categoryId: TypedId<CategoryId>?
}

interface TransactionAndConfirmUpdateItem : TransactionUpdateItem {
  val confirmed: Boolean
}
