package us.craigmiller160.expensetrackerapi.web.types.ai

import java.time.LocalDate

data class TestAutoCategorizeTransactionsRequest(val startDate: LocalDate, val endDate: LocalDate)
