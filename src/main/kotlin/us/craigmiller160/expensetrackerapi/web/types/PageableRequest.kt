package us.craigmiller160.expensetrackerapi.web.types

const val MAX_PAGE_SIZE = 100L

interface PageableRequest {
  val pageNumber: Int
  val pageSize: Int
}
