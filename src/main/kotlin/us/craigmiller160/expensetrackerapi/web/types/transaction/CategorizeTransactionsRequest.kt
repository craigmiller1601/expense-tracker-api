package us.craigmiller160.expensetrackerapi.web.types.transaction

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

data class TransactionAndCategory(
    override val transactionId: TypedId<TransactionId>,
    override val categoryId: TypedId<CategoryId>? = null
) : TransactionAndCategoryUpdateItem

data class CategorizeTransactionsRequest(
    val transactionsAndCategories: Set<TransactionAndCategory>
)
