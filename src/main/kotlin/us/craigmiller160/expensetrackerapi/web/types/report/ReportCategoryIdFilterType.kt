package us.craigmiller160.expensetrackerapi.web.types.report

enum class ReportCategoryIdFilterType {
  INCLUDE,
  EXCLUDE
}
