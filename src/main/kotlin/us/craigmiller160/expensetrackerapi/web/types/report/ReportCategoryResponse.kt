package us.craigmiller160.expensetrackerapi.web.types.report

import java.math.BigDecimal
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.data.projection.SpendingByCategory

data class ReportCategoryResponse(
    val id: TypedId<CategoryId>,
    val name: String,
    val color: String,
    val amount: BigDecimal,
    val percent: BigDecimal
) {
  companion object {
    fun from(category: SpendingByCategory, monthTotal: BigDecimal): ReportCategoryResponse =
        ReportCategoryResponse(
            id = category.categoryId,
            name = category.categoryName,
            color = category.color,
            amount = category.amount,
            percent = category.amount / monthTotal)
  }
}
