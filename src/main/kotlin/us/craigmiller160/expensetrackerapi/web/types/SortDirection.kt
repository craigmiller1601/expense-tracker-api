package us.craigmiller160.expensetrackerapi.web.types

enum class SortDirection {
  ASC,
  DESC
}
