package us.craigmiller160.expensetrackerapi.web.types.transaction

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

data class TransactionToUpdate(
    override val transactionId: TypedId<TransactionId>,
    override val confirmed: Boolean,
    override val categoryId: TypedId<CategoryId>? = null
) : TransactionAndCategoryUpdateItem, TransactionAndConfirmUpdateItem

data class UpdateTransactionsRequest(val transactions: Set<TransactionToUpdate>)
