package us.craigmiller160.expensetrackerapi.web.types.transaction

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

data class DeleteTransactionsRequest(val ids: Set<TypedId<TransactionId>>)
