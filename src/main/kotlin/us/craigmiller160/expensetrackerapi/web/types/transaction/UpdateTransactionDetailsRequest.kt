package us.craigmiller160.expensetrackerapi.web.types.transaction

import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

data class UpdateTransactionDetailsRequest(
    override val transactionId: TypedId<TransactionId>,
    override val confirmed: Boolean,
    val expenseDate: LocalDate,
    val description: String,
    val amount: BigDecimal,
    override val categoryId: TypedId<CategoryId>?
) : TransactionAndCategoryUpdateItem, TransactionAndConfirmUpdateItem
