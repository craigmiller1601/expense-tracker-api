package us.craigmiller160.expensetrackerapi.web.types.ai

import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransactionSearchResult
import us.craigmiller160.expensetrackerapi.data.model.TransactionView

data class EmbeddedTransactionResponse(
    val uid: TypedId<TransactionId>,
    val description: String,
    val expenseDate: LocalDate,
    val distance: Double,
    val certainty: Double,
    val category: String?
)

fun TransactionView.toEmbeddedTransactionResponse(
    embeddedTransaction: EmbeddedTransactionSearchResult
): EmbeddedTransactionResponse =
    EmbeddedTransactionResponse(
        uid = uid,
        description = description,
        expenseDate = expenseDate,
        distance = embeddedTransaction.distance,
        certainty = embeddedTransaction.certainty,
        category = categoryName)
