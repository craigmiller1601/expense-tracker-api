package us.craigmiller160.expensetrackerapi.web.types.transaction

import java.time.ZonedDateTime
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.data.model.TransactionView

data class TransactionDuplicateResponse(
    val id: TypedId<TransactionId>,
    val confirmed: Boolean,
    val created: ZonedDateTime,
    val updated: ZonedDateTime,
    val categoryName: String?
) {
  companion object {
    fun from(transaction: TransactionView): TransactionDuplicateResponse =
        TransactionDuplicateResponse(
            id = transaction.uid,
            confirmed = transaction.confirmed,
            created = transaction.created,
            updated = transaction.updated,
            categoryName = transaction.categoryName)
  }
}
