package us.craigmiller160.expensetrackerapi.web.types.transaction

enum class TransactionSortKey {
  EXPENSE_DATE
}
