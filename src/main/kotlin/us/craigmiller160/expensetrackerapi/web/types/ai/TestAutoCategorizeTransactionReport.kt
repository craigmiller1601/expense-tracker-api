package us.craigmiller160.expensetrackerapi.web.types.ai

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.CategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.UncategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.model.TransactionView

data class TestAutoCategorizeTransactionReport(
    val counts: ReportCounts,
    val uncategorized: List<UncategorizedTransaction>,
    val inaccuratelyCategorized: List<TransactionResult>
) {
  val percentages =
      ReportPercents(
          categorized = counts.categorized.toDouble() / counts.totalMinusSkipped.toDouble(),
          categorizedAccuracy = counts.accuratelyCategorized.toDouble() / counts.categorized)
}

data class ReportCounts(
    val total: Int,
    val skipped: Int,
    val categorized: Int,
    val accuratelyCategorized: Int
) {
  val totalMinusSkipped: Int = total - skipped
}

data class ReportPercents(val categorized: Double, val categorizedAccuracy: Double)

data class TransactionResult(
    val uid: TypedId<TransactionId>,
    val description: String,
    val actualCategory: String,
    val aiCategory: String?
)

fun CategorizedTransaction.toTransactionResult(
    originalTransaction: TransactionView
): TransactionResult =
    TransactionResult(
        uid = originalTransaction.uid,
        description = originalTransaction.description,
        aiCategory = category,
        actualCategory = originalTransaction.categoryName
                ?: throw IllegalArgumentException(
                    "Cannot build TransactionResult without a categorized original Transaction"))

fun TransactionResult.toUncategorizedTransaction(): UncategorizedTransaction =
    UncategorizedTransaction(uid = uid, description = description)
