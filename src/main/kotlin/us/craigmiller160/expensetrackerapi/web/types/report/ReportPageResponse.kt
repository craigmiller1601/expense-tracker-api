package us.craigmiller160.expensetrackerapi.web.types.report

import org.springframework.data.domain.Page
import us.craigmiller160.expensetrackerapi.data.projection.SpendingByMonth
import us.craigmiller160.expensetrackerapi.web.types.PageableResponse

data class ReportPageResponse(
    val reports: List<ReportMonthResponse>,
    val nextRecord: ReportMonthResponse? = null,
    override val pageNumber: Int,
    override val totalItems: Long
) : PageableResponse {
  companion object {
    fun from(page: Page<SpendingByMonth>, nextMonth: SpendingByMonth? = null): ReportPageResponse =
        ReportPageResponse(
            pageNumber = page.number,
            totalItems = page.totalElements,
            nextRecord = nextMonth?.let { ReportMonthResponse.from(it) },
            reports = page.content.map { ReportMonthResponse.from(it) })
  }
}
