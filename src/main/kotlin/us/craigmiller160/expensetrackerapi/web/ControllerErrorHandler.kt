package us.craigmiller160.expensetrackerapi.web

import java.time.ZoneId
import java.time.ZonedDateTime
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import us.craigmiller160.expensetrackerapi.common.error.BadRequestException
import us.craigmiller160.expensetrackerapi.common.error.EndpointDisabledException
import us.craigmiller160.expensetrackerapi.common.error.InvalidImportException
import us.craigmiller160.expensetrackerapi.web.types.ErrorResponse

@ControllerAdvice
class ControllerErrorHandler : ResponseEntityExceptionHandler() {
  private val log = LoggerFactory.getLogger(ControllerErrorHandler::class.java)

  override fun handleExceptionInternal(
      ex: java.lang.Exception,
      body: Any?,
      headers: HttpHeaders,
      statusCode: HttpStatusCode,
      request: WebRequest
  ): ResponseEntity<Any> {
    log.error(ex.message, ex)
    val (method, uri) = getMethodAndUri()
    val response =
        ErrorResponse(
            timestamp = ZonedDateTime.now(ZoneId.of("UTC")),
            method = method,
            path = uri,
            message = getExceptionMessage(ex),
            status = statusCode.value())
    return ResponseEntity.status(statusCode).headers(headers).body(response)
  }

  private fun getExceptionMessage(ex: Exception): String =
      when (ex) {
        is MethodArgumentNotValidException -> getValidationErrorMessage(ex)
        is BindException -> getValidationErrorMessage(ex)
        else -> ex.message ?: ""
      }

  private fun getValidationErrorMessage(ex: BindException): String {
    val globalErrors = ex.bindingResult.globalErrors.joinToString(";")
    val fieldErrors =
        ex.bindingResult.fieldErrors.joinToString(";") { "${it.field}: ${it.defaultMessage}" }
    return listOf(globalErrors, fieldErrors).filter { it.isNotBlank() }.joinToString(";")
  }

  @ExceptionHandler(BadRequestException::class)
  fun badRequestException(ex: BadRequestException, request: WebRequest): ResponseEntity<Any> {
    return handleExceptionInternal(ex, null, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
  }

  @ExceptionHandler(InvalidImportException::class)
  fun invalidImportException(ex: InvalidImportException, request: WebRequest): ResponseEntity<Any> {
    return handleExceptionInternal(ex, null, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
  }

  @ExceptionHandler(EndpointDisabledException::class)
  fun endpointDisabledException(
      ex: EndpointDisabledException,
      request: WebRequest
  ): ResponseEntity<Any> {
    return handleExceptionInternal(ex, null, HttpHeaders(), HttpStatus.NOT_FOUND, request)
  }

  private fun getMethodAndUri(): Pair<String, String> =
      RequestContextHolder.getRequestAttributes()
          ?.let {
            when (it) {
              is ServletRequestAttributes -> it
              else -> null
            }
          }
          ?.request
          ?.let { request -> Pair(request.method, request.requestURI) }
          ?: Pair("", "")
}
