package us.craigmiller160.expensetrackerapi.web.controller

import arrow.core.Either
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.error.EndpointDisabledException
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.web.types.rules.LastRuleAppliedResponse

@RestController
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class LastRuleAppliedControllerDisabled : LastRuleAppliedController {
  override fun getLastAppliedRuleForTransaction(
      transactionId: TypedId<TransactionId>
  ): TryEither<ResponseEntity<LastRuleAppliedResponse>> = Either.Left(EndpointDisabledException())
}
