package us.craigmiller160.expensetrackerapi.web.controller

import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.service.ai.AiAutoCategorizeService
import us.craigmiller160.expensetrackerapi.service.ai.EmbeddingAdminService
import us.craigmiller160.expensetrackerapi.service.ai.EmbeddingSyncInitService
import us.craigmiller160.expensetrackerapi.web.types.ai.EmbeddedTransactionResponse
import us.craigmiller160.expensetrackerapi.web.types.ai.QueryEmbeddingsRequest
import us.craigmiller160.expensetrackerapi.web.types.ai.TestAutoCategorizeTransactionReport
import us.craigmiller160.expensetrackerapi.web.types.ai.TestAutoCategorizeTransactionsRequest

@RestController
@RequestMapping("/ai/admin")
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
@PreAuthorize("hasRole('admin')")
class AiAdminController(
    private val embeddingAdminService: EmbeddingAdminService,
    private val aiAutoCategorizeService: AiAutoCategorizeService,
    private val embeddingSyncInitService: EmbeddingSyncInitService
) {

  @DeleteMapping("/all-tenants/embedding/all-transactions")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  fun deleteAllTransactions() = embeddingAdminService.deleteAllTransactionsAllTenants()

  @PostMapping("/init-sync")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  fun initializeSync() {
    embeddingSyncInitService.initializeSync()
  }

  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  schema = Schema(implementation = EmbeddedTransaction::class))])
  @GetMapping("/all-tenants/embedding/{userId}/transactions/{transactionId}")
  fun getTransactionById(
      @PathVariable userId: TypedId<UserId>,
      @PathVariable transactionId: TypedId<TransactionId>
  ): EmbeddedTransaction? = embeddingAdminService.getTransactionById(userId, transactionId)

  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  array =
                      ArraySchema(
                          schema = Schema(implementation = EmbeddedTransactionResponse::class)))])
  @PostMapping("/testing/query-embeddings")
  fun runQueryAgainstEmbeddings(
      @RequestBody request: QueryEmbeddingsRequest
  ): List<EmbeddedTransactionResponse> = embeddingAdminService.runQueryAgainstEmbeddings(request)

  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  schema = Schema(implementation = TestAutoCategorizeTransactionReport::class))])
  @PostMapping("/testing/auto-categorize-transactions")
  fun testAutoCategorizeTransactions(
      @RequestBody request: TestAutoCategorizeTransactionsRequest
  ): TestAutoCategorizeTransactionReport =
      aiAutoCategorizeService.testAutoCategorizeTransactions(request)
}
