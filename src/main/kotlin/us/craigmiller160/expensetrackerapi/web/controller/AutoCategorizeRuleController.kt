package us.craigmiller160.expensetrackerapi.web.controller

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import jakarta.validation.Valid
import java.util.UUID
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRulePageRequest
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRulePageResponse
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRuleRequest
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRuleResponse
import us.craigmiller160.expensetrackerapi.web.types.rules.MaxOrdinalResponse

@RequestMapping("/categories/rules")
interface AutoCategorizeRuleController {
  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  schema = Schema(implementation = AutoCategorizeRulePageResponse::class))])
  @Parameter(
      `in` = ParameterIn.QUERY, name = "categoryId", schema = Schema(implementation = UUID::class))
  @Parameter(
      `in` = ParameterIn.QUERY, name = "pageNumber", schema = Schema(implementation = Int::class))
  @Parameter(
      `in` = ParameterIn.QUERY, name = "pageSize", schema = Schema(implementation = Int::class))
  @Parameter(
      `in` = ParameterIn.QUERY, name = "regex", schema = Schema(implementation = String::class))
  @GetMapping
  fun getAllRules(
      @Valid @Parameter(hidden = true) request: AutoCategorizeRulePageRequest
  ): TryEither<AutoCategorizeRulePageResponse>

  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  array =
                      ArraySchema(
                          schema = Schema(implementation = AutoCategorizeRuleResponse::class)))])
  @PostMapping
  fun createRule(
      @RequestBody @Valid request: AutoCategorizeRuleRequest
  ): TryEither<AutoCategorizeRuleResponse>

  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  array =
                      ArraySchema(
                          schema = Schema(implementation = AutoCategorizeRuleResponse::class)))])
  @PutMapping("/{ruleId}")
  fun updateRule(
      @PathVariable ruleId: TypedId<AutoCategorizeRuleId>,
      @RequestBody @Valid request: AutoCategorizeRuleRequest
  ): TryEither<AutoCategorizeRuleResponse>

  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  array =
                      ArraySchema(
                          schema = Schema(implementation = AutoCategorizeRuleResponse::class)))])
  @GetMapping("/{ruleId}")
  fun getRule(
      @PathVariable ruleId: TypedId<AutoCategorizeRuleId>
  ): TryEither<AutoCategorizeRuleResponse>

  @ApiResponse(
      responseCode = "204",
      content =
          [
              Content(
                  mediaType = "application/json",
                  array = ArraySchema(schema = Schema(hidden = true)))])
  @DeleteMapping("/{ruleId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  fun deleteRule(@PathVariable ruleId: TypedId<AutoCategorizeRuleId>): TryEither<Unit>

  @PutMapping("/{ruleId}/reOrder/{ordinal}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  fun reOrderRule(
      @PathVariable ruleId: TypedId<AutoCategorizeRuleId>,
      @PathVariable ordinal: Int
  ): TryEither<Unit>

  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  array =
                      ArraySchema(schema = Schema(implementation = MaxOrdinalResponse::class)))])
  @GetMapping("/maxOrdinal")
  fun getMaxOrdinal(): TryEither<MaxOrdinalResponse>
}
