package us.craigmiller160.expensetrackerapi.web.controller

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.expensetrackerapi.service.ai.AiAutoCategorizeService

@RestController
@RequestMapping("/ai")
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class AiController(private val aiAutoCategorizeService: AiAutoCategorizeService) {
  @PostMapping("/auto-categorize-unconfirmed")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  fun autoCategorizeUnconfirmedTransactions() =
      aiAutoCategorizeService.autoCategorizeUnconfirmedTransactions()

  // TODO delete this
  @GetMapping
  fun foo(): String {
    Thread.sleep(120_000)
    return "Hello World"
  }
}
