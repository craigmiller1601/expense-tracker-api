package us.craigmiller160.expensetrackerapi.web.controller

import arrow.core.Either
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.common.error.EndpointDisabledException
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRulePageRequest
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRulePageResponse
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRuleRequest
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRuleResponse
import us.craigmiller160.expensetrackerapi.web.types.rules.MaxOrdinalResponse

@RestController
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class AutoCategorizeRuleControllerDisabled : AutoCategorizeRuleController {
  override fun getAllRules(
      request: AutoCategorizeRulePageRequest
  ): TryEither<AutoCategorizeRulePageResponse> = Either.Left(EndpointDisabledException())

  override fun createRule(
      request: AutoCategorizeRuleRequest
  ): TryEither<AutoCategorizeRuleResponse> = Either.Left(EndpointDisabledException())

  override fun updateRule(
      ruleId: TypedId<AutoCategorizeRuleId>,
      request: AutoCategorizeRuleRequest
  ): TryEither<AutoCategorizeRuleResponse> = Either.Left(EndpointDisabledException())

  override fun getRule(
      ruleId: TypedId<AutoCategorizeRuleId>
  ): TryEither<AutoCategorizeRuleResponse> = Either.Left(EndpointDisabledException())

  override fun deleteRule(ruleId: TypedId<AutoCategorizeRuleId>): TryEither<Unit> =
      Either.Left(EndpointDisabledException())

  override fun reOrderRule(ruleId: TypedId<AutoCategorizeRuleId>, ordinal: Int): TryEither<Unit> =
      Either.Left(EndpointDisabledException())

  override fun getMaxOrdinal(): TryEither<MaxOrdinalResponse> =
      Either.Left(EndpointDisabledException())
}
