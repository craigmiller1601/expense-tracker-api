package us.craigmiller160.expensetrackerapi.web.controller

import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.web.types.rules.LastRuleAppliedResponse

@RequestMapping("/transactions/rules/last-applied")
interface LastRuleAppliedController {

  @ApiResponses(
      value =
          [
              ApiResponse(
                  responseCode = "200",
                  content =
                      [
                          Content(
                              mediaType = "application/json",
                              schema = Schema(implementation = LastRuleAppliedResponse::class))]),
              ApiResponse(
                  responseCode = "204", content = [Content(schema = Schema(hidden = true))])])
  @GetMapping("/{transactionId}")
  fun getLastAppliedRuleForTransaction(
      @PathVariable transactionId: TypedId<TransactionId>
  ): TryEither<ResponseEntity<LastRuleAppliedResponse>>
}
