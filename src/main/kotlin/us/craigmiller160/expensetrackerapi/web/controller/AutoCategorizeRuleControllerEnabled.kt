package us.craigmiller160.expensetrackerapi.web.controller

import jakarta.validation.Valid
import org.springdoc.core.annotations.ParameterObject
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.service.AutoCategorizeRuleService
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRulePageRequest
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRulePageResponse
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRuleRequest
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRuleResponse
import us.craigmiller160.expensetrackerapi.web.types.rules.MaxOrdinalResponse

@RestController
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "false")
class AutoCategorizeRuleControllerEnabled(
    private val autoCategorizeRuleService: AutoCategorizeRuleService
) : AutoCategorizeRuleController {
  override fun getAllRules(
      @Valid @ParameterObject request: AutoCategorizeRulePageRequest
  ): TryEither<AutoCategorizeRulePageResponse> = autoCategorizeRuleService.getAllRules(request)

  override fun createRule(
      @RequestBody @Valid request: AutoCategorizeRuleRequest
  ): TryEither<AutoCategorizeRuleResponse> = autoCategorizeRuleService.createRule(request)

  override fun updateRule(
      @PathVariable ruleId: TypedId<AutoCategorizeRuleId>,
      @RequestBody @Valid request: AutoCategorizeRuleRequest
  ): TryEither<AutoCategorizeRuleResponse> = autoCategorizeRuleService.updateRule(ruleId, request)

  override fun getRule(
      @PathVariable ruleId: TypedId<AutoCategorizeRuleId>
  ): TryEither<AutoCategorizeRuleResponse> = autoCategorizeRuleService.getRule(ruleId)

  override fun deleteRule(@PathVariable ruleId: TypedId<AutoCategorizeRuleId>): TryEither<Unit> =
      autoCategorizeRuleService.deleteRule(ruleId)

  override fun reOrderRule(
      @PathVariable ruleId: TypedId<AutoCategorizeRuleId>,
      @PathVariable ordinal: Int
  ): TryEither<Unit> = autoCategorizeRuleService.reOrderRule(ruleId, ordinal)

  override fun getMaxOrdinal(): TryEither<MaxOrdinalResponse> =
      autoCategorizeRuleService.getMaxOrdinal()
}
