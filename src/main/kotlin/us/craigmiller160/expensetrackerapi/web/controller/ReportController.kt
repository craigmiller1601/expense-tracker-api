package us.craigmiller160.expensetrackerapi.web.controller

import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import jakarta.validation.Valid
import org.springdoc.core.annotations.ParameterObject
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.service.ReportService
import us.craigmiller160.expensetrackerapi.web.types.report.ReportPageResponse
import us.craigmiller160.expensetrackerapi.web.types.report.ReportRequest

@RestController
@RequestMapping("/reports")
class ReportController(private val reportService: ReportService) {
  @ApiResponse(
      responseCode = "200",
      content =
          [
              Content(
                  mediaType = "application/json",
                  schema = Schema(implementation = ReportPageResponse::class))])
  @GetMapping
  fun getSpendingByMonthAndCategory(
      @Valid @ParameterObject request: ReportRequest
  ): TryEither<ReportPageResponse> = reportService.getSpendingByMonthAndCategory(request)
}
