package us.craigmiller160.expensetrackerapi.web.controller

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.service.LastRuleAppliedService
import us.craigmiller160.expensetrackerapi.web.types.rules.LastRuleAppliedResponse

@RestController
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "false")
class LastRuleAppliedControllerEnabled(private val lastRuleAppliedService: LastRuleAppliedService) :
    LastRuleAppliedController {

  override fun getLastAppliedRuleForTransaction(
      @PathVariable transactionId: TypedId<TransactionId>
  ): TryEither<ResponseEntity<LastRuleAppliedResponse>> =
      lastRuleAppliedService.getLastAppliedRuleForTransaction(transactionId).map { result ->
        result?.let { ResponseEntity.ok(it) } ?: ResponseEntity.noContent().build()
      }
}
