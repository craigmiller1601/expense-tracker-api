package us.craigmiller160.expensetrackerapi.data.repository.impl

import jakarta.transaction.Transactional
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.data.SqlLoader
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository

@Repository
class EmbeddingSyncConfigJdbcRepositoryImpl(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val sqlLoader: SqlLoader
) : EmbeddingSyncConfigRepository {
  companion object {
    private const val CLEAR_EMBEDDING_SYNC_CONFIG =
        "embeddingSyncConfig/clear-embedding-sync-config.sql"
    private const val CREATE_EMBEDDING_SYNC_CONFIG =
        "embeddingSyncConfig/create-embedding-sync-config.sql"
    private const val IS_INITIAL_SYNC_COMPLETE = "embeddingSyncConfig/is-initial-sync-complete.sql"
    private const val SET_INITIAL_SYNC_COMPLETE =
        "embeddingSyncConfig/set-initial-sync-complete.sql"
  }

  @Transactional
  override fun resetEmbeddingSyncConfig() {
    sqlLoader.loadSql(CLEAR_EMBEDDING_SYNC_CONFIG).let { sql ->
      jdbcTemplate.update(sql, MapSqlParameterSource())
    }
    sqlLoader.loadSql(CREATE_EMBEDDING_SYNC_CONFIG).let { sql ->
      jdbcTemplate.update(sql, MapSqlParameterSource())
    }
  }

  override fun setInitialSyncComplete(initialSyncComplete: Boolean) {
    val params = MapSqlParameterSource().addValue("complete", initialSyncComplete)
    sqlLoader.loadSql(SET_INITIAL_SYNC_COMPLETE).let { sql -> jdbcTemplate.update(sql, params) }
  }

  override fun isInitialSyncComplete(): Boolean =
      sqlLoader
          .loadSql(IS_INITIAL_SYNC_COMPLETE)
          .let { sql ->
            jdbcTemplate.query(sql, MapSqlParameterSource()) { rs, _ -> rs.getBoolean(1) }
          }
          .first()
}
