package us.craigmiller160.expensetrackerapi.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.AutoCategorizeRuleView

interface AutoCategorizeRuleViewRepository :
    JpaRepository<AutoCategorizeRuleView, TypedId<AutoCategorizeRuleId>>,
    AutoCategorizeRuleViewRepositoryCustom {
  fun findByUidAndUserId(
      id: TypedId<AutoCategorizeRuleId>,
      userId: TypedId<UserId>
  ): AutoCategorizeRuleView?
}
