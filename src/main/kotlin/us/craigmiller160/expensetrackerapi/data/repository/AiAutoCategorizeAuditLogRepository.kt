package us.craigmiller160.expensetrackerapi.data.repository

import us.craigmiller160.expensetrackerapi.data.model.AiAutoCategorizeAuditLog

interface AiAutoCategorizeAuditLogRepository {
  fun createAuditLog(auditLog: AiAutoCategorizeAuditLog): AiAutoCategorizeAuditLog
  fun getAllAuditLogs(): List<AiAutoCategorizeAuditLog>
  fun deleteAllAuditLogs()
}
