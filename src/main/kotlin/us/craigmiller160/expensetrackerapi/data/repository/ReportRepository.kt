package us.craigmiller160.expensetrackerapi.data.repository

import org.springframework.data.domain.Page
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.projection.SpendingByMonth
import us.craigmiller160.expensetrackerapi.web.types.report.ReportRequest

interface ReportRepository {
  fun getSpendingByMonthAndCategory(
      userId: TypedId<UserId>,
      request: ReportRequest
  ): Pair<Page<SpendingByMonth>, SpendingByMonth?>
}
