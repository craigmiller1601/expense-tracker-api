package us.craigmiller160.expensetrackerapi.data.repository

interface EmbeddingSyncConfigRepository {
  fun resetEmbeddingSyncConfig()
  fun setInitialSyncComplete(initialSyncComplete: Boolean)
  fun isInitialSyncComplete(): Boolean
}
