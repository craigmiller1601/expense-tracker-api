package us.craigmiller160.expensetrackerapi.data.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.TransactionView
import us.craigmiller160.expensetrackerapi.web.types.transaction.SearchTransactionsRequest

interface TransactionRepositoryCustom {

  fun searchForTransactions(
      request: SearchTransactionsRequest,
      userId: TypedId<UserId>,
      page: Pageable
  ): Page<TransactionView>
}
