package us.craigmiller160.expensetrackerapi.data.repository.impl

import jakarta.transaction.Transactional
import java.time.LocalDate
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.SqlLoader
import us.craigmiller160.expensetrackerapi.data.constants.CategoryConstants
import us.craigmiller160.expensetrackerapi.data.projection.SpendingByCategory
import us.craigmiller160.expensetrackerapi.data.projection.SpendingByMonth
import us.craigmiller160.expensetrackerapi.data.repository.ReportRepository
import us.craigmiller160.expensetrackerapi.data.repository.toQueryType
import us.craigmiller160.expensetrackerapi.web.types.report.ReportCategoryIdFilterType
import us.craigmiller160.expensetrackerapi.web.types.report.ReportRequest

@Repository
class ReportRepositoryImpl(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val sqlLoader: SqlLoader
) : ReportRepository {

  @Transactional
  override fun getSpendingByMonthAndCategory(
      userId: TypedId<UserId>,
      request: ReportRequest
  ): Pair<Page<SpendingByMonth>, SpendingByMonth?> {
    val (spendingByMonth, nextMonthSpending) =
        getSpendingByMonth(userId, request, request.categoryIdType, request.categoryIds)
    val spendingByMonthCount =
        getSpendingByMonthCount(userId, request.categoryIdType, request.categoryIds)
    val months = spendingByMonth.map { it.month }

    val fullResults =
        if (months.isNotEmpty()) {
          val spendingByCategory =
              getSpendingByCategoryForMonths(
                  userId, months, request.categoryIdType, request.categoryIds)

          spendingByMonth.map { monthRecord ->
            mergeSpendingByMonthAndCategories(monthRecord, spendingByCategory)
          }
        } else {
          listOf()
        }

    val fullNextMonth =
        nextMonthSpending?.let { nextMonthSpendingActual ->
          val nextMonthSpendingByCategory =
              getSpendingByCategoryForMonths(
                  userId,
                  listOf(nextMonthSpendingActual.month),
                  request.categoryIdType,
                  request.categoryIds)
          mergeSpendingByMonthAndCategories(nextMonthSpendingActual, nextMonthSpendingByCategory)
        }

    return PageImpl(
        fullResults, PageRequest.of(request.pageNumber, request.pageSize), spendingByMonthCount) to
        fullNextMonth
  }

  private fun mergeSpendingByMonthAndCategories(
      spendingByMonth: SpendingByMonth,
      spendingByCategory: List<SpendingByCategory>
  ): SpendingByMonth =
      spendingByMonth.copy(
          categories =
              spendingByCategory.filter { categoryRecord ->
                categoryRecord.month == spendingByMonth.month
              })

  private fun getSpendingByCategoryForMonths(
      userId: TypedId<UserId>,
      months: List<LocalDate>,
      categoryIdType: ReportCategoryIdFilterType,
      categoryIds: List<TypedId<CategoryId>>
  ): List<SpendingByCategory> {
    val getSpendingByCategoryForMonthSql =
        sqlLoader.loadSql("reports/get_spending_by_category_for_month.sql")
    val finalWrapper =
        months
            .mapIndexed { index, month ->
              val sql =
                  getSpendingByCategoryForMonthSql
                      .replace(":theDate", ":theDate$index")
                      .replace(";", "")
              val params = mapOf("theDate$index" to month)
              SpendingByCategoryQueryWrapper(params = params, sql = sql)
            }
            .reduce { acc, record ->
              SpendingByCategoryQueryWrapper(
                  params = acc.params + record.params, sql = "(${acc.sql})\nUNION\n(${record.sql})")
            }
            .let { wrapper ->
              if (months.size > 1) {
                // Order By at the end is necessary because the union breaks the ordering for each
                // individual query
                wrapper.copy(sql = "${wrapper.sql} ORDER BY category_name ASC")
              } else {
                wrapper
              }
            }

    val params =
        MapSqlParameterSource()
            .addValues(finalWrapper.params)
            .addValue("userId", userId.uuid)
            .addCategoryIds(categoryIdType, categoryIds)
    return jdbcTemplate.query(finalWrapper.sql, params) { rs, _ ->
      SpendingByCategory(
          categoryId = rs.getString("category_id")?.let { TypedId(it) }
                  ?: CategoryConstants.UNKNOWN_CATEGORY.id,
          month = rs.getDate("month").toLocalDate(),
          categoryName = rs.getString("category_name") ?: CategoryConstants.UNKNOWN_CATEGORY.name,
          amount = rs.getBigDecimal("amount"),
          color = rs.getString("color") ?: CategoryConstants.UNKNOWN_CATEGORY.color)
    }
  }

  private fun getSpendingByMonthCount(
      userId: TypedId<UserId>,
      categoryIdType: ReportCategoryIdFilterType,
      categoryIds: List<TypedId<CategoryId>>
  ): Long {
    val getSpendingByMonthCountSql =
        sqlLoader.loadSql("reports/get_total_spending_by_month_count.sql")
    val params =
        MapSqlParameterSource()
            .addValue("userId", userId.uuid)
            .addCategoryIds(categoryIdType, categoryIds)
    return jdbcTemplate.queryForObject(getSpendingByMonthCountSql, params, Long::class.java)!!
  }

  private data class SpendingByCategoryQueryWrapper(val params: Map<String, Any>, val sql: String)

  private fun getSpendingByMonth(
      userId: TypedId<UserId>,
      request: ReportRequest,
      categoryIdType: ReportCategoryIdFilterType,
      categoryIds: List<TypedId<CategoryId>>
  ): Pair<List<SpendingByMonth>, SpendingByMonth?> {
    val getTotalSpendingByMonthSql = sqlLoader.loadSql("reports/get_total_spending_by_month.sql")
    val limit = if (request.includeNextRecord) request.pageSize + 1 else request.pageSize
    val totalSpendingByMonthParams =
        MapSqlParameterSource()
            .addValue("userId", userId.uuid)
            .addValue("offset", request.pageNumber * request.pageSize)
            .addValue("limit", limit)
            .addCategoryIds(categoryIdType, categoryIds)
    val results =
        jdbcTemplate.query(getTotalSpendingByMonthSql, totalSpendingByMonthParams) { rs, _ ->
          SpendingByMonth(
              month = rs.getDate("month").toLocalDate(),
              total = rs.getBigDecimal("total"),
              categories = listOf())
        }

    if (request.includeNextRecord && results.isNotEmpty() && results.size > request.pageSize) {
      return results.subList(0, results.size - 1) to results.lastOrNull()
    }
    return results to null
  }

  private fun MapSqlParameterSource.addCategoryIds(
      categoryIdType: ReportCategoryIdFilterType,
      categoryIds: List<TypedId<CategoryId>>
  ): MapSqlParameterSource {
    val (unknownCategoryIds, otherCategoryIds) =
        categoryIds.partition { categoryId -> CategoryConstants.UNKNOWN_CATEGORY.id == categoryId }
    val queryType =
        categoryIdType.toQueryType(unknownCategoryIds.isNotEmpty(), otherCategoryIds.isNotEmpty())

    val queryCategoryIds = otherCategoryIds.map { it.uuid }.ifEmpty { null }

    return this.addValue("categoryIdType", queryType.name).addValue("categoryIds", queryCategoryIds)
  }
}
