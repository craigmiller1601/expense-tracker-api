package us.craigmiller160.expensetrackerapi.data.repository

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.projection.NeedsAttentionCount
import us.craigmiller160.expensetrackerapi.data.projection.NeedsAttentionOldest

interface NeedsAttentionRepository {
  fun getAllNeedsAttentionCounts(userId: TypedId<UserId>): List<NeedsAttentionCount>

  fun getAllNeedsAttentionOldest(userId: TypedId<UserId>): List<NeedsAttentionOldest>
}
