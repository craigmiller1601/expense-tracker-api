package us.craigmiller160.expensetrackerapi.data.repository

import org.springframework.data.domain.Page
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.AutoCategorizeRuleView
import us.craigmiller160.expensetrackerapi.web.types.rules.AutoCategorizeRulePageRequest

interface AutoCategorizeRuleViewRepositoryCustom {
  fun searchForRules(
      request: AutoCategorizeRulePageRequest,
      userId: TypedId<UserId>
  ): Page<AutoCategorizeRuleView>
}
