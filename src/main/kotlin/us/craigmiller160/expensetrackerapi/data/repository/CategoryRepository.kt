package us.craigmiller160.expensetrackerapi.data.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.Category

interface CategoryRepository : JpaRepository<Category, TypedId<CategoryId>> {
  fun findAllByUserIdOrderByName(userId: TypedId<UserId>): List<Category>

  fun findByUidAndUserId(id: TypedId<CategoryId>, userId: TypedId<UserId>): Category?

  @Modifying(flushAutomatically = true, clearAutomatically = true)
  fun deleteByUidAndUserId(id: TypedId<CategoryId>, userId: TypedId<UserId>)

  fun existsByUidAndUserId(id: TypedId<CategoryId>, userId: TypedId<UserId>): Boolean
}
