package us.craigmiller160.expensetrackerapi.data.repository.impl

import java.sql.Timestamp
import java.sql.Types
import java.time.ZoneId
import java.time.ZonedDateTime
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.EmbeddingChangeLogId
import us.craigmiller160.expensetrackerapi.data.SqlLoader
import us.craigmiller160.expensetrackerapi.data.model.ChangeTrackedTable
import us.craigmiller160.expensetrackerapi.data.model.ChangeType
import us.craigmiller160.expensetrackerapi.data.model.EmbeddingChangeLog
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingChangeLogRepository
import us.craigmiller160.expensetrackerapi.extension.getNullableZonedDateTime
import us.craigmiller160.expensetrackerapi.extension.getTypedId
import us.craigmiller160.expensetrackerapi.extension.getUUID
import us.craigmiller160.expensetrackerapi.extension.getZonedDateTime

@Repository
class EmbeddingChangeLogJdbcRepository(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val sqlLoader: SqlLoader
) : EmbeddingChangeLogRepository {
  companion object {
    private const val GET_ALL_NOT_SYNCED_CHANGE_LOGS =
        "embeddingChangeLog/get-all-not-synced-change-logs.sql"
    private const val MARK_CHANGE_LOGS_AS_SYNCED =
        "embeddingChangeLog/mark-change-logs-as-synced.sql"
    private const val DELETE_ALL_CHANGE_LOGS = "embeddingChangeLog/delete-all-change-logs.sql"
    private const val GET_ALL_CHANGE_LOGS = "embeddingChangeLog/get-all-change-logs.sql"
    private const val CREATE_CHANGE_LOG = "embeddingChangeLog/create-change-log.sql"
    private const val DELETE_SYNCED_CHANGE_LOGS_OLDER_THAN_TIMESTAMP =
        "embeddingChangeLog/delete-synced-change-logs-older-than-timestamp.sql"
  }

  override fun getAllNotSyncedChangeLogs(): List<EmbeddingChangeLog> =
      sqlLoader.loadSql(GET_ALL_NOT_SYNCED_CHANGE_LOGS).let { sql ->
        jdbcTemplate.query(sql, MapSqlParameterSource(), embeddingChangeLogRowMapper)
      }

  override fun markChangeLogsAsSynced(changeLogIds: List<TypedId<EmbeddingChangeLogId>>) {
    val params =
        MapSqlParameterSource()
            .addValue("ids", changeLogIds.map { it.uuid })
            .addValue("timestamp", Timestamp.from(ZonedDateTime.now(ZoneId.of("UTC")).toInstant()))
    sqlLoader.loadSql(MARK_CHANGE_LOGS_AS_SYNCED).let { sql -> jdbcTemplate.update(sql, params) }
  }

  override fun deleteAllChangeLogs() =
      sqlLoader
          .loadSql(DELETE_ALL_CHANGE_LOGS)
          .let { sql -> jdbcTemplate.update(sql, MapSqlParameterSource()) }
          .let { Unit }

  override fun createChangeLog(changeLog: EmbeddingChangeLog) {
    val syncTimestamp =
        changeLog.syncTimestamp?.let { Timestamp.from(changeLog.changeTimestamp.toInstant()) }
    val params =
        MapSqlParameterSource()
            .addValue("id", changeLog.uid.uuid)
            .addValue("type", changeLog.type.name.lowercase())
            .addValue("tableName", changeLog.tableName.name.lowercase())
            .addValue("isSynced", changeLog.isSynced)
            .addValue("userId", changeLog.userId.uuid)
            .addValue("changeTimestamp", Timestamp.from(changeLog.changeTimestamp.toInstant()))
            .addValue("syncTimestamp", syncTimestamp, Types.TIMESTAMP)
            .addValue("recordId", changeLog.recordId)
    sqlLoader.loadSql(CREATE_CHANGE_LOG).let { sql -> jdbcTemplate.update(sql, params) }
  }

  override fun deleteSyncedChangeLogsOlderThanTimestamp(timestamp: ZonedDateTime) {
    val params =
        MapSqlParameterSource().addValue("timestamp", Timestamp.from(timestamp.toInstant()))
    sqlLoader.loadSql(DELETE_SYNCED_CHANGE_LOGS_OLDER_THAN_TIMESTAMP).let { sql ->
      jdbcTemplate.update(sql, params)
    }
  }

  override fun getAllChangeLogs(): List<EmbeddingChangeLog> =
      sqlLoader.loadSql(GET_ALL_CHANGE_LOGS).let { sql ->
        jdbcTemplate.query(sql, MapSqlParameterSource(), embeddingChangeLogRowMapper)
      }
}

private val embeddingChangeLogRowMapper: RowMapper<EmbeddingChangeLog> = RowMapper { rs, _ ->
  EmbeddingChangeLog(
      uid = rs.getTypedId("uid"),
      recordId = rs.getUUID("record_id"),
      tableName = ChangeTrackedTable.valueOf(rs.getString("table_name").uppercase()),
      type = ChangeType.valueOf(rs.getString("type").uppercase()),
      isSynced = rs.getBoolean("is_synced"),
      userId = rs.getTypedId("user_id"),
      changeTimestamp = rs.getZonedDateTime("change_timestamp"),
      syncTimestamp = rs.getNullableZonedDateTime("sync_timestamp"))
}
