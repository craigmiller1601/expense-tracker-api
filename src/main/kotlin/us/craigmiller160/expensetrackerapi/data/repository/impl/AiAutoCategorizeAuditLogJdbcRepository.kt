package us.craigmiller160.expensetrackerapi.data.repository.impl

import java.sql.Timestamp
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.data.SqlLoader
import us.craigmiller160.expensetrackerapi.data.model.AiAutoCategorizeAuditLog
import us.craigmiller160.expensetrackerapi.data.repository.AiAutoCategorizeAuditLogRepository
import us.craigmiller160.expensetrackerapi.extension.getTypedId
import us.craigmiller160.expensetrackerapi.extension.getZonedDateTime

@Repository
class AiAutoCategorizeAuditLogJdbcRepository(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
    private val sqlLoader: SqlLoader
) : AiAutoCategorizeAuditLogRepository {
  companion object {
    private const val CREATE_AUDIT_LOG_SQL = "aiAuditLog/create-audit-log.sql"
    private const val GET_ALL_AUDIT_LOGS_SQL = "aiAuditLog/get-all-audit-logs.sql"
    private const val DELETE_ALL_AUDIT_LOGS_SQL = "aiAuditLog/delete-all-audit-logs.sql"
  }

  override fun createAuditLog(auditLog: AiAutoCategorizeAuditLog): AiAutoCategorizeAuditLog {
    val params =
        MapSqlParameterSource()
            .addValue("id", auditLog.id.uuid)
            .addValue("operationId", auditLog.operationId.uuid)
            .addValue("batchIndex", auditLog.batchIndex)
            .addValue("timestamp", Timestamp.from(auditLog.timestamp.toInstant()))
            .addValue("aiResponse", auditLog.aiResponse)
            .addValue("success", auditLog.success)
            .addValue("error", auditLog.error)
    sqlLoader.loadSql(CREATE_AUDIT_LOG_SQL).let { sql -> jdbcTemplate.update(sql, params) }
    return auditLog
  }

  override fun getAllAuditLogs(): List<AiAutoCategorizeAuditLog> =
      sqlLoader.loadSql(GET_ALL_AUDIT_LOGS_SQL).let { sql ->
        jdbcTemplate.query(sql, MapSqlParameterSource(), aiAutoCategorizeAuditLogRowMapper)
      }

  override fun deleteAllAuditLogs() {
    sqlLoader.loadSql(DELETE_ALL_AUDIT_LOGS_SQL).let { sql ->
      jdbcTemplate.update(sql, MapSqlParameterSource())
    }
  }

  private val aiAutoCategorizeAuditLogRowMapper = RowMapper { rs, _ ->
    AiAutoCategorizeAuditLog(
        id = rs.getTypedId("id"),
        operationId = rs.getTypedId("operation_id"),
        batchIndex = rs.getInt("batch_index"),
        timestamp = rs.getZonedDateTime("timestamp"),
        aiResponse = rs.getString("ai_response"),
        success = rs.getBoolean("success"),
        error = rs.getString("error"))
  }
}
