package us.craigmiller160.expensetrackerapi.data.repository.impl

import com.querydsl.core.BooleanBuilder
import com.querydsl.jpa.JPQLQueryFactory
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.QAutoCategorizeRule
import us.craigmiller160.expensetrackerapi.data.querydsl.QueryDSLSupport
import us.craigmiller160.expensetrackerapi.data.repository.AutoCategorizeRuleRepositoryCustom

@Repository
class AutoCategorizeRuleRepositoryCustomImpl(private val queryFactory: JPQLQueryFactory) :
    AutoCategorizeRuleRepositoryCustom {

  override fun decrementOrdinals(
      userId: TypedId<UserId>,
      minOrdinal: Int,
      maxOrdinal: Int,
      excludeId: TypedId<AutoCategorizeRuleId>?
  ) {
    val whereClause = createOrdinalUpdateWhereClause(userId, minOrdinal, maxOrdinal, excludeId)
    queryFactory
        .update(QAutoCategorizeRule.autoCategorizeRule)
        .set(
            QAutoCategorizeRule.autoCategorizeRule.ordinal,
            QAutoCategorizeRule.autoCategorizeRule.ordinal.subtract(1))
        .set(
            QAutoCategorizeRule.autoCategorizeRule.version,
            QAutoCategorizeRule.autoCategorizeRule.version.add(1))
        .where(whereClause)
        .execute()
  }

  private fun createOrdinalUpdateWhereClause(
      userId: TypedId<UserId>,
      minOrdinal: Int,
      maxOrdinal: Int,
      excludeId: TypedId<AutoCategorizeRuleId>?
  ): BooleanBuilder =
      BooleanBuilder(QAutoCategorizeRule.autoCategorizeRule.userId.eq(userId))
          .and(QAutoCategorizeRule.autoCategorizeRule.ordinal.loe(maxOrdinal))
          .and(QAutoCategorizeRule.autoCategorizeRule.ordinal.goe(minOrdinal))
          .let(
              QueryDSLSupport.andIfNotNull(excludeId) { id ->
                QAutoCategorizeRule.autoCategorizeRule.uid.ne(id)
              })

  override fun incrementOrdinals(
      userId: TypedId<UserId>,
      minOrdinal: Int,
      maxOrdinal: Int,
      excludeId: TypedId<AutoCategorizeRuleId>?
  ) {
    val whereClause = createOrdinalUpdateWhereClause(userId, minOrdinal, maxOrdinal, excludeId)
    queryFactory
        .update(QAutoCategorizeRule.autoCategorizeRule)
        .set(
            QAutoCategorizeRule.autoCategorizeRule.ordinal,
            QAutoCategorizeRule.autoCategorizeRule.ordinal.add(1))
        .set(
            QAutoCategorizeRule.autoCategorizeRule.version,
            QAutoCategorizeRule.autoCategorizeRule.version.add(1))
        .where(whereClause)
        .execute()
  }
}
