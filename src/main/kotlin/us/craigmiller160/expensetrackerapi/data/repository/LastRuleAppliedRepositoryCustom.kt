package us.craigmiller160.expensetrackerapi.data.repository

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.projection.LastRuleAppliedForTransaction

interface LastRuleAppliedRepositoryCustom {
  fun getLastRuleDetailsForTransaction(
      userId: TypedId<UserId>,
      transactionId: TypedId<TransactionId>
  ): LastRuleAppliedForTransaction?
}
