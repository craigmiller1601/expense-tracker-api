package us.craigmiller160.expensetrackerapi.data.repository

import java.time.ZonedDateTime
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.EmbeddingChangeLogId
import us.craigmiller160.expensetrackerapi.data.model.EmbeddingChangeLog

interface EmbeddingChangeLogRepository {
  fun getAllNotSyncedChangeLogs(): List<EmbeddingChangeLog>
  fun markChangeLogsAsSynced(changeLogIds: List<TypedId<EmbeddingChangeLogId>>)
  fun deleteAllChangeLogs()
  fun createChangeLog(changeLog: EmbeddingChangeLog)
  fun deleteSyncedChangeLogsOlderThanTimestamp(timestamp: ZonedDateTime)
  fun getAllChangeLogs(): List<EmbeddingChangeLog>
}
