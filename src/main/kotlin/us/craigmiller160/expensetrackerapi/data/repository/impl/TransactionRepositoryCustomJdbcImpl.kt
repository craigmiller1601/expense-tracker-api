package us.craigmiller160.expensetrackerapi.data.repository.impl

import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.SqlLoader
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionAndCategory
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepositoryCustomJdbc
import us.craigmiller160.expensetrackerapi.extension.getLocalDate
import us.craigmiller160.expensetrackerapi.extension.getNullableLong
import us.craigmiller160.expensetrackerapi.extension.getNullableTypedId
import us.craigmiller160.expensetrackerapi.extension.getTypedId
import us.craigmiller160.expensetrackerapi.extension.getZonedDateTime

@Repository
class TransactionRepositoryCustomJdbcImpl(
    private val sqlLoader: SqlLoader,
    private val jdbcTemplate: NamedParameterJdbcTemplate
) : TransactionRepositoryCustomJdbc {
  companion object {
    private const val CATEGORIZE_TRANSACTION = "transactions/categorize_transaction.sql"
    private const val GET_ALL_TRANSACTIONS_ALL_USERS =
        "transactions/get-all-transactions-all-users.sql"
  }

  @Transactional
  override fun categorizeAllTransactions(
      userId: TypedId<UserId>,
      transactionsWithCategories: List<TransactionAndCategory>
  ): Int {
    val batchParameters =
        transactionsWithCategories.map { (txnId, catId) ->
          MapSqlParameterSource()
              .addValue("userId", userId.uuid)
              .addValue("transactionId", txnId.uuid)
              .addValue("categoryId", catId.uuid)
        }
    return sqlLoader
        .loadSql(CATEGORIZE_TRANSACTION)
        .let { sql -> jdbcTemplate.batchUpdate(sql, batchParameters.toTypedArray()) }
        .fold(0) { a, b -> a + b }
  }

  override fun getAllTransactionsAllUsers(pageNumber: Long, pageSize: Long): List<Transaction> {
    val params =
        MapSqlParameterSource()
            .addValue("offset", pageNumber * pageSize)
            .addValue("limit", pageSize)
    return sqlLoader.loadSql(GET_ALL_TRANSACTIONS_ALL_USERS).let { sql ->
      jdbcTemplate.query(sql, params, transactionRowMapper)
    }
  }
}

private val transactionRowMapper: RowMapper<Transaction> = RowMapper { rs, index ->
  Transaction(
          userId = rs.getTypedId("user_id"),
          expenseDate = rs.getLocalDate("expense_date"),
          description = rs.getString("description"),
          amount = rs.getBigDecimal("amount"),
          contentHash = rs.getString("content_hash"),
          markNotDuplicateNano = rs.getNullableLong("mark_not_duplicate_nano"),
          confirmed = rs.getBoolean("confirmed"),
          categoryId = rs.getNullableTypedId("category_id"),
      )
      .apply {
        uid = rs.getTypedId("uid")
        created = rs.getZonedDateTime("created")
        updated = rs.getZonedDateTime("updated")
        version = rs.getLong("version")
      }
}
