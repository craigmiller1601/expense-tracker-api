package us.craigmiller160.expensetrackerapi.data.repository

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionAndCategory

interface TransactionRepositoryCustomJdbc {
  fun categorizeAllTransactions(
      userId: TypedId<UserId>,
      transactionsWithCategories: List<TransactionAndCategory>
  ): Int

  fun getAllTransactionsAllUsers(pageNumber: Long, pageSize: Long): List<Transaction>
}
