package us.craigmiller160.expensetrackerapi.data.ai.model.chat

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.data.model.TransactionCommon

data class UncategorizedTransaction(val uid: TypedId<TransactionId>, val description: String)

fun TransactionCommon.toUncategorizedTransaction(): UncategorizedTransaction =
    UncategorizedTransaction(uid = uid, description = description)
