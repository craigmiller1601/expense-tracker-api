package us.craigmiller160.expensetrackerapi.data.ai.model.embedding

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

data class EmbeddedTransactionSearchResult(
    val uid: TypedId<TransactionId>,
    val distance: Double,
    val certainty: Double
)
