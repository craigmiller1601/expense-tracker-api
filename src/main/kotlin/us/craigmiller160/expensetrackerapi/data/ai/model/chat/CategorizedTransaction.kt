package us.craigmiller160.expensetrackerapi.data.ai.model.chat

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

data class CategorizedTransaction(val uid: TypedId<TransactionId>, val category: String?)
