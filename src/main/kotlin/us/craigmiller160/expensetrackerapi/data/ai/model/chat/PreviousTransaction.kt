package us.craigmiller160.expensetrackerapi.data.ai.model.chat

import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.data.model.TransactionView

data class PreviousTransaction(
    val expenseDate: LocalDate,
    val description: String,
    val category: String
) : Comparable<PreviousTransaction> {
  override fun compareTo(other: PreviousTransaction): Int {
    val expenseDateCompare = expenseDate.compareTo(other.expenseDate)
    if (expenseDateCompare == 0) {
      return description.compareTo(other.description)
    }
    return expenseDateCompare
  }
}

fun TransactionView.toPreviousTransaction(): PreviousTransaction =
    PreviousTransaction(
        expenseDate = expenseDate,
        description = description,
        category = categoryName
                ?: throw IllegalArgumentException("Previous transaction must have a category"))
