package us.craigmiller160.expensetrackerapi.data.ai.weaviate

import io.weaviate.client.base.WeaviateError
import io.weaviate.client.v1.batch.model.ObjectsGetResponseAO2Result.ErrorResponse
import io.weaviate.client.v1.graphql.model.GraphQLError

private const val MESSAGE_SEPARATOR = ", "
private const val EXCEPTION_SEPARATOR = "\n"

class WeaviateException(message: String) : RuntimeException(message) {
  companion object
}

fun WeaviateError.toException(): WeaviateException =
    messages.joinToString(MESSAGE_SEPARATOR) { it.message }.let { WeaviateException(it) }

fun Array<GraphQLError>.toException(): WeaviateException =
    joinToString(MESSAGE_SEPARATOR) { it.message }.let { WeaviateException(it) }

fun ErrorResponse.toException(): WeaviateException =
    error.joinToString(MESSAGE_SEPARATOR) { it.message }.let { WeaviateException(it) }

fun WeaviateException.Companion.aggregate(exceptions: List<WeaviateException>): WeaviateException =
    exceptions
        .mapIndexed { index, exception -> "Exception $index = ${exception.message}" }
        .joinToString(EXCEPTION_SEPARATOR)
        .let { WeaviateException(it) }
