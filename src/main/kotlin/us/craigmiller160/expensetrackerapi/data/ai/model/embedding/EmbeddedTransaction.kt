package us.craigmiller160.expensetrackerapi.data.ai.model.embedding

import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionForEmbedding

data class EmbeddedTransaction(
    val uid: TypedId<TransactionId>,
    val userId: TypedId<UserId>,
    val description: String,
    val expenseDate: LocalDate,
    val vectors: List<Float>,
    val isCategorized: Boolean
)

fun TransactionForEmbedding.toEmbeddedTransaction(vectors: List<Float>): EmbeddedTransaction =
    EmbeddedTransaction(
        uid = uid,
        description = description,
        expenseDate = expenseDate,
        userId = userId,
        vectors = vectors,
        isCategorized = isCategorized)
