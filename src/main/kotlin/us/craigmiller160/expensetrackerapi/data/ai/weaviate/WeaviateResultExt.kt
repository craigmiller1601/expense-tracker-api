package us.craigmiller160.expensetrackerapi.data.ai.weaviate

import io.weaviate.client.base.Result
import io.weaviate.client.v1.batch.model.ObjectGetResponse
import io.weaviate.client.v1.graphql.model.GraphQLResponse

fun <T> Result<T>.toKotlinResult(): kotlin.Result<T> = runCatching { getOrThrow() }

fun <T> Result<T>.getOrThrow(): T {
  if (hasErrors()) {
    throw error.toException()
  }

  if (result == null) {
    return result
  }

  if (result is Array<*>) {
    val failedResponses =
        (result as Array<*>).filterIsInstance<ObjectGetResponse>().filter { response ->
          response.result.status.lowercase() == "failed"
        }
    if (failedResponses.isNotEmpty()) {
      throw failedResponses
          .map { response -> response.result.errors.toException() }
          .let { WeaviateException.aggregate(it) }
    }
  }

  return result
}

val GraphQLResponse.wrapData: WeaviateGraphQLObject
  get() {
    check(data is Map<*, *>) { "GraphQLResponse is not of type Map" }
    return (data as Map<String, Any?>).let { WeaviateGraphQLObject(it) }
  }

fun Result<GraphQLResponse>.toDataAsMapKotlinResult(): kotlin.Result<GraphQLResponse> =
    kotlin.runCatching { getOrThrow() }

fun Result<GraphQLResponse>.getOrThrow(): GraphQLResponse {
  if (hasErrors()) {
    throw error.toException()
  }

  if ((result.errors?.size ?: 0) > 0) {
    throw result.errors.toException()
  }
  return result
}
