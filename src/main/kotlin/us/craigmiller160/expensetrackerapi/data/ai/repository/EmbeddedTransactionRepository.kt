package us.craigmiller160.expensetrackerapi.data.ai.repository

import java.time.LocalDate
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransactionSearchResult

@Repository
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
interface EmbeddedTransactionRepository {
  fun deleteAllTransactionsAllTenants()

  fun deleteAllTransactions(userId: TypedId<UserId>)

  fun getAllTenants(): List<TypedId<UserId>>

  fun insertTransaction(transaction: EmbeddedTransaction)

  fun updateTransaction(transaction: EmbeddedTransaction)

  fun updateTransactionIsCategorized(
      userId: TypedId<UserId>,
      transactionId: TypedId<TransactionId>,
      isCategorized: Boolean
  )

  fun getTransactionById(userId: TypedId<UserId>, id: TypedId<TransactionId>): EmbeddedTransaction?

  fun getAllTransactionsById(
      userId: TypedId<UserId>,
      ids: List<TypedId<TransactionId>>
  ): List<EmbeddedTransaction>

  fun getAllTransactions(
      userId: TypedId<UserId>,
      pageNumber: Int,
      pageSize: Int
  ): List<EmbeddedTransaction>

  fun getTransactionCount(userId: TypedId<UserId>): Long

  fun insertAllTransactions(transactions: List<EmbeddedTransaction>)

  fun searchForTransactions(
      userId: TypedId<UserId>,
      embeddedQuery: List<Float>,
      startDate: LocalDate,
      endDate: LocalDate,
      limit: Int
  ): List<EmbeddedTransactionSearchResult>

  fun deleteTransactionById(userId: TypedId<UserId>, id: TypedId<TransactionId>)

  fun deleteAllTransactionsById(userId: TypedId<UserId>, ids: List<TypedId<TransactionId>>)
}
