package us.craigmiller160.expensetrackerapi.data.ai.repository

import io.weaviate.client.WeaviateClient
import io.weaviate.client.v1.data.model.WeaviateObject
import io.weaviate.client.v1.filters.Operator
import io.weaviate.client.v1.filters.WhereFilter
import io.weaviate.client.v1.graphql.query.argument.NearVectorArgument
import io.weaviate.client.v1.graphql.query.argument.SortArgument
import io.weaviate.client.v1.graphql.query.argument.SortOrder
import io.weaviate.client.v1.graphql.query.argument.WhereArgument
import io.weaviate.client.v1.graphql.query.fields.Field
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Date
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.config.WeaviateProperties
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransactionSearchResult
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.ADDITIONAL_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.CERTAINTY_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.COUNT_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.DESCRIPTION_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.DISTANCE_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.EXPENSE_DATE_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.ID_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.IS_CATEGORIZED_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.META_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.VECTOR_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.WeaviateGraphQLObject
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.getOrThrow
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.wrapData

@Repository
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class WeaviateEmbeddedTransactionRepository(
    private val weaviateClient: WeaviateClient,
    private val weaviateProperties: WeaviateProperties
) : EmbeddedTransactionRepository {
  override fun deleteAllTransactionsAllTenants() {
    getAllTenants().forEach { tenant -> deleteAllTransactions(tenant) }
  }

  override fun getAllTenants(): List<TypedId<UserId>> =
      weaviateClient
          .schema()
          .tenantsGetter()
          .withClassName(weaviateProperties.classes.transactions)
          .run()
          .getOrThrow()
          .map { TypedId(it.name) }

  override fun deleteAllTransactions(userId: TypedId<UserId>) {
    val tenantExists =
        weaviateClient
            .schema()
            .tenantsExists()
            .withClassName(weaviateProperties.classes.transactions)
            .withTenant(userId.uuid.toString())
            .run()
            .getOrThrow()

    if (!tenantExists) {
      return
    }

    weaviateClient
        .batch()
        .objectsBatchDeleter()
        .withClassName(weaviateProperties.classes.transactions)
        .withTenant(userId.uuid.toString())
        .withWhere(
            WhereFilter.builder()
                .path(DESCRIPTION_FIELD_NAME)
                .operator(Operator.Like)
                .valueText("*")
                .build())
        .run()
        .getOrThrow()

    weaviateClient
        .schema()
        .tenantsDeleter()
        .withClassName(weaviateProperties.classes.transactions)
        .withTenants(userId.uuid.toString())
        .run()
        .getOrThrow()
  }

  override fun insertTransaction(transaction: EmbeddedTransaction) {
    val properties =
        mapOf(
            DESCRIPTION_FIELD_NAME to transaction.description,
            IS_CATEGORIZED_FIELD_NAME to transaction.isCategorized,
            EXPENSE_DATE_FIELD_NAME to transaction.expenseDate.toWeaviateDateString())
    weaviateClient
        .data()
        .creator()
        .withClassName(weaviateProperties.classes.transactions)
        .withProperties(properties)
        .withVector(transaction.vectors.toTypedArray())
        .withTenant(transaction.userId.uuid.toString())
        .withID(transaction.uid.uuid.toString())
        .run()
        .getOrThrow()
  }

  override fun updateTransaction(transaction: EmbeddedTransaction) {
    val properties =
        mapOf(
            DESCRIPTION_FIELD_NAME to transaction.description,
            IS_CATEGORIZED_FIELD_NAME to transaction.isCategorized,
            EXPENSE_DATE_FIELD_NAME to transaction.expenseDate.toWeaviateDateString())
    weaviateClient
        .data()
        .updater()
        .withClassName(weaviateProperties.classes.transactions)
        .withTenant(transaction.userId.uuid.toString())
        .withVector(transaction.vectors.toTypedArray())
        .withID(transaction.uid.uuid.toString())
        .withProperties(properties)
        .run()
        .getOrThrow()
  }

  override fun updateTransactionIsCategorized(
      userId: TypedId<UserId>,
      transactionId: TypedId<TransactionId>,
      isCategorized: Boolean
  ) {
    val properties =
        mapOf(
            IS_CATEGORIZED_FIELD_NAME to isCategorized,
        )

    weaviateClient
        .data()
        .updater()
        .withMerge()
        .withClassName(weaviateProperties.classes.transactions)
        .withTenant(userId.uuid.toString())
        .withID(transactionId.uuid.toString())
        .withProperties(properties)
        .run()
        .getOrThrow()
  }

  @Suppress("performance.SpreadOperator")
  override fun getAllTransactionsById(
      userId: TypedId<UserId>,
      ids: List<TypedId<TransactionId>>
  ): List<EmbeddedTransaction> {
    val descriptionField = Field.builder().name(DESCRIPTION_FIELD_NAME).build()
    val expenseDateField = Field.builder().name(EXPENSE_DATE_FIELD_NAME).build()
    val isCategorizedField = Field.builder().name(IS_CATEGORIZED_FIELD_NAME).build()
    val additionalFields =
        Field.builder()
            .name(ADDITIONAL_FIELD_NAME)
            .fields(
                Field.builder().name(ID_FIELD_NAME).build(),
                Field.builder().name(VECTOR_FIELD_NAME).build())
            .build()

    val filter =
        WhereFilter.builder()
            .path(ID_FIELD_NAME)
            .operator(Operator.ContainsAny)
            .valueString(*ids.map { it.toString() }.toTypedArray())
            .build()

    return weaviateClient
        .graphQL()
        .get()
        .withTenant(userId.uuid.toString())
        .withClassName(weaviateProperties.classes.transactions)
        .withWhere(WhereArgument.builder().filter(filter).build())
        .withFields(descriptionField, expenseDateField, isCategorizedField, additionalFields)
        .run()
        .getOrThrow()
        .wrapData
        .toEmbeddedTransactionList(userId, weaviateProperties.classes.transactions)
  }

  override fun getTransactionById(
      userId: TypedId<UserId>,
      uid: TypedId<TransactionId>
  ): EmbeddedTransaction? =
      weaviateClient
          .data()
          .objectsGetter()
          .withClassName(weaviateProperties.classes.transactions)
          .withID(uid.uuid.toString())
          .withTenant(userId.uuid.toString())
          .withVector()
          .run()
          ?.getOrThrow()
          ?.firstOrNull()
          ?.let { it.toEmbeddedTransaction() }

  override fun getAllTransactions(
      userId: TypedId<UserId>,
      pageNumber: Int,
      pageSize: Int
  ): List<EmbeddedTransaction> {
    val offset = pageNumber * pageSize

    val sort =
        SortArgument.builder().path(arrayOf(EXPENSE_DATE_FIELD_NAME)).order(SortOrder.asc).build()

    val descriptionField = Field.builder().name(DESCRIPTION_FIELD_NAME).build()
    val expenseDateField = Field.builder().name(EXPENSE_DATE_FIELD_NAME).build()
    val isCategorizedField = Field.builder().name(IS_CATEGORIZED_FIELD_NAME).build()
    val additionalFields =
        Field.builder()
            .name(ADDITIONAL_FIELD_NAME)
            .fields(
                Field.builder().name(ID_FIELD_NAME).build(),
                Field.builder().name(VECTOR_FIELD_NAME).build())
            .build()

    return weaviateClient
        .graphQL()
        .get()
        .withClassName(weaviateProperties.classes.transactions)
        .withOffset(offset)
        .withLimit(pageSize)
        .withSort(sort)
        .withFields(descriptionField, expenseDateField, isCategorizedField, additionalFields)
        .withTenant(userId.uuid.toString())
        .run()
        .getOrThrow()
        .wrapData
        .toEmbeddedTransactionList(userId, weaviateProperties.classes.transactions)
  }

  override fun getTransactionCount(userId: TypedId<UserId>): Long {
    val metaCountField =
        Field.builder()
            .name(META_FIELD_NAME)
            .fields(Field.builder().name(COUNT_FIELD_NAME).build())
            .build()

    return weaviateClient
        .graphQL()
        .aggregate()
        .withClassName(weaviateProperties.classes.transactions)
        .withFields(metaCountField)
        .withTenant(userId.uuid.toString())
        .run()
        .getOrThrow()
        .wrapData
        .toCount(weaviateProperties.classes.transactions)
  }

  @Suppress("performance.SpreadOperator")
  override fun insertAllTransactions(transactions: List<EmbeddedTransaction>) {
    transactions
        .map { txn ->
          val properties =
              mapOf(
                  DESCRIPTION_FIELD_NAME to txn.description,
                  IS_CATEGORIZED_FIELD_NAME to txn.isCategorized,
                  EXPENSE_DATE_FIELD_NAME to txn.expenseDate.toWeaviateDateString())
          WeaviateObject.builder()
              .className(weaviateProperties.classes.transactions)
              .properties(properties)
              .vector(txn.vectors.toTypedArray())
              .id(txn.uid.uuid.toString())
              .tenant(txn.userId.uuid.toString())
              .build()
        }
        .let { objects ->
          weaviateClient
              .batch()
              .objectsBatcher()
              .withObjects(*objects.toTypedArray())
              .run()
              .getOrThrow()
        }
  }

  override fun searchForTransactions(
      userId: TypedId<UserId>,
      embeddedQuery: List<Float>,
      startDate: LocalDate,
      endDate: LocalDate,
      limit: Int
  ): List<EmbeddedTransactionSearchResult> {
    val tenantExists =
        weaviateClient
            .schema()
            .tenantsExists()
            .withClassName(weaviateProperties.classes.transactions)
            .withTenant(userId.uuid.toString())
            .run()
            .getOrThrow()
    if (!tenantExists) {
      return listOf()
    }

    val additionalFields =
        Field.builder()
            .name(ADDITIONAL_FIELD_NAME)
            .fields(
                Field.builder().name(ID_FIELD_NAME).build(),
                Field.builder().name(CERTAINTY_FIELD_NAME).build(),
                Field.builder().name(DISTANCE_FIELD_NAME).build())
            .build()

    val minDate = Date.from(startDate.atStartOfDay().toInstant(ZoneOffset.UTC))
    val maxDate = Date.from(endDate.atStartOfDay().toInstant(ZoneOffset.UTC))

    val whereFilter =
        WhereFilter.builder()
            .operator(Operator.And)
            .operands(
                WhereFilter.builder()
                    .path(EXPENSE_DATE_FIELD_NAME)
                    .operator(Operator.GreaterThanEqual)
                    .valueDate(minDate)
                    .build(),
                WhereFilter.builder()
                    .path(EXPENSE_DATE_FIELD_NAME)
                    .operator(Operator.LessThanEqual)
                    .valueDate(maxDate)
                    .build(),
                WhereFilter.builder()
                    .path(IS_CATEGORIZED_FIELD_NAME)
                    .operator(Operator.Equal)
                    .valueBoolean(true)
                    .build())
            .build()

    return weaviateClient
        .graphQL()
        .get()
        .withClassName(weaviateProperties.classes.transactions)
        .withTenant(userId.uuid.toString())
        .withFields(additionalFields)
        .withSort(
            SortArgument.builder()
                .path(arrayOf(EXPENSE_DATE_FIELD_NAME))
                .order(SortOrder.desc)
                .build())
        .withWhere(WhereArgument.builder().filter(whereFilter).build())
        .withNearVector(NearVectorArgument.builder().vector(embeddedQuery.toTypedArray()).build())
        .withLimit(limit)
        .run()
        .getOrThrow()
        .wrapData
        .getObject("Get")
        .getObjectArray(weaviateProperties.classes.transactions)
        .map { txn ->
          val additional = txn.getObject(ADDITIONAL_FIELD_NAME)
          val id = additional.getString(ID_FIELD_NAME)
          val distance = additional.getDouble(DISTANCE_FIELD_NAME)
          val certainty = additional.getDouble(CERTAINTY_FIELD_NAME)
          EmbeddedTransactionSearchResult(
              uid = TypedId(id), distance = distance, certainty = certainty)
        }
  }

  override fun deleteTransactionById(userId: TypedId<UserId>, id: TypedId<TransactionId>) {
    weaviateClient
        .data()
        .deleter()
        .withClassName(weaviateProperties.classes.transactions)
        .withTenant(userId.uuid.toString())
        .withID(id.uuid.toString())
        .run()
        .getOrThrow()
  }

  @Suppress("performance.SpreadOperator")
  override fun deleteAllTransactionsById(
      userId: TypedId<UserId>,
      ids: List<TypedId<TransactionId>>
  ) {
    weaviateClient
        .batch()
        .objectsBatchDeleter()
        .withClassName(weaviateProperties.classes.transactions)
        .withTenant(userId.uuid.toString())
        .withWhere(
            WhereFilter.builder()
                .path(ID_FIELD_NAME)
                .operator(Operator.ContainsAny)
                .valueText(*ids.map { it.uuid.toString() }.toTypedArray())
                .build())
        .run()
        .getOrThrow()
  }
}

private fun WeaviateGraphQLObject.toCount(className: String): Long =
    getObject("Aggregate")
        .getObjectArray(className)
        .first()
        .getObject("meta")
        .getDouble("count")
        .toLong()

private fun WeaviateGraphQLObject.toEmbeddedTransactionList(
    userId: TypedId<UserId>,
    className: String
): List<EmbeddedTransaction> =
    getObject("Get").getObjectArray(className).map { obj ->
      val expenseDate = obj.getString(EXPENSE_DATE_FIELD_NAME).fromWeaviateDateString()
      val description = obj.getString(DESCRIPTION_FIELD_NAME)
      val isCategorized = obj.getBoolean(IS_CATEGORIZED_FIELD_NAME)
      val additional = obj.getObject(ADDITIONAL_FIELD_NAME)
      val id = additional.getString(ID_FIELD_NAME).let { TypedId<TransactionId>(it) }
      // Weaviate requires the input as Floats, but is returning/deserializing as Doubles...
      val vectors = additional.getDoubleArray(VECTOR_FIELD_NAME).map { it.toFloat() }
      EmbeddedTransaction(
          uid = id,
          expenseDate = expenseDate,
          description = description,
          vectors = vectors,
          userId = userId,
          isCategorized = isCategorized)
    }

private fun LocalDate.toWeaviateDateString(): String =
    ZonedDateTime.of(this, LocalTime.MIDNIGHT, ZoneId.of("UTC"))
        .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)

private fun String.fromWeaviateDateString(): LocalDate =
    ZonedDateTime.parse(this, DateTimeFormatter.ISO_OFFSET_DATE_TIME).toLocalDate()

private fun WeaviateObject.toEmbeddedTransaction(): EmbeddedTransaction =
    EmbeddedTransaction(
        uid = TypedId(id),
        description = properties[DESCRIPTION_FIELD_NAME] as String,
        expenseDate = (properties[EXPENSE_DATE_FIELD_NAME] as String).fromWeaviateDateString(),
        vectors = vector.toList(),
        userId = TypedId(tenant),
        isCategorized = properties[IS_CATEGORIZED_FIELD_NAME] as Boolean)
