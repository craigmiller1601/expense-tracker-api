package us.craigmiller160.expensetrackerapi.data.ai.weaviate

const val TEXT_FIELD_NAME = "text"
const val DESCRIPTION_FIELD_NAME = "description"
const val IS_CATEGORIZED_FIELD_NAME = "isCategorized"
const val EXPENSE_DATE_FIELD_NAME = "expense_date"
const val COUNT_FIELD_NAME = "count"
const val META_FIELD_NAME = "meta"
const val CREATION_TIME_UNIX_FIELD_NAME = "_creationTimeUnix"
const val ADDITIONAL_FIELD_NAME = "_additional"
const val VECTOR_FIELD_NAME = "vector"
const val ID_FIELD_NAME = "id"
const val DISTANCE_FIELD_NAME = "distance"
const val CERTAINTY_FIELD_NAME = "certainty"
