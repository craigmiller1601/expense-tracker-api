package us.craigmiller160.expensetrackerapi.data.ai.weaviate

data class WeaviateGraphQLObject(private val data: Map<String, Any?>) {
  fun getString(key: String): String = data[key] as String
  fun getBoolean(key: String): Boolean = data[key] as Boolean
  fun getInt(key: String): Int = data[key] as Int
  fun getLong(key: String): Long = data[key] as Long
  fun getObject(key: String): WeaviateGraphQLObject =
      (data[key] as Map<String, Any?>).let { WeaviateGraphQLObject(it) }
  fun getObjectArray(key: String): List<WeaviateGraphQLObject> =
      (data[key] as List<Map<String, Any?>>).map { WeaviateGraphQLObject(it) }
  fun getDouble(key: String): Double = data[key] as Double
  fun getFloatArray(key: String): List<Float> = data[key] as List<Float>
  fun getDoubleArray(key: String): List<Double> = data[key] as List<Double>
}
