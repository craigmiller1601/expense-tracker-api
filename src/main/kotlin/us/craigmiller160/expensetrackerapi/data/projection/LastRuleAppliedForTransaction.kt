package us.craigmiller160.expensetrackerapi.data.projection

import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.LastRuleAppliedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId

data class LastRuleAppliedForTransaction(
    val uid: TypedId<LastRuleAppliedId>,
    val ruleId: TypedId<AutoCategorizeRuleId>,
    val transactionId: TypedId<TransactionId>,
    val userId: TypedId<UserId>,
    val categoryId: TypedId<CategoryId>,
    val categoryName: String,
    val ordinal: Int,
    val regex: String,
    val startDate: LocalDate? = null,
    val endDate: LocalDate? = null,
    val minAmount: BigDecimal? = null,
    val maxAmount: BigDecimal? = null
)
