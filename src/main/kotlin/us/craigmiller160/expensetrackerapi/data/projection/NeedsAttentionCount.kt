package us.craigmiller160.expensetrackerapi.data.projection

data class NeedsAttentionCount(val type: NeedsAttentionType, val count: Long)
