package us.craigmiller160.expensetrackerapi.data.projection

import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId

data class SpendingByCategory(
    val categoryId: TypedId<CategoryId>,
    val month: LocalDate,
    val categoryName: String,
    val color: String,
    val amount: BigDecimal
)
