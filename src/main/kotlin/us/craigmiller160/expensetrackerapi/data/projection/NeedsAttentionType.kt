package us.craigmiller160.expensetrackerapi.data.projection

enum class NeedsAttentionType {
  UNCONFIRMED,
  DUPLICATE,
  UNCATEGORIZED,
  POSSIBLE_REFUND
}
