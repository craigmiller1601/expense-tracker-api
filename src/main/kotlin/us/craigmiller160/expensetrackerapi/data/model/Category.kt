package us.craigmiller160.expensetrackerapi.data.model

import jakarta.persistence.Entity
import jakarta.persistence.Table
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.common.data.typedid.jpatype.JpaTypedId
import us.craigmiller160.expensetrackerapi.data.model.core.MutableTableEntity

@Entity
@Table(name = "categories")
class Category(var name: String, @JpaTypedId var userId: TypedId<UserId>, var color: String) :
    MutableTableEntity<CategoryId>()
