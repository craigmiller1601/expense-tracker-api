package us.craigmiller160.expensetrackerapi.data.model

import org.springframework.data.domain.Sort
import us.craigmiller160.expensetrackerapi.web.types.SortDirection

fun SortDirection.toSpringSortDirection(): Sort.Direction =
    when (this) {
      SortDirection.ASC -> Sort.Direction.ASC
      SortDirection.DESC -> Sort.Direction.DESC
    }
