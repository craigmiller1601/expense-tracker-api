package us.craigmiller160.expensetrackerapi.data.model.core

import jakarta.persistence.MappedSuperclass

@MappedSuperclass
abstract class ViewEntity<T> : DatabaseRecord<T>() {
  override fun onPrePersist() {
    error("Cannot persist a view")
  }

  override fun onPreUpdate() {
    error("Cannot update a view")
  }
}
