package us.craigmiller160.expensetrackerapi.data.model

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.EmbeddingSyncConfigId

data class EmbeddingSyncConfig(
    val uid: TypedId<EmbeddingSyncConfigId>,
    val initialSyncComplete: Boolean
)
