package us.craigmiller160.expensetrackerapi.data.model

import us.craigmiller160.expensetrackerapi.web.types.transaction.TransactionSortKey

fun TransactionSortKey.toColumnName(): String =
    when (this) {
      TransactionSortKey.EXPENSE_DATE -> "expenseDate"
    }
