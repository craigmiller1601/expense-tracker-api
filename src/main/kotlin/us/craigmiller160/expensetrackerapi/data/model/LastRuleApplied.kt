package us.craigmiller160.expensetrackerapi.data.model

import jakarta.persistence.Entity
import jakarta.persistence.Table
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.LastRuleAppliedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.common.data.typedid.jpatype.JpaTypedId
import us.craigmiller160.expensetrackerapi.data.model.core.ImmutableTableEntity

@Entity
@Table(name = "last_rule_applied")
class LastRuleApplied(
    @JpaTypedId var userId: TypedId<UserId>,
    @JpaTypedId var ruleId: TypedId<AutoCategorizeRuleId>,
    @JpaTypedId var transactionId: TypedId<TransactionId>
) : ImmutableTableEntity<LastRuleAppliedId>()
