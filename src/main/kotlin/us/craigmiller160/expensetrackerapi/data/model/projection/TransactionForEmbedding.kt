package us.craigmiller160.expensetrackerapi.data.model.projection

import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.data.model.TransactionView

data class TransactionForEmbedding(
    val uid: TypedId<TransactionId>,
    val userId: TypedId<UserId>,
    val expenseDate: LocalDate,
    val description: String,
    val isCategorized: Boolean,
)

fun Transaction.toTransactionForEmbedding(): TransactionForEmbedding =
    TransactionForEmbedding(
        uid = uid,
        expenseDate = expenseDate,
        userId = userId,
        description = description,
        isCategorized = categoryId != null)

fun TransactionView.toTransactionForEmbedding(): TransactionForEmbedding =
    TransactionForEmbedding(
        uid = uid,
        userId = userId,
        expenseDate = expenseDate,
        description = description,
        isCategorized = categoryId != null)
