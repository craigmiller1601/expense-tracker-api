package us.craigmiller160.expensetrackerapi.data.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Table
import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.common.data.typedid.jpatype.JpaTypedId
import us.craigmiller160.expensetrackerapi.data.model.core.MutableTableEntity

@Entity
@Table(name = "transactions")
class Transaction(
    @JpaTypedId override var userId: TypedId<UserId>,
    override var expenseDate: LocalDate,
    override var description: String,
    override var amount: BigDecimal,
    @Column(name = "content_hash", insertable = false, updatable = false)
    var contentHash: String = "",
    var markNotDuplicateNano: Long? = null,
    override var confirmed: Boolean = false,
    @JpaTypedId override var categoryId: TypedId<CategoryId>? = null,
) : MutableTableEntity<TransactionId>(), TransactionCommon {
  constructor(
      other: Transaction
  ) : this(
      userId = other.userId,
      expenseDate = other.expenseDate,
      description = other.description,
      amount = other.amount,
      contentHash = other.contentHash,
      markNotDuplicateNano = other.markNotDuplicateNano,
      confirmed = other.confirmed,
      categoryId = other.categoryId)
}
