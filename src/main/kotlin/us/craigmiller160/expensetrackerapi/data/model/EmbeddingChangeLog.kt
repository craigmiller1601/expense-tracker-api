package us.craigmiller160.expensetrackerapi.data.model

import java.time.ZonedDateTime
import java.util.UUID
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.EmbeddingChangeLogId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId

data class EmbeddingChangeLog(
    val uid: TypedId<EmbeddingChangeLogId>,
    val userId: TypedId<UserId>,
    val recordId: UUID,
    val tableName: ChangeTrackedTable,
    val type: ChangeType,
    val isSynced: Boolean,
    val changeTimestamp: ZonedDateTime,
    val syncTimestamp: ZonedDateTime? = null
)

enum class ChangeType {
  INSERT,
  UPDATE,
  DELETE,
  TRUNCATE
}

enum class ChangeTrackedTable {
  TRANSACTIONS
}
