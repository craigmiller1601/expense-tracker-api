package us.craigmiller160.expensetrackerapi.data.model

enum class YesNoFilter {
  ALL,
  YES,
  NO
}
