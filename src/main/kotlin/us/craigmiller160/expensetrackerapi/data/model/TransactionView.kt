package us.craigmiller160.expensetrackerapi.data.model

import jakarta.persistence.Entity
import jakarta.persistence.Table
import java.math.BigDecimal
import java.time.LocalDate
import java.time.ZonedDateTime
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.common.data.typedid.jpatype.JpaTypedId
import us.craigmiller160.expensetrackerapi.data.model.core.ViewEntity

@Entity
@Table(name = "transactions_view")
class TransactionView(
    @JpaTypedId override var userId: TypedId<UserId>,
    override var expenseDate: LocalDate,
    override var description: String,
    override var amount: BigDecimal,
    var contentHash: String,
    @JpaTypedId override var categoryId: TypedId<CategoryId>?,
    var categoryName: String?,
    override var confirmed: Boolean,
    var duplicate: Boolean,
    var created: ZonedDateTime,
    var updated: ZonedDateTime
) : ViewEntity<TransactionId>(), TransactionCommon
