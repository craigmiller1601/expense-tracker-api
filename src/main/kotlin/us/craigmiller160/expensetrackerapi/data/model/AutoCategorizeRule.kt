package us.craigmiller160.expensetrackerapi.data.model

import jakarta.persistence.Entity
import jakarta.persistence.Table
import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AutoCategorizeRuleId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.common.data.typedid.jpatype.JpaTypedId
import us.craigmiller160.expensetrackerapi.data.model.core.MutableTableEntity

@Entity
@Table(name = "auto_categorize_rules")
class AutoCategorizeRule(
    @JpaTypedId var userId: TypedId<UserId>,
    @JpaTypedId var categoryId: TypedId<CategoryId>,
    var ordinal: Int,
    var regex: String,
    var startDate: LocalDate? = null,
    var endDate: LocalDate? = null,
    var minAmount: BigDecimal? = null,
    var maxAmount: BigDecimal? = null,
) : MutableTableEntity<AutoCategorizeRuleId>()
