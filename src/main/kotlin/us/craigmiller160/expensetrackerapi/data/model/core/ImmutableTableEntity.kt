package us.craigmiller160.expensetrackerapi.data.model.core

import jakarta.persistence.MappedSuperclass

@MappedSuperclass
abstract class ImmutableTableEntity<T> : TableEntity<T>() {
  override fun onPreUpdate() {
    error("Cannot update an immutable entity")
  }
}
