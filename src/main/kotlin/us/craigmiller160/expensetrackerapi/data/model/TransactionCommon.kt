package us.craigmiller160.expensetrackerapi.data.model

import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId

interface TransactionCommon {
  var uid: TypedId<TransactionId>
  var userId: TypedId<UserId>
  var expenseDate: LocalDate
  var description: String
  var amount: BigDecimal
  var confirmed: Boolean
  var categoryId: TypedId<CategoryId>?
}
