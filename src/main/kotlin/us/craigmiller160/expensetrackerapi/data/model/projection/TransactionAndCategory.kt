package us.craigmiller160.expensetrackerapi.data.model.projection

import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId

data class TransactionAndCategory(
    val transactionId: TypedId<TransactionId>,
    val categoryId: TypedId<CategoryId>,
)
