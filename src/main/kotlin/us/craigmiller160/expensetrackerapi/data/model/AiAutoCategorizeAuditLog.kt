package us.craigmiller160.expensetrackerapi.data.model

import java.time.ZonedDateTime
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AiAutoCategorizeAuditLogId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AiAutoCategorizeAuditLogOperationId

data class AiAutoCategorizeAuditLog(
    val id: TypedId<AiAutoCategorizeAuditLogId>,
    val operationId: TypedId<AiAutoCategorizeAuditLogOperationId>,
    val batchIndex: Int,
    val timestamp: ZonedDateTime,
    val success: Boolean,
    val aiResponse: String?,
    val error: String?
)
