package us.craigmiller160.expensetrackerapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.PropertySource
import us.craigmiller160.expensetrackerapi.config.YamlPropertySourceFactory

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = ["db.weaviate", "us.craigmiller160.expensetrackerapi"])
@ComponentScan(basePackages = ["db.weaviate", "us.craigmiller160.expensetrackerapi"])
@PropertySource(
    value = ["file:secret.yml"],
    ignoreResourceNotFound = true,
    factory = YamlPropertySourceFactory::class)
class ExpenseTrackerApiApplication

@Suppress("performance.SpreadOperator")
fun main(args: Array<String>) {
  runApplication<ExpenseTrackerApiApplication>(*args)
}
