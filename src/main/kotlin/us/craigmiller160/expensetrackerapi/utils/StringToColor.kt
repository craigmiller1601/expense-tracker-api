package us.craigmiller160.expensetrackerapi.utils

import java.util.Locale

object StringToColor {
  @Suppress("style.MagicNumber")
  fun get(value: String): String =
      String.format(Locale.getDefault(), "#%06x", 0xFFFFFF and value.hashCode())
}
