package us.craigmiller160.expensetrackerapi.service

import arrow.core.Either
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.CategoryId
import us.craigmiller160.expensetrackerapi.common.error.BadRequestException
import us.craigmiller160.expensetrackerapi.data.constants.CategoryConstants
import us.craigmiller160.expensetrackerapi.data.model.Category
import us.craigmiller160.expensetrackerapi.data.repository.CategoryRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.function.mapCatch
import us.craigmiller160.expensetrackerapi.utils.StringToColor
import us.craigmiller160.expensetrackerapi.web.types.category.CategoryRequest
import us.craigmiller160.expensetrackerapi.web.types.category.CategoryResponse

@Service
class CategoryService(
    private val categoryRepository: CategoryRepository,
    private val transactionRepository: TransactionRepository,
    private val authService: AuthorizationService
) {
  fun getAllCategories(): TryEither<List<CategoryResponse>> {
    val userId = authService.getAuthUserId()
    return Either.catch {
      categoryRepository.findAllByUserIdOrderByName(userId).map { CategoryResponse.from(it) }
    }
  }

  fun getUnknownCategory(): CategoryResponse =
      CategoryResponse.from(CategoryConstants.UNKNOWN_CATEGORY)

  private fun validateRequest(request: CategoryRequest): TryEither<CategoryRequest> {
    if (CategoryConstants.UNKNOWN_CATEGORY.name == request.name) {
      return Either.Left(
          BadRequestException("Illegal category name: ${CategoryConstants.UNKNOWN_CATEGORY.name}"))
    }
    return Either.Right(request)
  }

  @Transactional
  fun createCategory(request: CategoryRequest): TryEither<CategoryResponse> {
    val userId = authService.getAuthUserId()
    return validateRequest(request)
        .mapCatch {
          categoryRepository.save(
              Category(
                  name = request.name, userId = userId, color = StringToColor.get(request.name)))
        }
        .map { CategoryResponse.from(it) }
  }

  @Transactional
  fun updateCategory(categoryId: TypedId<CategoryId>, request: CategoryRequest): TryEither<Unit> {
    val userId = authService.getAuthUserId()
    return validateRequest(request)
        .mapCatch { categoryRepository.findByUidAndUserId(categoryId, userId) }
        .mapCatch { nullableCategory ->
          nullableCategory?.let { category ->
            categoryRepository.save(
                category.apply {
                  name = request.name
                  color = StringToColor.get(request.name)
                })
          }
        }
  }

  @Transactional
  fun deleteCategory(categoryId: TypedId<CategoryId>): TryEither<Unit> {
    val userId = authService.getAuthUserId()
    return Either.catch {
          transactionRepository.removeCategoryFromAllTransactions(userId, categoryId)
        }
        .mapCatch { categoryRepository.deleteByUidAndUserId(categoryId, userId) }
  }
}
