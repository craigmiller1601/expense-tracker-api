package us.craigmiller160.expensetrackerapi.service

import arrow.core.Either
import arrow.core.flatMap
import jakarta.transaction.Transactional
import java.io.InputStream
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.function.mapCatch
import us.craigmiller160.expensetrackerapi.service.parsing.TransactionParserManager
import us.craigmiller160.expensetrackerapi.web.types.importing.ImportTransactionsResponse
import us.craigmiller160.expensetrackerapi.web.types.importing.ImportTypeResponse

@Service
class TransactionImportService(
    private val transactionRepository: TransactionRepository,
    private val transactionParserManager: TransactionParserManager,
    private val applyCategoriesToTransactionsService: ApplyCategoriesToTransactionsService,
    private val authService: AuthorizationService,
    @Value("\${spring.ai.enabled}") private val aiEnabled: Boolean
) {
  fun getImportTypes(): List<ImportTypeResponse> =
      TransactionImportType.values().map { ImportTypeResponse(it.name, it.displayName) }

  @Transactional
  fun importTransactions(
      type: TransactionImportType,
      stream: InputStream
  ): TryEither<ImportTransactionsResponse> {
    val parser = transactionParserManager.getParserForType(type)
    val userId = authService.getAuthUserId()
    return parser
        .parse(userId, stream)
        .mapCatch { transactions -> transactionRepository.saveAll(transactions) }
        .flatMap { transactions ->
          if (!aiEnabled) {
            return@flatMap applyCategoriesToTransactionsService.applyCategoriesToTransactions(
                userId, transactions)
          }
          return@flatMap Either.Right(transactions)
        }
        .map { transactions -> ImportTransactionsResponse(transactions.size) }
  }
}
