package us.craigmiller160.expensetrackerapi.service

import arrow.core.Either
import jakarta.transaction.Transactional
import org.springframework.stereotype.Service
import us.craigmiller160.expensetrackerapi.data.repository.ReportRepository
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.web.types.report.ReportPageResponse
import us.craigmiller160.expensetrackerapi.web.types.report.ReportRequest

@Service
class ReportService(
    private val reportRepository: ReportRepository,
    private val authorizationService: AuthorizationService
) {
  @Transactional
  fun getSpendingByMonthAndCategory(request: ReportRequest): TryEither<ReportPageResponse> {
    val userId = authorizationService.getAuthUserId()
    return Either.catch { reportRepository.getSpendingByMonthAndCategory(userId, request) }
        .map { (spending, nextMonth) -> ReportPageResponse.from(spending, nextMonth) }
  }
}
