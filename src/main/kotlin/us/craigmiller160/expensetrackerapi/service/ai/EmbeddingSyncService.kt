package us.craigmiller160.expensetrackerapi.service.ai

import java.util.concurrent.atomic.AtomicBoolean
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.support.TransactionTemplate
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.EmbeddingChangeLogId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.common.error.EmbeddingSyncException
import us.craigmiller160.expensetrackerapi.data.model.ChangeType
import us.craigmiller160.expensetrackerapi.data.model.EmbeddingChangeLog
import us.craigmiller160.expensetrackerapi.data.model.projection.toTransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingChangeLogRepository
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository

@Service
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class EmbeddingSyncService(
    private val embeddingChangeLogRepository: EmbeddingChangeLogRepository,
    private val embeddingService: EmbeddingService,
    private val transactionRepository: TransactionRepository,
    private val transactionTemplate: TransactionTemplate,
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository
) {
  private val log = LoggerFactory.getLogger(javaClass)
  private val isRunning = AtomicBoolean(false)

  @Scheduled(initialDelay = 1000 * 60 * 1, fixedRate = 1000 * 60)
  fun sync() {
    if (!embeddingSyncConfigRepository.isInitialSyncComplete()) {
      log.info(
          "Initial sync has not been completed, skipping scheduled syncing changes with embedded database")
      return
    }

    val canProceed = isRunning.compareAndSet(false, true)
    if (!canProceed) {
      log.warn("Attempted to run sync while sync is already running, skipping this run")
      return
    }

    log.info("Running scheduled sync of changes with embedded database")
    try {
      val changeLogsToSync = embeddingChangeLogRepository.getAllNotSyncedChangeLogs()
      if (changeLogsToSync.isNotEmpty()) {
        prepareResourcesForSync(changeLogsToSync).let { syncResources(it) }
      }
    } catch (ex: Exception) {
      log.error("Error syncing embedded resources", ex)
      throw ex
    } finally {
      isRunning.set(false)
    }
  }

  private fun syncResources(transactionsForSync: TransactionsForSync) {
    transactionsForSync.toCreate.forEach { txnAndChangeIds ->
      transactionTemplate.execute {
        embeddingChangeLogRepository.markChangeLogsAsSynced(txnAndChangeIds.changeIds)
        val txn =
            transactionRepository.findById(txnAndChangeIds.transactionId).orElseThrow {
              EmbeddingSyncException(
                  "Cannot find transaction to sync by uid: ${txnAndChangeIds.transactionId}")
            }
        embeddingService.embedNewTransaction(txn.toTransactionForEmbedding())
      }
    }

    transactionsForSync.toUpdate.forEach { txnAndChangeIds ->
      transactionTemplate.execute {
        embeddingChangeLogRepository.markChangeLogsAsSynced(txnAndChangeIds.changeIds)
        val txn =
            transactionRepository.findById(txnAndChangeIds.transactionId).orElseThrow {
              EmbeddingSyncException(
                  "Cannot find transaction to sync by uid: ${txnAndChangeIds.transactionId}")
            }
        embeddingService.embedAndUpdateTransaction(txn.toTransactionForEmbedding())
      }
    }

    transactionsForSync.toDelete
        .groupBy { it.userId }
        .entries
        .forEach { (userId, transactions) ->
          transactionTemplate.execute {
            embeddingChangeLogRepository.markChangeLogsAsSynced(
                transactions.flatMap { it.changeIds })
            embeddingService.deleteAllTransactionsById(
                userId, transactions.map { it.transactionId })
          }
        }
  }

  private fun handleTruncate(
      forSync: TransactionsForSync,
      txn: EmbeddingChangeLog
  ): TransactionsForSync {
    throw EmbeddingSyncException("Truncate is not currently supported by sync")
  }

  private fun handleInsert(
      forSync: TransactionsForSync,
      txn: EmbeddingChangeLog
  ): TransactionsForSync {
    if (forSync.toCreate.find { it.transactionId.uuid == txn.recordId } != null) {
      throw EmbeddingSyncException("Should not find multiple inserts for the same record")
    }
    return forSync.copy(toCreate = forSync.toCreate + listOf(TransactionChangeIds(txn)))
  }

  private fun handleUpdate(
      forSync: TransactionsForSync,
      txn: EmbeddingChangeLog
  ): TransactionsForSync {
    val isBeingCreated = forSync.toCreate.find { it.transactionId.uuid == txn.recordId } != null
    val isBeingUpdated = forSync.toUpdate.find { it.transactionId.uuid == txn.recordId } != null
    if (!isBeingCreated && !isBeingUpdated) {
      return forSync.copy(toUpdate = forSync.toUpdate + listOf(TransactionChangeIds(txn)))
    }

    if (isBeingCreated) {
      return forSync.copy(
          toCreate =
              forSync.toCreate.map { txnChangeIds ->
                if (txnChangeIds.transactionId.uuid == txn.recordId) {
                  return@map txnChangeIds.copy(changeIds = txnChangeIds.changeIds + listOf(txn.uid))
                }
                return@map txnChangeIds
              })
    }

    // isBeingUpdated
    return forSync.copy(
        toUpdate =
            forSync.toUpdate.map { txnChangeIds ->
              if (txnChangeIds.transactionId.uuid == txn.recordId) {
                return@map txnChangeIds.copy(changeIds = txnChangeIds.changeIds + listOf(txn.uid))
              }
              return@map txnChangeIds
            })
  }

  private fun handleDelete(
      forSync: TransactionsForSync,
      txn: EmbeddingChangeLog
  ): TransactionsForSync {
    val (toUpdateMatching, toUpdate) =
        forSync.toUpdate.partition { it.transactionId.uuid == txn.recordId }
    val (toCreateMatching, toCreate) =
        forSync.toCreate.partition { it.transactionId.uuid == txn.recordId }

    val txnChangeIds =
        TransactionChangeIds(
            transactionId = TypedId(txn.recordId),
            userId = txn.userId,
            changeIds =
                listOf(txn.uid) +
                    toUpdateMatching.flatMap { it.changeIds } +
                    toCreateMatching.flatMap { it.changeIds })

    return forSync.copy(
        toCreate = toCreate,
        toUpdate = toUpdate,
        toDelete = forSync.toDelete + listOf(txnChangeIds))
  }

  private fun prepareResourcesForSync(changeLogs: List<EmbeddingChangeLog>): TransactionsForSync =
      changeLogs.fold(TransactionsForSync()) { forSync, txn ->
        when (txn.type) {
          ChangeType.TRUNCATE -> handleTruncate(forSync, txn)
          ChangeType.INSERT -> handleInsert(forSync, txn)
          ChangeType.UPDATE -> handleUpdate(forSync, txn)
          ChangeType.DELETE -> handleDelete(forSync, txn)
        }
      }

  private data class TransactionsForSync(
      val toCreate: List<TransactionChangeIds> = listOf(),
      val toUpdate: List<TransactionChangeIds> = listOf(),
      val toDelete: List<TransactionChangeIds> = listOf()
  )

  private data class TransactionChangeIds(
      val transactionId: TypedId<TransactionId>,
      val userId: TypedId<UserId>,
      val changeIds: List<TypedId<EmbeddingChangeLogId>>
  ) {
    constructor(
        changeLog: EmbeddingChangeLog
    ) : this(TypedId(changeLog.recordId), changeLog.userId, listOf(changeLog.uid))
  }
}
