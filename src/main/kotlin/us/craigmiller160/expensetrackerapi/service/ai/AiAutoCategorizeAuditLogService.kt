package us.craigmiller160.expensetrackerapi.service.ai

import com.fasterxml.jackson.databind.ObjectMapper
import java.time.ZoneId
import java.time.ZonedDateTime
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AiAutoCategorizeAuditLogOperationId
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.CategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.model.AiAutoCategorizeAuditLog
import us.craigmiller160.expensetrackerapi.data.repository.AiAutoCategorizeAuditLogRepository

@Service
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class AiAutoCategorizeAuditLogService(
    private val auditLogRepo: AiAutoCategorizeAuditLogRepository,
    private val objectMapper: ObjectMapper
) {
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  fun recordAuditLogSuccess(
      operationId: TypedId<AiAutoCategorizeAuditLogOperationId>,
      batchIndex: Int,
      data: List<CategorizedTransaction>
  ) {
    val auditLog =
        AiAutoCategorizeAuditLog(
            id = TypedId(),
            operationId = operationId,
            timestamp = ZonedDateTime.now(ZoneId.of("UTC")),
            batchIndex = batchIndex,
            aiResponse = objectMapper.writeValueAsString(data),
            success = true,
            error = null)
    auditLogRepo.createAuditLog(auditLog)
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  fun recordAuditLogFailed(
      operationId: TypedId<AiAutoCategorizeAuditLogOperationId>,
      batchIndex: Int,
      exception: Exception,
      data: List<CategorizedTransaction>?
  ) {
    val aiResponse = data?.let { objectMapper.writeValueAsString(it) }
    val auditLog =
        AiAutoCategorizeAuditLog(
            id = TypedId(),
            operationId = operationId,
            timestamp = ZonedDateTime.now(ZoneId.of("UTC")),
            batchIndex = batchIndex,
            aiResponse = aiResponse,
            success = false,
            error = exception.getFullMessage())
    auditLogRepo.createAuditLog(auditLog)
  }

  private fun Throwable.getFullMessage(): String {
    val type = this.javaClass.name
    val message = this.message
    if (this.cause != null) {
      return "$type: $message\n${this.cause!!.getFullMessage()}"
    }
    return "$type: $message"
  }
}
