package us.craigmiller160.expensetrackerapi.service.ai

import java.time.LocalDate
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import us.craigmiller160.expensetrackerapi.client.ai.AiClient
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.config.AiSettingsProperties
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransactionSearchResult
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.toEmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.repository.EmbeddedTransactionRepository
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionForEmbedding
import us.craigmiller160.expensetrackerapi.web.types.transaction.TransactionAndCategoryUpdateItem

@Service
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class EmbeddingService(
    private val aiClient: AiClient,
    private val embeddedTransactionRepository: EmbeddedTransactionRepository,
    private val aiSettingsProperties: AiSettingsProperties
) {
  companion object {
    private const val END_DATE_YEARS = 10L
    private const val BATCH_UPDATE_SIZE = 10
  }
  private val log = LoggerFactory.getLogger(javaClass)
  private val executor =
      Executors.newFixedThreadPool(BATCH_UPDATE_SIZE, Thread.ofVirtual().factory())

  fun queryForEmbeddedTransactions(
      query: String,
      limit: Int,
      userId: TypedId<UserId>,
      startDate: LocalDate =
          LocalDate.now()
              .minusMonths(aiSettingsProperties.transactionSearch.oldestRecordMonths.toLong()),
      endDate: LocalDate = LocalDate.now().plusYears(END_DATE_YEARS)
  ): List<EmbeddedTransactionSearchResult> {
    val embeddedQuery = aiClient.createEmbedding(listOf(query)).flatten().map { it.toFloat() }

    return embeddedTransactionRepository.searchForTransactions(
        userId, embeddedQuery, startDate, endDate, limit)
  }

  fun embedNewTransactions(transactions: List<TransactionForEmbedding>) {
    log.info("Embedding {} transactions", transactions.size)
    val textToEmbed = transactions.map { txn -> txn.description }
    val embeddings = aiClient.createEmbedding(textToEmbed)
    transactions
        .mapIndexed { index, txn ->
          txn.toEmbeddedTransaction(embeddings[index].map { it.toFloat() })
        }
        .let { embeddedTransactions ->
          embeddedTransactionRepository.insertAllTransactions(embeddedTransactions)
        }
    log.debug("Embedding complete")
  }

  fun updateIsCategorizedForTransactionBatch(
      userId: TypedId<UserId>,
      transactionsAndCategories: Set<TransactionAndCategoryUpdateItem>
  ) {
    log.info("Updating is categorized for transactions")
    val futures =
        transactionsAndCategories.map { txn ->
          CompletableFuture.runAsync(
              {
                embeddedTransactionRepository.updateTransactionIsCategorized(
                    userId, txn.transactionId, txn.categoryId != null)
              },
              executor)
        }
    CompletableFuture.allOf(*futures.toTypedArray()).join()
  }

  fun updateIsCategorizedForTransaction(
      userId: TypedId<UserId>,
      transactionId: TypedId<TransactionId>,
      isCategorized: Boolean
  ) =
      embeddedTransactionRepository.updateTransactionIsCategorized(
          userId, transactionId, isCategorized)

  fun embedNewTransaction(transaction: TransactionForEmbedding) {
    log.info("Embedding transaction")
    aiClient
        .createEmbedding(listOf(transaction.description))
        .first()
        .let { vectors -> transaction.toEmbeddedTransaction(vectors.map { it.toFloat() }) }
        .let { embeddedTransactionRepository.insertTransaction(it) }
    log.debug("Embedding complete")
  }

  fun embedAndUpdateTransaction(transaction: TransactionForEmbedding) {
    log.info("Embedding and updating transaction")
    aiClient
        .createEmbedding(listOf(transaction.description))
        .first()
        .let { vectors -> transaction.toEmbeddedTransaction(vectors.map { it.toFloat() }) }
        .let { embeddedTransactionRepository.updateTransaction(it) }
    log.debug("Embedding update complete")
  }

  fun deleteAllTransactionsAllTenants() {
    embeddedTransactionRepository.deleteAllTransactionsAllTenants()
  }

  fun deleteAllTransactionsById(
      userId: TypedId<UserId>,
      transactionIds: List<TypedId<TransactionId>>
  ) {
    embeddedTransactionRepository.deleteAllTransactionsById(userId, transactionIds)
  }

  fun getTransactionById(
      userId: TypedId<UserId>,
      transactionId: TypedId<TransactionId>
  ): EmbeddedTransaction? = embeddedTransactionRepository.getTransactionById(userId, transactionId)
}
