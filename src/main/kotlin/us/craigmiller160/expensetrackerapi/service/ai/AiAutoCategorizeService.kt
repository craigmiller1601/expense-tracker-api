package us.craigmiller160.expensetrackerapi.service.ai

import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import us.craigmiller160.expensetrackerapi.client.ai.AiClient
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.AiAutoCategorizeAuditLogOperationId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.config.AiSettingsProperties
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.CategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.PreviousTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.toPreviousTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.chat.toUncategorizedTransaction
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransactionSearchResult
import us.craigmiller160.expensetrackerapi.data.model.TransactionCommon
import us.craigmiller160.expensetrackerapi.data.model.YesNoFilter
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionAndCategory
import us.craigmiller160.expensetrackerapi.data.repository.CategoryRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionViewRepository
import us.craigmiller160.expensetrackerapi.service.AuthorizationService
import us.craigmiller160.expensetrackerapi.service.ai.AiAutoCategorizeService.Companion.TRANSACTIONS_PER_PROMPT_LIMIT
import us.craigmiller160.expensetrackerapi.web.types.SortDirection
import us.craigmiller160.expensetrackerapi.web.types.ai.ReportCounts
import us.craigmiller160.expensetrackerapi.web.types.ai.TestAutoCategorizeTransactionReport
import us.craigmiller160.expensetrackerapi.web.types.ai.TestAutoCategorizeTransactionsRequest
import us.craigmiller160.expensetrackerapi.web.types.ai.TransactionResult
import us.craigmiller160.expensetrackerapi.web.types.ai.toTransactionResult
import us.craigmiller160.expensetrackerapi.web.types.ai.toUncategorizedTransaction
import us.craigmiller160.expensetrackerapi.web.types.transaction.SearchTransactionsRequest
import us.craigmiller160.expensetrackerapi.web.types.transaction.TransactionSortKey
import us.craigmiller160.expensetrackerapi.web.types.transaction.toPageable

private typealias QueryForTransactionFunction =
    (String, Int) -> List<EmbeddedTransactionSearchResult>

@Service
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class AiAutoCategorizeService(
    private val embeddingService: EmbeddingService,
    private val transactionRepository: TransactionRepository,
    private val transactionViewRepository: TransactionViewRepository,
    private val categoryRepository: CategoryRepository,
    private val aiClient: AiClient,
    private val aiSettings: AiSettingsProperties,
    private val authService: AuthorizationService,
    private val auditLogService: AiAutoCategorizeAuditLogService
) {
  companion object {
    private const val PARALLEL_LIMIT = 8
    private const val SEARCH_LIMIT = 3
    private const val TRANSACTIONS_PER_PROMPT_LIMIT = 50
    private const val SKIPPED_TXN_KEY = "SKIPPED_TXN"
    private const val UNCATEGORIZED_TXN_KEY = "UNCATEGORIZED_TXN"
    private const val INACCURATELY_CATEGORIZED_TXN_KEY = "INACCURATELY_CATEGORIZED_TXN"
    private const val ACCURATELY_CATEGORIZED_TXN_KEY = "ACCURATELY_CATEGORIZED_TXN"
  }
  private val log = LoggerFactory.getLogger(javaClass)
  private val executor = Executors.newFixedThreadPool(PARALLEL_LIMIT, Thread.ofVirtual().factory())

  @Transactional
  fun autoCategorizeUnconfirmedTransactions(): TypedId<AiAutoCategorizeAuditLogOperationId> {
    log.info("Auto-categorizing all unconfirmed transactions")
    val userId = authService.getAuthUserId()
    val categories = categoryRepository.findAll()

    val unconfirmedCount = transactionRepository.countAllUnconfirmed(userId)
    val batchCount = getTransactionBatchCount(unconfirmedCount)
    log.debug(
        "Found {} unconfirmed transactions. Will process in {} batches",
        unconfirmedCount,
        batchCount)

    val auditLogOperationId = TypedId<AiAutoCategorizeAuditLogOperationId>()

    (0 until batchCount).map { batchNumber ->
      var categorizedTransactions: List<CategorizedTransaction>? = null
      try {
        val req =
            SearchTransactionsRequest(
                pageNumber = batchNumber.toInt(),
                pageSize = TRANSACTIONS_PER_PROMPT_LIMIT,
                sortKey = TransactionSortKey.EXPENSE_DATE,
                sortDirection = SortDirection.DESC,
                startDate = null,
                endDate = null,
                confirmed = YesNoFilter.NO,
                categorized = YesNoFilter.ALL)
        log.debug(
            "Auto-categorizing transaction batch {} of {}. Search request: {}",
            batchNumber + 1,
            batchCount,
            req)
        val transactions =
            transactionRepository.searchForTransactions(req, userId, req.toPageable())

        if (transactions.content.isEmpty()) {
          log.warn(
              "Auto-categorize transactions search returned 0 results for batch {}", batchNumber)
          return@map
        }

        val pastTransactions = getPotentialMatchTransactions(userId, transactions.content)
        categorizedTransactions = categorizeTransactions(transactions.content, pastTransactions)

        transactions.content
            .mapNotNull { txn -> categorizedTransactions.find { it.uid == txn.uid } }
            .filter { txn -> txn.category?.isNotBlank() == true }
            .mapNotNull { txn ->
              categories
                  .find { it.name == txn.category }
                  ?.let { category ->
                    TransactionAndCategory(transactionId = txn.uid, categoryId = category.uid)
                  }
                  ?: run {
                    log.error(
                        "Cannot find existing category for AI assigned category. Transaction UID: ${txn.uid} Category: ${txn.category}")
                    null
                  }
            }
            .let { values -> transactionRepository.categorizeAllTransactions(userId, values) }

        auditLogService.recordAuditLogSuccess(
            auditLogOperationId, batchNumber.toInt(), categorizedTransactions)
        log.info("Auto-categorization of transactions is complete")
      } catch (ex: Exception) {
        auditLogService.recordAuditLogFailed(
            auditLogOperationId, batchNumber.toInt(), ex, categorizedTransactions)
        throw ex
      }
    }
    return auditLogOperationId
  }

  private fun categorizeTransactions(
      transactionsToCategorize: List<TransactionCommon>,
      pastTransactions: List<PreviousTransaction>
  ): List<CategorizedTransaction> {
    val categories = categoryRepository.findAll()
    val categoryNames = categories.map { it.name }
    val uncategorizedTransactions = transactionsToCategorize.map { it.toUncategorizedTransaction() }
    return aiClient.autoCategorizeTransactions(
        categoryNames, pastTransactions, uncategorizedTransactions)
  }

  @Suppress("performance.SpreadOperator")
  private fun getPotentialMatchTransactions(
      userId: TypedId<UserId>,
      transactionsToCategorize: List<TransactionCommon>,
      queryFn: QueryForTransactionFunction = { query, limit ->
        embeddingService.queryForEmbeddedTransactions(query, limit, userId)
      }
  ): List<PreviousTransaction> {
    val futures =
        transactionsToCategorize.toList().map { txn ->
          CompletableFuture.supplyAsync(
              {
                queryFn("Show me transactions similar to ${txn.description}", SEARCH_LIMIT)
                    .map { it.uid }
                    .let { transactionViewRepository.findAllByUidInAndUserId(it, userId) }
              },
              executor)
        }

    CompletableFuture.allOf(*futures.toTypedArray()).join()
    return futures
        .map { it.get() }
        .flatten()
        .distinctBy { it.uid }
        .map { it.toPreviousTransaction() }
        .sorted()
  }

  fun testAutoCategorizeTransactions(
      request: TestAutoCategorizeTransactionsRequest
  ): TestAutoCategorizeTransactionReport {
    log.info("Testing auto-categorization of transactions")
    val userId = authService.getAuthUserId()

    val transactionsToCategorize =
        SearchTransactionsRequest(
                pageNumber = 0,
                pageSize = 1000,
                sortKey = TransactionSortKey.EXPENSE_DATE,
                sortDirection = SortDirection.DESC,
                startDate = request.startDate,
                endDate = request.endDate,
                confirmed = YesNoFilter.YES,
                categorized = YesNoFilter.YES)
            .also { log.debug("Auto-categorize test request: {}", it) }
            .let { req ->
              transactionRepository.searchForTransactions(
                  req, userId, PageRequest.of(req.pageNumber, req.pageSize))
            }

    require(transactionsToCategorize.content.isNotEmpty()) {
      "Request did not match any potential transactions"
    }

    log.debug(
        "Transactions for testing auto-categorization count: {}",
        transactionsToCategorize.content.size)

    val pastTransactions =
        getPotentialMatchTransactions(userId, transactionsToCategorize.content) { query, limit ->
          embeddingService.queryForEmbeddedTransactions(
              query,
              limit,
              userId,
              request.startDate.minusMonths(
                  aiSettings.transactionSearch.oldestRecordMonths.toLong()),
              request.startDate.minusDays(1))
        }

    val categorizedTransactions =
        categorizeTransactions(transactionsToCategorize.content, pastTransactions)

    val groupedResults =
        categorizedTransactions
            .map { txn ->
              val originalTxn =
                  transactionsToCategorize.find { oTxn -> oTxn.uid == txn.uid }
                      ?: throw IllegalArgumentException(
                          "Cannot find matching original transaction to AI response: ${txn.uid}")
              txn.toTransactionResult(originalTxn)
            }
            .groupBy { it.groupKey }

    return TestAutoCategorizeTransactionReport(
        counts =
            ReportCounts(
                total = categorizedTransactions.size,
                skipped = groupedResults[SKIPPED_TXN_KEY].orEmpty().size,
                categorized =
                    groupedResults[ACCURATELY_CATEGORIZED_TXN_KEY].orEmpty().size +
                        groupedResults[INACCURATELY_CATEGORIZED_TXN_KEY].orEmpty().size,
                accuratelyCategorized =
                    groupedResults[ACCURATELY_CATEGORIZED_TXN_KEY].orEmpty().size),
        uncategorized =
            groupedResults[UNCATEGORIZED_TXN_KEY].orEmpty().map { it.toUncategorizedTransaction() },
        inaccuratelyCategorized = groupedResults[INACCURATELY_CATEGORIZED_TXN_KEY].orEmpty())
  }

  private val TransactionResult.groupKey: String
    get() {
      if (description.contains("AMAZON") ||
          description.contains("AMZN MKTP") ||
          description.contains("CHECK")) {
        return SKIPPED_TXN_KEY
      }

      if (aiCategory == null) {
        return UNCATEGORIZED_TXN_KEY
      }

      if (aiCategory != actualCategory) {
        return INACCURATELY_CATEGORIZED_TXN_KEY
      }

      return ACCURATELY_CATEGORIZED_TXN_KEY
    }

  private fun getTransactionBatchCount(unconfirmedTransactionCount: Long): Long =
      (unconfirmedTransactionCount / TRANSACTIONS_PER_PROMPT_LIMIT.toLong()) +
          if (unconfirmedTransactionCount % TRANSACTIONS_PER_PROMPT_LIMIT.toLong() > 0L) 1L else 0L
}
