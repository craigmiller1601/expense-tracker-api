package us.craigmiller160.expensetrackerapi.service.ai

import jakarta.transaction.Transactional
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.common.error.EmbedTransactionBatchException
import us.craigmiller160.expensetrackerapi.data.ai.model.embedding.EmbeddedTransaction
import us.craigmiller160.expensetrackerapi.data.model.projection.TransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.model.projection.toTransactionForEmbedding
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionRepository
import us.craigmiller160.expensetrackerapi.data.repository.TransactionViewRepository
import us.craigmiller160.expensetrackerapi.service.AuthorizationService
import us.craigmiller160.expensetrackerapi.web.types.ai.EmbeddedTransactionResponse
import us.craigmiller160.expensetrackerapi.web.types.ai.QueryEmbeddingsRequest
import us.craigmiller160.expensetrackerapi.web.types.ai.toEmbeddedTransactionResponse

@Service
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class EmbeddingAdminService(
    private val embeddingService: EmbeddingService,
    private val transactionRepository: TransactionRepository,
    private val transactionViewRepository: TransactionViewRepository,
    private val authService: AuthorizationService,
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository
) {
  companion object {
    private const val BATCH_SIZE = 50L
    private const val EMBED_ALL_PARALLEL_LIMIT = 5
  }

  private val log = LoggerFactory.getLogger(javaClass)
  private val executor =
      Executors.newFixedThreadPool(EMBED_ALL_PARALLEL_LIMIT, Thread.ofVirtual().factory())

  @Transactional
  fun deleteAllTransactionsAllTenants() {
    embeddingService.deleteAllTransactionsAllTenants()
    embeddingSyncConfigRepository.setInitialSyncComplete(false)
  }

  fun getTransactionById(
      userId: TypedId<UserId>,
      transactionId: TypedId<TransactionId>
  ): EmbeddedTransaction? = embeddingService.getTransactionById(userId, transactionId)

  @Suppress("performance.SpreadOperator")
  fun embedAllTransactionsAllTenants() {
    log.info("Embedding all transactions for all tenants")
    val count = transactionRepository.count()
    val totalPages = getPages(count)
    log.debug("Transactions to embed {} in {} batches", count, totalPages)
    val futures =
        (0 until totalPages).map { index ->
          CompletableFuture.supplyAsync({ embedTransactionBatch(index) }, executor)
        }
    CompletableFuture.allOf(*futures.toTypedArray()).join()

    log.info("All transactions embedded. Total: {}", count)
  }

  fun runQueryAgainstEmbeddings(
      request: QueryEmbeddingsRequest
  ): List<EmbeddedTransactionResponse> {
    val userId = authService.getAuthUserId()
    val embeddedTransactions =
        embeddingService.queryForEmbeddedTransactions(request.query, request.limit, userId)
    val transactionIds = embeddedTransactions.map { it.uid }
    return transactionViewRepository
        .findAllByUserIdAndUidIn(authService.getAuthUserId(), transactionIds)
        .map { txn ->
          val embeddedTxn =
              embeddedTransactions.find { it.uid == txn.uid }
                  ?: throw IllegalArgumentException(
                      "Unable to find match between embedding and transaction")
          txn.toEmbeddedTransactionResponse(embeddedTxn)
        }
  }

  private fun embedTransactionBatch(index: Long): List<TransactionForEmbedding> {
    log.debug("Embedding transaction batch {} starting", index)
    return try {
      transactionRepository
          .getAllTransactionsAllUsers(index, BATCH_SIZE)
          .map { it.toTransactionForEmbedding() }
          .let { txns ->
            log.debug("Found {} transactions for batch {}", txns.size, index)
            embeddingService.embedNewTransactions(txns)
            log.debug("Embedding transaction batch {} complete", index)
            txns
          }
    } catch (ex: Exception) {
      throw EmbedTransactionBatchException("Error embedding transaction batch $index", ex)
    }
  }

  private fun getPages(count: Long): Long {
    val basePages = count / BATCH_SIZE
    val extra = if (count % BATCH_SIZE > 0) 1 else 0
    return basePages + extra
  }
}
