package us.craigmiller160.expensetrackerapi.service.ai

import jakarta.annotation.PostConstruct
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles
import org.springframework.stereotype.Service
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingChangeLogRepository
import us.craigmiller160.expensetrackerapi.data.repository.EmbeddingSyncConfigRepository

@Service
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class EmbeddingSyncInitService(
    private val embeddingSyncConfigRepository: EmbeddingSyncConfigRepository,
    private val embeddingChangeLogRepository: EmbeddingChangeLogRepository,
    private val embeddingAdminService: EmbeddingAdminService,
    private val environment: Environment
) {

  fun initializeSync() {
    if (!embeddingSyncConfigRepository.isInitialSyncComplete()) {
      embeddingAdminService.embedAllTransactionsAllTenants()
      embeddingChangeLogRepository.deleteAllChangeLogs()
      embeddingSyncConfigRepository.setInitialSyncComplete(true)
    }
  }

  @PostConstruct
  fun initializeSyncOnStartup() {
    if (environment.acceptsProfiles(Profiles.of("prod"))) {
      initializeSync()
    }
  }
}
