package us.craigmiller160.expensetrackerapi.service.parsing

import java.io.InputStream
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.function.TryEither

interface TransactionParser {
  fun parse(userId: TypedId<UserId>, stream: InputStream): TryEither<List<Transaction>>
}
