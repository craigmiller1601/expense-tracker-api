package us.craigmiller160.expensetrackerapi.service.parsing

import arrow.core.flatMap
import arrow.core.sequence
import java.io.InputStream
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.UserId
import us.craigmiller160.expensetrackerapi.data.model.Transaction
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.function.flatMapKeepRight

abstract class AbstractCsvTransactionParser : TransactionParser {
  override fun parse(userId: TypedId<UserId>, stream: InputStream): TryEither<List<Transaction>> =
      CsvParser.parse(stream)
          .flatMapKeepRight { data -> validateImportType(data.header) }
          .flatMap { data -> data.records.map { parseRecord(userId, it) }.sequence() }

  abstract fun parseRecord(userId: TypedId<UserId>, row: Array<String>): TryEither<Transaction>

  abstract fun validateImportType(headerRow: Array<String>): TryEither<Unit>
}
