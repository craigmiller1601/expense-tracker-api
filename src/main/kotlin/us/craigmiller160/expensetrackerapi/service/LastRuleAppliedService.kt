package us.craigmiller160.expensetrackerapi.service

import arrow.core.Either
import org.springframework.stereotype.Service
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.common.data.typedid.ids.TransactionId
import us.craigmiller160.expensetrackerapi.data.repository.LastRuleAppliedRepository
import us.craigmiller160.expensetrackerapi.function.TryEither
import us.craigmiller160.expensetrackerapi.web.types.rules.LastRuleAppliedResponse

@Service
class LastRuleAppliedService(
    private val lastRuleAppliedRepository: LastRuleAppliedRepository,
    private val authService: AuthorizationService
) {
  fun getLastAppliedRuleForTransaction(
      transactionId: TypedId<TransactionId>
  ): TryEither<LastRuleAppliedResponse?> {
    val userId = authService.getAuthUserId()
    return Either.catch {
          lastRuleAppliedRepository.getLastRuleDetailsForTransaction(userId, transactionId)
        }
        .map { result -> result?.let { LastRuleAppliedResponse.from(it) } }
  }
}
