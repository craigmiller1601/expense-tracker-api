package db.weaviate

import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.core.type.classreading.CachingMetadataReaderFactory

fun loadMigrations(vararg packagePaths: String): List<Migration> {
  val resolver = PathMatchingResourcePatternResolver()
  val factory = CachingMetadataReaderFactory(resolver)
  return packagePaths
      .asSequence()
      .flatMap { path -> resolver.getResources("$path/*.class").toList() }
      .map { resource -> factory.getMetadataReader(resource).classMetadata.className }
      .map { Class.forName(it) }
      .filter { clazz -> Migration::class.java.isAssignableFrom(clazz) }
      .map { clazz -> clazz.getDeclaredConstructor().newInstance() }
      .map { obj -> obj as Migration }
      .sortedBy { it.javaClass.simpleName }
      .toList()
}
