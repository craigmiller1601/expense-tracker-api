package db.weaviate

class MigrationException(msg: String, cause: Throwable? = null) : RuntimeException(msg, cause)
