package db.weaviate.history

import db.weaviate.MigrationName
import java.sql.Timestamp
import java.time.ZoneId
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId
import us.craigmiller160.expensetrackerapi.extension.getUUID

@Repository
class MigrationHistoryRepository(private val jdbcTemplate: NamedParameterJdbcTemplate) {
  companion object {
    private val GET_HISTORY_QUERY =
        """
            SELECT *
            FROM weaviate_migration_history
        """
            .trimIndent()

    private val INSERT_HISTORY_QUERY =
        """
            INSERT INTO weaviate_migration_history (id, index, version, name, timestamp)
            VALUES (:id, :index, :version, :name, :timestamp)
        """
            .trimIndent()
  }

  fun getMigrationHistory(): List<MigrationHistoryRecord> =
      jdbcTemplate.query(GET_HISTORY_QUERY, MapSqlParameterSource()) { rs, _ ->
        MigrationHistoryRecord(
            id = TypedId(rs.getUUID("id")),
            index = rs.getInt("index"),
            version = rs.getString("version"),
            name = rs.getString("name"),
            timestamp = rs.getTimestamp("timestamp").toInstant().atZone(ZoneId.of("UTC")))
      }

  fun insertHistoryRecord(index: Int, migrationName: MigrationName): MigrationHistoryRecord {
    val record =
        MigrationHistoryRecord(
            name = migrationName.name, version = migrationName.version, index = index)
    val params =
        MapSqlParameterSource()
            .addValue("id", record.id.uuid)
            .addValue("version", record.version)
            .addValue("name", record.name)
            .addValue("timestamp", Timestamp.from(record.timestamp.toInstant()))
            .addValue("index", record.index)
    jdbcTemplate.update(INSERT_HISTORY_QUERY, params)
    return record
  }
}
