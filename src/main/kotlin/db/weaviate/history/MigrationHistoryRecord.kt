package db.weaviate.history

import db.weaviate.MigrationHistoryId
import java.time.ZoneId
import java.time.ZonedDateTime
import us.craigmiller160.expensetrackerapi.common.data.typedid.TypedId

data class MigrationHistoryRecord(
    val index: Int,
    val version: String,
    val name: String,
    val timestamp: ZonedDateTime = ZonedDateTime.now(ZoneId.of("UTC")),
    val id: TypedId<MigrationHistoryId> = TypedId()
)
