package db.weaviate

import db.weaviate.history.MigrationHistoryRecord
import db.weaviate.history.MigrationHistoryRepository
import io.weaviate.client.WeaviateClient
import jakarta.annotation.PostConstruct
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import us.craigmiller160.expensetrackerapi.config.WeaviateProperties

@Component
@ConditionalOnProperty(name = ["spring.ai.enabled"], havingValue = "true")
class MigrationRunner(
    private val migrationRepository: MigrationHistoryRepository,
    private val weaviateClient: WeaviateClient,
    private val weaviateProperties: WeaviateProperties
) {
  private val log = LoggerFactory.getLogger(javaClass)

  @PostConstruct
  @Suppress("style.ThrowsCount")
  fun run() {
    log.info("Finding and running weaviate migrations")
    val actualMigrations = loadMigrations("classpath:db/weaviate/migration")
    val historyMigrations = migrationRepository.getMigrationHistory()

    actualMigrations.mapIndexed { index, migration ->
      runCatching {
            val actualIndex = index + 1
            val migrationName = getMigrationName(actualIndex, migration)
            val historyRecord = getHistoryRecord(historyMigrations, index)
            if (historyRecord == null) {
              log.info("Running migration: ${migration.javaClass.name}")
              migration.migrate(weaviateClient, weaviateProperties.classes.transactions)
              migrationRepository.insertHistoryRecord(actualIndex, migrationName)
              return@mapIndexed MigrationReport(
                  migrationName = migration.javaClass.simpleName, executed = true)
            }

            if (historyRecord.version != migrationName.version) {
              throw MigrationException(
                  "Migration at index $actualIndex has incorrect version. Expected: ${historyRecord.version} Actual: ${migrationName.version}")
            }

            if (historyRecord.name != migrationName.name) {
              throw MigrationException(
                  "Migration at index $actualIndex has incorrect name. Expected: ${historyRecord.name} Actual: ${migrationName.name}")
            }

            MigrationReport(migrationName = migration.javaClass.simpleName, executed = false)
          }
          .recoverCatching { ex ->
            throw MigrationException("Error performing weaviate migration", ex)
          }
          .getOrThrow()
    }
  }

  private fun getHistoryRecord(
      historyRecords: List<MigrationHistoryRecord>,
      index: Int
  ): MigrationHistoryRecord? {
    if (historyRecords.size > index) {
      return historyRecords[index]
    }
    return null
  }
}
