package db.weaviate

import io.weaviate.client.WeaviateClient

interface Migration {
  fun migrate(client: WeaviateClient, className: String)
}
