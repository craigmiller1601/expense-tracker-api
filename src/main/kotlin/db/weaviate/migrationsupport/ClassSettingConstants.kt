package db.weaviate.migrationsupport

const val EF = -1
const val DYNAMIC_EF_FACTOR = 10
const val DYNAMIC_EF_MIN = 5
const val DYNAMIC_EF_MAX = 50
const val STOPWORD_CONFIG_PRESET = "en"
