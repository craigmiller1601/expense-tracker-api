package db.weaviate

data class MigrationReport(val migrationName: String, val executed: Boolean)
