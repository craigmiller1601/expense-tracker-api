package db.weaviate.migration

import db.weaviate.Migration
import db.weaviate.migrationsupport.STOPWORD_CONFIG_PRESET
import io.weaviate.client.WeaviateClient
import io.weaviate.client.v1.misc.model.InvertedIndexConfig
import io.weaviate.client.v1.misc.model.MultiTenancyConfig
import io.weaviate.client.v1.misc.model.StopwordConfig
import io.weaviate.client.v1.misc.model.VectorIndexConfig
import io.weaviate.client.v1.schema.model.DataType
import io.weaviate.client.v1.schema.model.Property
import io.weaviate.client.v1.schema.model.WeaviateClass
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.DESCRIPTION_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.EXPENSE_DATE_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.IS_CATEGORIZED_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.getOrThrow

class V1_20250222__Better_Index : Migration {
  override fun migrate(client: WeaviateClient, className: String) {
    client.schema().classDeleter().withClassName(className).run().getOrThrow()

    val indexConfig =
        VectorIndexConfig.builder().ef(512).efConstruction(128).distance("cosine").build()

    val stopwordConfig = StopwordConfig.builder().preset(STOPWORD_CONFIG_PRESET).build()
    val invertedIndexConfig =
        InvertedIndexConfig.builder().stopwords(stopwordConfig).indexTimestamps(true).build()

    val multiTenancyConfig =
        MultiTenancyConfig.builder().enabled(true).autoTenantCreation(true).build()

    val expenseDateProperty =
        Property.builder()
            .name(EXPENSE_DATE_FIELD_NAME)
            .dataType(listOf(DataType.DATE))
            .indexFilterable(true)
            .build()
    val descriptionProperty =
        Property.builder().name(DESCRIPTION_FIELD_NAME).dataType(listOf(DataType.TEXT)).build()
    val isCategorizedProperty =
        Property.builder()
            .name(IS_CATEGORIZED_FIELD_NAME)
            .dataType(listOf(DataType.BOOLEAN))
            .indexFilterable(true)
            .build()

    WeaviateClass.builder()
        .className(className)
        .properties(listOf(expenseDateProperty, descriptionProperty, isCategorizedProperty))
        .vectorIndexConfig(indexConfig)
        .invertedIndexConfig(invertedIndexConfig)
        .multiTenancyConfig(multiTenancyConfig)
        .build()
        .let { client.schema().classCreator().withClass(it).run() }
        .getOrThrow()
  }
}
