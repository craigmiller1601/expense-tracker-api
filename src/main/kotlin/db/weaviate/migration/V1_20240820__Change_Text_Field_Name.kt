package db.weaviate.migration

import db.weaviate.Migration
import io.weaviate.client.WeaviateClient
import io.weaviate.client.v1.schema.model.DataType
import io.weaviate.client.v1.schema.model.Property
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.DESCRIPTION_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.getOrThrow

class V1_20240820__Change_Text_Field_Name : Migration {
  override fun migrate(client: WeaviateClient, className: String) {
    client
        .schema()
        .propertyCreator()
        .withClassName(className)
        .withProperty(
            Property.builder().name(DESCRIPTION_FIELD_NAME).dataType(listOf(DataType.TEXT)).build())
        .run()
        .getOrThrow()
  }
}
