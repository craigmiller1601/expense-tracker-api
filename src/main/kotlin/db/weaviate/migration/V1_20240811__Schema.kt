package db.weaviate.migration

import db.weaviate.Migration
import db.weaviate.migrationsupport.DYNAMIC_EF_FACTOR
import db.weaviate.migrationsupport.DYNAMIC_EF_MAX
import db.weaviate.migrationsupport.DYNAMIC_EF_MIN
import db.weaviate.migrationsupport.EF
import db.weaviate.migrationsupport.STOPWORD_CONFIG_PRESET
import io.weaviate.client.WeaviateClient
import io.weaviate.client.v1.misc.model.InvertedIndexConfig
import io.weaviate.client.v1.misc.model.StopwordConfig
import io.weaviate.client.v1.misc.model.VectorIndexConfig
import io.weaviate.client.v1.schema.model.DataType
import io.weaviate.client.v1.schema.model.Property
import io.weaviate.client.v1.schema.model.WeaviateClass
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.TEXT_FIELD_NAME
import us.craigmiller160.expensetrackerapi.data.ai.weaviate.getOrThrow

class V1_20240811__Schema : Migration {
  override fun migrate(client: WeaviateClient, className: String) {
    val indexConfig =
        VectorIndexConfig.builder()
            .ef(EF)
            .dynamicEfFactor(DYNAMIC_EF_FACTOR)
            .dynamicEfMin(DYNAMIC_EF_MIN)
            .dynamicEfMax(DYNAMIC_EF_MAX)
            .build()

    val stopwordConfig = StopwordConfig.builder().preset(STOPWORD_CONFIG_PRESET).build()
    val invertedIndexConfig =
        InvertedIndexConfig.builder().stopwords(stopwordConfig).indexTimestamps(true).build()

    WeaviateClass.builder()
        .className(className)
        .properties(
            listOf(
                Property.builder().name(TEXT_FIELD_NAME).dataType(listOf(DataType.TEXT)).build()))
        .vectorIndexConfig(indexConfig)
        .invertedIndexConfig(invertedIndexConfig)
        .build()
        .let { client.schema().classCreator().withClass(it).run() }
        .getOrThrow()
  }
}
