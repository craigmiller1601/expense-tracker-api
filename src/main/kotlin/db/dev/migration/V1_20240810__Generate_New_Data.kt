package db.dev.migration

import java.math.BigDecimal
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters
import java.util.UUID
import kotlin.random.Random
import org.flywaydb.core.api.migration.BaseJavaMigration
import org.flywaydb.core.api.migration.Context
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.datasource.SingleConnectionDataSource

/**
 * This works because dev data gets wiped and reset often, which makes it predictable at the point
 * of execution.
 */
class V1_20240810__Generate_New_Data : BaseJavaMigration() {
  companion object {
    private val GET_ALL_EXISTING_QUERY =
        """
            SELECT user_id, confirmed, description, category_id
            FROM transactions
        """
            .trimIndent()

    private val DELETE_ALL_TRANSACTIONS = "DELETE FROM transactions"

    private val GET_ALL_CATEGORY_IDS =
        """
          SELECT uid
          FROM categories
      """
            .trimIndent()

    private val INSERT_QUERY =
        """
          INSERT INTO public.transactions(uid, user_id, expense_date, description, amount, category_id, confirmed, created, updated, version)
          VALUES(:uid, :userId, :expenseDate, :description, :amount, :categoryId, :confirmed, NOW(), NOW(), 1)
      """
            .trimIndent()
  }

  override fun migrate(ctx: Context) {
    val jdbcTemplate = NamedParameterJdbcTemplate(SingleConnectionDataSource(ctx.connection, true))
    val existingTransactions = getExistingTransactions(jdbcTemplate)
    val existingCategories = getExistingCategories(jdbcTemplate)

    jdbcTemplate.update(DELETE_ALL_TRANSACTIONS, MapSqlParameterSource())

    val minDateForMods = LocalDate.now().minusMonths(2).with(TemporalAdjusters.firstDayOfMonth())

    val batchParameters =
        (0 until 7800).map { index ->
          val existingTransaction = existingTransactions[index % existingTransactions.size]
          val validCategoryId = existingCategories[index % existingCategories.size]

          val expenseDate = LocalDate.now().minusDays(index.toLong())

          val rawAmount = BigDecimal((Random.nextFloat() * 100).toString())
          val amount =
              if (minDateForMods < expenseDate && index % 20 == 0) {
                rawAmount
              } else {
                rawAmount * BigDecimal("-1")
              }

          val categoryId =
              if (minDateForMods < expenseDate && index % 2 == 0) null else validCategoryId

          MapSqlParameterSource()
              .addValue("uid", UUID.randomUUID())
              .addValue("userId", existingTransaction.userId)
              .addValue("expenseDate", expenseDate)
              .addValue("description", existingTransaction.description)
              .addValue("amount", amount)
              .addValue("categoryId", categoryId)
              .addValue("confirmed", existingTransaction.confirmed)
        }

    jdbcTemplate.batchUpdate(INSERT_QUERY, batchParameters.toTypedArray())
  }

  private fun getExistingCategories(jdbcTemplate: NamedParameterJdbcTemplate): List<UUID> =
      jdbcTemplate.query(GET_ALL_CATEGORY_IDS, MapSqlParameterSource()) { rs, _ ->
        UUID.fromString(rs.getString("uid"))
      }

  private fun getExistingTransactions(
      jdbcTemplate: NamedParameterJdbcTemplate
  ): List<ExistingTransaction> =
      jdbcTemplate.query(GET_ALL_EXISTING_QUERY, MapSqlParameterSource()) { rs, _ ->
        val description = rs.getString("description")
        val categoryId = rs.getString("category_id")?.let { UUID.randomUUID() }
        val confirmed = rs.getBoolean("confirmed")
        val userId = UUID.fromString(rs.getString("user_id"))
        ExistingTransaction(userId, description, confirmed, categoryId)
      }

  private data class ExistingTransaction(
      val userId: UUID,
      val description: String,
      val confirmed: Boolean,
      val categoryId: UUID?
  )
  private data class NewTransaction(
      val uid: UUID,
      val userId: UUID,
      val expenseDate: LocalDate,
      val description: String,
      val amount: BigDecimal,
      val categoryId: UUID?,
      val confirmed: Boolean
  )

  private data class NewTransactionsHolder(
      val newTransactions: List<NewTransaction>,
      val nextExistingTransactionIndex: Int
  )
}
