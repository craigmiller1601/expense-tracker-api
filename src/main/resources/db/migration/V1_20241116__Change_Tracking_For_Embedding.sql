CREATE TYPE change_type AS ENUM ('insert', 'update', 'delete', 'truncate');
CREATE TYPE change_tracked_table AS ENUM ('transactions');

CREATE TABLE embedding_change_log(
    uid UUID NOT NULL,
    type change_type NOT NULL,
    table_name change_tracked_table NOT NULL,
    user_id UUID NOT NULL,
    record_id UUID NOT NULL,
    is_synced BOOLEAN NOT NULL DEFAULT false,
    change_timestamp TIMESTAMP NOT NULL DEFAULT now(),
    sync_timestamp TIMESTAMP,
    PRIMARY KEY (uid)
);

CREATE FUNCTION record_change_for_embedding()
RETURNS TRIGGER AS
$$
    DECLARE
        type change_type;
        table_name change_tracked_table;
    BEGIN
        type = lower(tg_op);
        table_name = tg_table_name;

        CASE
            WHEN type = 'insert' THEN
                INSERT INTO embedding_change_log (uid, type, table_name, user_id, record_id)
                VALUES (gen_random_uuid(), type, table_name, new.user_id, new.uid);
                RETURN new;
            WHEN type = 'update' THEN
                INSERT INTO embedding_change_log (uid, type, table_name, user_id, record_id)
                VALUES (gen_random_uuid(), type, table_name, old.user_id, old.uid);
                RETURN new;
            WHEN type = 'delete' THEN
                INSERT INTO embedding_change_log (uid, type, table_name, user_id, record_id)
                VALUES (gen_random_uuid(), type, table_name, old.user_id, old.uid);
                RETURN old;
            WHEN type = 'truncate' THEN
                RAISE EXCEPTION 'No implementation for trigger for truncate';
        END CASE;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER transactions_record_change_for_weaviate_insert_update
BEFORE INSERT OR UPDATE ON transactions
FOR EACH ROW
EXECUTE FUNCTION record_change_for_embedding();

CREATE TRIGGER transactions_record_changes_for_weaviate_delete
AFTER DELETE on transactions
FOR EACH ROW
EXECUTE FUNCTION record_change_for_embedding();

CREATE TRIGGER transactions_record_change_for_weaviate_truncate
BEFORE TRUNCATE ON transactions
FOR EACH STATEMENT
EXECUTE FUNCTION record_change_for_embedding();