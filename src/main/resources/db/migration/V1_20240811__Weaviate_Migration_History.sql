CREATE TABLE weaviate_migration_history (
    id UUID NOT NULL,
    index INT NOT NULL,
    version VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    timestamp TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (index),
    UNIQUE (version, name)
);