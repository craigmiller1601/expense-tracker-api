CREATE TABLE ai_auto_categorize_audit_log (
    id UUID NOT NULL,
    operation_id UUID NOT NULL,
    batch_index INT NOT NULL,
    timestamp TIMESTAMPTZ NOT NULL,
    success BOOLEAN NOT NULL DEFAULT false,
    ai_response JSONB,
    error TEXT,
    PRIMARY KEY (id)
);
CREATE INDEX ai_auto_categorize_audit_log_operation_id ON ai_auto_categorize_audit_log (operation_id);