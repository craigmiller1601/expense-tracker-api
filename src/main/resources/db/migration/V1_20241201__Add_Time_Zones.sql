ALTER TABLE auto_categorize_rules
ALTER COLUMN created TYPE timestamptz;

ALTER TABLE auto_categorize_rules
ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE categories
ALTER COLUMN created TYPE timestamptz;

ALTER TABLE categories
ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE last_rule_applied
ALTER COLUMN created TYPE timestamptz;

ALTER TABLE embedding_change_log
ALTER COLUMN change_timestamp TYPE timestamptz;

ALTER TABLE embedding_change_log
ALTER COLUMN sync_timestamp TYPE timestamptz;

DO $$
DECLARE
    view_def TEXT;
    exec_text TEXT;
BEGIN
    view_def := pg_get_viewdef('transactions_view');

    DROP VIEW transactions_view;

    ALTER TABLE transactions
    ALTER COLUMN created TYPE timestamptz;

    ALTER TABLE transactions
    ALTER COLUMN updated TYPE timestamptz;

    exec_text := format('CREATE VIEW transactions_view AS %s', view_def);
    execute exec_text;
END;
$$ LANGUAGE plpgsql;

