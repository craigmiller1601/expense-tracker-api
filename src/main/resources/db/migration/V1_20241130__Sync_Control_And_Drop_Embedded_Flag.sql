ALTER TABLE transactions
DROP COLUMN embedded;

CREATE TABLE embedding_sync_config (
    uid UUID NOT NULL,
    initial_sync_complete BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (uid)
);

CREATE FUNCTION embedding_sync_config_only_one_record()
RETURNS TRIGGER AS
$$
DECLARE
    record_count INT;
BEGIN
    SELECT COUNT(*) INTO record_count
    FROM embedding_sync_config;

    IF record_count > 0 THEN
        RAISE EXCEPTION 'Cannot have more than one record in embedding_sync_config table';
    END IF;

    RETURN new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER embedding_sync_config_only_one_record
BEFORE INSERT ON embedding_sync_config
FOR EACH ROW
EXECUTE FUNCTION embedding_sync_config_only_one_record();

INSERT INTO embedding_sync_config (uid, initial_sync_complete)
VALUES (gen_random_uuid(), false);