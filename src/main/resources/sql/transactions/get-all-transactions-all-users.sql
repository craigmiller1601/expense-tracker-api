SELECT *
FROM transactions
ORDER BY created ASC
OFFSET :offset LIMIT :limit;