WITH valid_category AS (
    SELECT c.uid
    FROM categories c
    WHERE c.uid = :categoryId
    AND c.user_id = :userId
)
UPDATE transactions t
SET category_id = vc.uid,
version = version + 1
FROM valid_category vc
WHERE vc.uid = :categoryId
AND t.uid = :transactionId
AND t.user_id = :userId;