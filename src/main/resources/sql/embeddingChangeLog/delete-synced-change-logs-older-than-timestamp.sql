DELETE FROM embedding_change_log
WHERE change_timestamp <= :timestamp
AND is_synced = true;