UPDATE embedding_change_log
SET is_synced = true, sync_timestamp = :timestamp
WHERE uid IN (:ids);