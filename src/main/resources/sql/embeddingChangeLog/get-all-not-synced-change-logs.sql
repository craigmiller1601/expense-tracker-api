SELECT *
FROM embedding_change_log
WHERE is_synced = false
ORDER BY change_timestamp ASC;