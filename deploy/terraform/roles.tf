locals {
  access_role_common = {
    name = "access"
  }

  admin_role_common = {
    name = "admin"
  }
}

import {
  to = keycloak_role.expense_tracker_api_access_role_dev
  id = "apps-dev/ee604fa0-372e-4689-89c2-5975d02bf618"
}

resource "keycloak_role" "expense_tracker_api_access_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.expense_tracker_api_dev.id
  name = local.access_role_common.name
}

import {
  to = keycloak_role.expense_tracker_api_access_role_prod
  id = "apps-prod/179c89d0-7c5e-46d8-9990-5d0535b3c6a6"
}

resource "keycloak_role" "expense_tracker_api_access_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.expense_tracker_api_prod.id
  name = local.access_role_common.name
}

import {
  to = keycloak_role.expense_tracker_api_admin_role_dev
  id = "apps-dev/508d57d6-c08b-4052-93ef-e77eaba90401"
}

resource "keycloak_role" "expense_tracker_api_admin_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.expense_tracker_api_dev.id
  name = local.admin_role_common.name
}

import {
  to = keycloak_role.expense_tracker_api_admin_role_prod
  id = "apps-prod/d930fbad-5919-4a6a-a845-1e23952b9a58"
}

resource "keycloak_role" "expense_tracker_api_admin_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.expense_tracker_api_prod.id
  name = local.admin_role_common.name
}