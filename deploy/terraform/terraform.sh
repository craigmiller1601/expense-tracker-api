#!/bin/bash

set -euo pipefail

terraform apply \
  -var=onepassword_token="$ONEPASSWORD_TOKEN"