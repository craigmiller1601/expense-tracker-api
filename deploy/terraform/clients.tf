data "keycloak_realm" "apps_dev" {
  realm = "apps-dev"
}

data "keycloak_realm" "apps_prod" {
  realm = "apps-prod"
}

locals {
  client_common = {
    client_id = "expense-tracker-api"
    name = "expense-tracker-api"
    enabled = true
    access_type = "CONFIDENTIAL"
    service_accounts_enabled = true
  }
}

import {
  to = keycloak_openid_client.expense_tracker_api_dev
  id = "apps-dev/61836f57-974a-4544-8f09-9018214f4753"
}

resource "keycloak_openid_client" "expense_tracker_api_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}

import {
  to = keycloak_openid_client.expense_tracker_api_prod
  id = "apps-prod/0bc32e80-942c-49a7-87e1-d2a4d6f4cca5"
}

resource "keycloak_openid_client" "expense_tracker_api_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}

