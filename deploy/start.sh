#!/bin/bash

set -e

keytool -import \
  -alias cluster-ca \
  -file /cluster-certs/ca.crt \
  -keystore "$JAVA_HOME/lib/security/cacerts" \
  -noprompt \
  -storepass changeit

java -jar /expense-tracker-api.jar